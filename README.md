# University's Digital Registry

This project aims to create a hybrid environment for University's Registry which allows digital record of documents' flows the University receives and issues. Users can self-register their documents and have the record of their submits. Moreover, they can vizualize the documents they received in their department and the ones that will be due within a specific number of days. When documents change their place and status, users can update the information accordingly.

The system improves traceability of documents within the University, reduces paper consumption and helps creating a less stressful work environment.

The project was created with **ReactJS** and **JavaScript**.

Used **node packages**:
* [axios](https://www.npmjs.com/package/axios)
* [semantic-ui-react](https://react.semantic-ui.com/)
* [react-router-dom](https://v5.reactrouter.com/web/guides/quick-start)
* [moment](https://momentjs.com/)
* [react-semantic-toasts](https://www.npmjs.com/package/react-semantic-toasts) 
* [react-autosuggest](https://github.com/moroshko/react-autosuggest)

## Table of contents
1. [Installation](https://gitlab.com/agsis_team/registratura/registratura-frontend#installation)
2. [Usage](https://gitlab.com/agsis_team/registratura/registratura-frontend#usage)
3. [Roadmap](https://gitlab.com/agsis_team/registratura/registratura-frontend#roadmap)
4. [Acknowledgements](https://gitlab.com/agsis_team/registratura/registratura-frontend#acknowledgements)

## Installation

After the project was cloned, you have to run these scripts in the project directory (it requires to have installed [Node.js](https://nodejs.org/en/)):

```bash
npm install
```
Installs the Node Package Manager (NPM).

```bash
npm start
```
Runs the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Usage

In the following steps it will be presented the user's flow within the application.

1. Form

Users can submit forms which will stand for registering their documents. They have to fill in the required fields in order to enable the submit button. The required fields are marked with a red star. 

Users can submit their documents no matter the time of the day, but only within working hours of the University's Registry documents can be considered as registered. Each registered document will get one unique number in order to be legally identified within the institution.

<img src="https://gitlab.com/agsis_team/registratura/registratura-frontend/-/raw/master/src/README-images/form.jpg" width="700" />

To improve user's experience and to reduce the possibility to fill in wrong information, each field has one info bubble with additional information when it is hovered which describes the purpose of the field. 

<img src="https://gitlab.com/agsis_team/registratura/registratura-frontend/-/raw/master/src/README-images/help-info.jpg" width="400" />

In order to reduce the time spent in filling in the form, some fields have the autosuggest function based on previous completions of logged user for that field. 

Each user can submit documents only for themselves and within the departments they are part of. Users that have admin role can submit documents for any of the University's personnel. For name and email fields they can choose from the suggestions based on the entered name/email.

<img src="https://gitlab.com/agsis_team/registratura/registratura-frontend/-/raw/master/src/README-images/autosuggest.jpg" width="300" />


2. Record history

In order to reproduce the physical registry there is the record history section, which allows users to see the documents that have been submitted by their department in order to have a general perspective of the department's activity. Access to all of the documents submitted within the University was granted only to the personnel of University's Registry, who can see and update any document. 

To improve users experience, document are color coded:
- <span style="color:red">red</span> - overdue and unfinished document
- <span style="color:yellow">yellow</span> - within due date and unfinished document
- <span style="color:green">green</span> - finished document

<img src="https://gitlab.com/agsis_team/registratura/registratura-frontend/-/raw/master/src/README-images/record-history.jpg" width="800" />

Taking into consideration that there is an average of 45 documents submitted daily and almost 20 000 yearly, the time spent to search a certain document might be too long. In order to improve this experience, it was created a filter bar which allows users to search a document by various options: year of submission, unique number, department which submitted and so on. 

<img src="https://gitlab.com/agsis_team/registratura/registratura-frontend/-/raw/master/src/README-images/filters.jpg" width="600" />

3. Documents' track history

Each documents has its own flow within the University. In an institution it is preferred to have detailed information about the departments and people responsible for a certain document and its solution. When Registry's personnel hands in a document to a certain person from a department, both the person who gave the document and the one that took it have the responsibility to write down these details. Users can add a new department in the document's flow and can check if they have handed in or received the document. Their email address and date are filled in automatically. 

<img src="https://gitlab.com/agsis_team/registratura/registratura-frontend/-/raw/master/src/README-images/track-update.jpg" width="600" />

Documents' track can be update by any personnel in the Registry's University and departments that have received the document.

<img src="https://gitlab.com/agsis_team/registratura/registratura-frontend/-/raw/master/src/README-images/track-history.jpg" width="600" />

4. Status history

Similar to documents' track history, each document has a history of their statuses and date of status update. This can also be update by any person in Registry's University and departments that have received the document.

<img src="https://gitlab.com/agsis_team/registratura/registratura-frontend/-/raw/master/src/README-images/status-history.jpg" width="600" />

5. Received documents

Similar to record history section, there is received documents section, which allows users to see the documents they have received within their department at a certain moment. This feature improves the experience of day to day tasks because it gives a general point of view of the documents that need to be solved, when there is a huge workload of documents to be processed. 

6. Due documents

Similar to record history and received documents, due documents section allows users to see which documents are close to due date or overdue. This helps users to focus on the important documents that have to be solved.

7. Unregistered documents

Taking into consideration that submitted documents are registered only within working hours, the unregistered ones are processed manually by Registry's personnel. In the unregistered documents section each document has one "Register" green button. This section is available only to Registry's personnel.

8. Report problems

Users can submit one ticket in the University's problem reporting platform by selecting the topic called 'Registratura' and detailing the problem.  

9. Settings

The settings section is available only to Registry's personnel. Within this section they have the possibility to:
- generate and download the Registry in pdf format
- set the time of working hours
- set the national free days in order not to allow submitted documents to be automatically registered within these legally free days 

<img src="https://gitlab.com/agsis_team/registratura/registratura-frontend/-/raw/master/src/README-images/settings.jpg" width="600" />

## Roadmap

* At the moment, users don't have the possibility to upload files within the platform. One important feature to be added is to **allows users to attach justificative documents** when they submit new form or to attach them afterwards, in the record history section. This can reduce significant amount of time spent to search a certain document among physical archives.
* For future updates, it can be useful to implement **templates for repetitive types of documents** that are submitted. One meaningful example can be vacation requests, which can be simplified.
* Including **a section with statistics** can also be helpful. This can include statistics such as which departments receive the most documents, which days are the most busy or which departments take the most time to solve a document.
* Updating the track and status history of documents can take really long time. A certain document can be searched only by using the filters bar and there are many click a user has to make in order to find certain document. To reduce the time significantly, it is taken into consideration to implement a system which consists in the following flow:
    - each physical document receives a **barcode** with their unique number in the database
    - each person in the Registry department has one **tablet which can scan the barcode**
    - after scanning the barcode, the user is **redirected to the details of the scanned document**
    - in this page, they are able to update the track, the status or any other information of the scanned document

## Acknowledgements

This project was developed in the "We improve the University!" 2019-2020 competition of Transilvania University of Brașov. 

Thank you @curti1 for all your support and advice and work on the backend of this project.