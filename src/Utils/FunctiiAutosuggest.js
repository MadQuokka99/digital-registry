import {getSugestiiColoane, getSugestiiNumeEmail} from "./ApiCalls";
import moment from "moment";

const getSugestii = async (value, numeColoana, email) => {

    return await getSugestiiColoane(numeColoana, value, 10, email)
        .then(response => {
            return response;
        })
        .catch(error => console.log(error));
}

const getSugestiiNumeEmailAutosuggest = async (value, key) => {

    if (value.length >= 4) {
        return await getSugestiiNumeEmail(key, value)
            .then(response => {
                let lista = [];
                for (let i = 0; i < response.length; i++) {
                    let elementListaUsers = {
                        nume: response[i].Nume.concat(' '.concat(response[i].Prenume)),
                        email: response[i].Username
                    }
                    lista.push(elementListaUsers);
                }
                return lista;
            })
            .catch(error => {
                console.log(error);
            })
    } else return []
}

const formatareTextEsteRevenireRaspuns = (cerereRaspunsRevenireInfo) => {
    return 'Nr de ordine: ' + cerereRaspunsRevenireInfo.NrDeOrdine + ', conținut: ' + cerereRaspunsRevenireInfo.ContinutulActului + ', data: '
        + moment(cerereRaspunsRevenireInfo.DataInregistrare).format("DD-MM-YYYY");
}

export {
    getSugestii,
    getSugestiiNumeEmailAutosuggest,
    formatareTextEsteRevenireRaspuns
}