import axios from './axios-AGSIS-API'

export const descarcaFisier = async (url) => {

  return axios
    .get(
      url,
      {headers: {'Content-Type': 'application/json'}, responseType: 'blob'}
    )
    .then((response) => {
      //se ia numele fisierului din headerele trimise de pe server
      let filename = "";
      let disposition = response.headers["content-disposition"]
      if (disposition && disposition.indexOf('attachment') !== -1) {
        let filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
        let matches = filenameRegex.exec(disposition);
        if (matches != null && matches[1]) {
          filename = matches[1].replace(/['"]/g, '');
        }
      }

      if (!filename) {
        filename = "fisier.pdf"
      }

      //se creeaza un element si simuleaza un click pe ele pentru a face descarcarea fisierului in browser
      const downloadUrl = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement('a');
      link.href = downloadUrl;
      link.setAttribute('download', filename); //any other extension
      document.body.appendChild(link);
      link.click();
      link.remove();
    })
};

export default descarcaFisier;
