import {getDocumentByNrOrdineAni} from "./ApiCalls";
import moment from "moment";

const verificareObjectIsEmpty = (obiect) => {

    let obiectCopy = {...obiect};
    let isEmpty = true;

    if (obiectCopy.MailDeponent !== "" || obiectCopy.NumeDeponent !== "" ||
        obiectCopy.NrActuluiIntrat !== "" || obiectCopy.CineAEmis_Autoritate !== "" || (obiectCopy.NrDeOrdine !== null && obiectCopy.NrDeOrdine !== '') ||
        obiectCopy.DataInregistrare_Inceput !== "" || obiectCopy.CineAEmis_ID_Departament !== "" ||
        obiectCopy.DataInregistrare_Sfarsit !== "" || obiectCopy.ContinutulActului !== "")

        isEmpty = false;
    return isEmpty;
}

const cautaCerereServerSide = async (nrDeOrdine, aniPtMetodaGetDocByNrDeOrdine) => {
    return await getDocumentByNrOrdineAni(nrDeOrdine, aniPtMetodaGetDocByNrDeOrdine.an1, aniPtMetodaGetDocByNrDeOrdine.an2)
        .then(response => {
            let copie = [];

            for (let i = 0; i < response.length; i++) {
                let elementSearch = {
                    key: response[i].ID_Document,
                    value: response[i].ID_Document,
                    text: "Nr de ordine: " + response[i].NrDeOrdine + ", nr. actului intrat: " + response[i].NrActuluiIntrat + ", conținut: " + response[i].ContinutulActului + ", data: " + moment(response[i].DataInregistrare).format("DD-MM-YYYY"),
                }
                copie.push(elementSearch);
            }
            return copie;
        })
        .catch((error) => {
            console.log(error);
        })
}

export {
    verificareObjectIsEmpty,
    cautaCerereServerSide
}