import axios from "./axios-AGSIS-API";
import moment from "moment";
import {toast} from "react-semantic-toasts";

/**
 * retinerea tipurilor de solicitari
 * @returns {Promise<[]>}
 */
const getTipuriSolicitari = async () => {

    let tipuri = await axios
        .get("Registratura/TipDocumentList");

    return tipuri.data;
}

/**
 * retinerea continutului pentru cererile depuse pentru Rectorat
 * @returns {Promise<[]>}
 */
const getContinutCereri = async () => {

    let continuturi = await
        axios
            .get("Registratura/GetContinutActe");

    let raspuns = continuturi.data;
    let continutCereriCopy = [];

    for (let i = 0; i < raspuns.length; i++) {

        let elementDropdown = {
            key: i + 1,
            text: raspuns[i].Title,
            value: raspuns[i].Title
        }
        continutCereriCopy.push(elementDropdown);
    }

    return continutCereriCopy;

}
/**
 * retinerea statusurilor posibile pentru solicitari
 * @returns {Promise<void>}
 */
const getModuriTrimitere = async () => {
    let moduri = await
        axios
            .get("Registratura/ModTrimitereRaspunsList");

    return moduri.data;
}

/**
 * retinerea departamentelor din Rectorat
 * @returns {Promise<[]>}
 */
const getDepartamente = async () => {

    let departamente = await
        axios
            .get("Registratura/DepartamenteList");

    return departamente.data;

}

/**
 * retinerea statusurilor posibile pentru solicitari
 * @returns {Promise<void>}
 */
const getStatusuriCerere = async () => {
    let statusuri = await
        axios
            .get("Registratura/StatusDocumentList");

    let raspuns = statusuri.data;

    return raspuns;

}

const estePersonalRegistratura = async (rol, username) => {
    let eInRol = await axios
        .get(`Users/IsInRole?role=${rol}&username=${username}`);

    let esteInRol;

    if (username === 'd.givan@unitbv.ro') {
        esteInRol = false;
    } else {
        esteInRol = eInRol.data
    }

    return esteInRol;
}


const EsteInTimpulProgramului = async () => {

    let sePoate = await axios
        .get(`Registratura/EsteInTimpulProgramului`);

    return sePoate.data;
}



const setariRegistratura = async () => {
    let setari = await axios
        .get(`Registratura/SetariRegistraturaGet`)
    return setari.data;
}


const getProgramRegistratura = async () => {
    let program = await
        axios
            .get(`Registratura/ProgramLucruRegistraturaGet`);
    return program.data;
}

const formatarePtDropdown = (lista, ID, denumire) => {

    let listaDeReturnat = [];
    for (let i = 0; i < lista.length; i++) {

        let element = {
            key: lista[i][ID],
            text: lista[i][denumire],
            value: lista[i][ID]
        }

        listaDeReturnat.push(element);
    }

    return listaDeReturnat;
}

const formatareNumePtDropdown = (lista) => {
    let listaDeReturnat = [];
    for (let i = 0; i < lista.length; i++) {

        let numeIntreg = lista[i].Nume.concat(' '.concat(lista[i].Prenume));

        let element = {
            key: lista[i].ID_Personal,
            value: numeIntreg,
            text: numeIntreg
        }

        listaDeReturnat.push(element);
    }

    return listaDeReturnat;
}

const formatareEmailPtDropdown = (lista) => {
    let listaDeReturnat = [];
    for (let i = 0; i < lista.length; i++) {

        let element = {
            key: lista[i].ID_Personal,
            value: lista[i].Username,
            text: lista[i].Username
        }

        listaDeReturnat.push(element);
    }

    return listaDeReturnat;
}

const formatareDepPtDropdown = (estePersonalRegistratura, lista) => {
    let listaDeReturnat = [];
    for (let i = 0; i < lista.length; i++) {

        let element = {
            key: lista[i]["ID_Departament"],
            value: estePersonalRegistratura ? '-1' : lista[i]["ID_Departament"],
            text: lista[i]["NumeDepartament"]
        }
        listaDeReturnat.push(element);
    }
    return listaDeReturnat;
}

const formatareDepUserPtDropdown = (lista) => {
    let listaDeReturnat = [];
    for (let i = 0; i < lista.length; i++) {

        let element = {
            key: lista[i]["ID_Departament"],
            value: lista[i]["ID_Departament"],
            text: lista[i]["NumeDepartament"]
        }

        listaDeReturnat.push(element);
    }

    //console.log(listaDeReturnat);
    return listaDeReturnat;
}


const getNumeEmailByUsername = async (username) => {
    return await axios
        .get(`Registratura/PersonalInfoByUsername?Username=${username}`)
        .then(raspuns => {

            return raspuns.data;
        })
        .catch(error => {
            console.log(error);
        })
}

const getCereriPanaLaTermenLimita = async (ID_Departament, nrZile) => {
    return await axios
        .get(`Registratura/DocumentListDue?ID_Departament=${ID_Departament}&NrZilePanaLaTermenLimita=${nrZile}`)
        .then(raspuns => {

            return raspuns.data;
        })
        .catch(error => {
            console.log(error);
        })
}

const getDepartamenteDinCareFaceParte = async (username) => {
    return await axios
        .get(`Registratura/DepartamenteDinCareFaceParte?Username=${username}`)
        .then(raspuns => {

            return raspuns.data;
        })
        .catch(error => {
            console.log(error);
        })
}

const getCereriIntrateByDepartament = async (ID_Departament, an, nrPePagina, nrPagina) => {
    return await axios
        .get(`Registratura/DocumentListIntratePagedByDepartamentAn?ID_Departament=${ID_Departament}&An=${an}&NrPePagina=${nrPePagina}&NrPagina=${nrPagina}`)
        .then(raspuns => {
            return raspuns.data
        })
        .catch(error => {
            console.log(error);
        })
}

const getCereriIstoricByDepartament = async (ID_Departament, an, nrPePagina, nrPagina) => {
    return await axios
        .get(`Registratura/DocumentListPagedByDepartamentAn?ID_Departament=${ID_Departament}&An=${an}&NrPePagina=${nrPePagina}&NrPagina=${nrPagina}`)
        .then(raspuns => {
            return raspuns.data
        })
        .catch(error => {
            console.log(error);
        })
}


const getCountCereriIntrate = async (ID_Departament, an) => {

    return await axios
        .get(`Registratura/DocumenteIntrateCountByDepartamentAn?ID_Departament=${ID_Departament}&An=${an}`)
        .then(raspuns => {
            return raspuns.data
        })
        .catch(error => {
            console.log(error);
        })

}

const getCountCereriIstoric = async (ID_Departament, an) => {

    return await axios
        .get(`Registratura/DocumentCountByDepartamentAn?ID_Departament=${ID_Departament}&An=${an}`)
        .then(raspuns => {
            return raspuns.data
        })
        .catch(error => {
            console.log(error);
        })

}

const getSugestiiColoane = async (NumeColoana, Caractere, TopN, Username) => {

    return await axios
        .get(`Registratura/SugestiiColoanaListByCaractere?NumeColoana=${NumeColoana}&Caractere=${Caractere}&TopN=${TopN}&Username=${Username}`)
        .then(raspuns => {
            return raspuns.data;
        })
        .catch(error => {
            console.log(error);
        })
}

const getSugestiiNumeEmail = async (key, value) => {
    let nume = '';
    let username = '';
    if(key === 'nume') {
        nume = value;
    } else if(key === 'username'){
        username = value;
    }
    return await axios
        .get(`Registratura/PersonalListGetByNumeUsername?nume=${nume}&username=${username}`)
        .then(raspuns => {
            return raspuns.data;
        })
        .catch(error => {
            console.log(error);
        })
}

const aniPtDropdown = () => {
    let listaAni = [];
    let anInceput = {
        key: 2020,
        text: '2020',
        value: 2020,
    }
    let anPrezent = moment().year() + 1;
    while (anPrezent > anInceput.value) {
        let anDeRetinut = (anPrezent - 1).toString();
        let an = {
            key: anPrezent - 1,
            text: anDeRetinut,
            value: anPrezent - 1
        }
        listaAni.push(an);
        anPrezent = anPrezent - 1;
    }

    return listaAni;
}

const getZileLibere = async () => {
    return await axios
        .get(`Registratura/ZiLiberaList`)
        .then(response => {
            return response.data
        })
        .catch(error => {
            console.log(error);
        })
}


const getCountCereriTermenLimita = async (ID_Departament, nrZile) => {
    return await axios
        .get(`Registratura/DocumentCountDue?ID_Departament=${ID_Departament}&NrZilePanaLaTermenLimita=${nrZile}`)
        .then(response => {
            return response.data
        })
        .catch(error => {
            console.log(error);
        })
}

const getCountCereriNeprocesate = async () => {
    return await axios
        .get(`Registratura/DocumentNeprocesatCount`)
        .then(response => {
            return response.data
        })
        .catch(error => {
            console.log(error);
        })
}

const getCereriNeprocesate = async () => {
    return await axios
        .get(`Registratura/DocumentNeprocesatList`)
        .then(response => {
            return response.data
        })
        .catch(error => {
            console.log(error);
        })
}

const getDocumentByNrOrdineAni = async (nrDeOrdine, an1, an2) => {
    return await axios
        .get(`Registratura/DocumentBDGetByNrDeOrdineAni?nrDeOrdine=${nrDeOrdine}&ani=${an1}&ani=${an2}`)
        .then(response => {
            return response.data
        })
        .catch(error => {
            console.log(error);
        })
}

const inregistreazaCerereNeprocesata = async (ID_Document) => {
    return await axios
        .post(`Registratura/InregistreazaDocument?ID_Document=${ID_Document}`)
        .then(response => {
            return response.data
        })

}

const updateRegistraturaSetari = async (obiectDeTrimis) => {
    return await axios
        .post(`Registratura/SetariRegistraturaUpdate`, obiectDeTrimis)
        .then(response => {
            toast({
                type: "success",
                icon: "check",
                title: "Setarea a fost salvată cu succes!",
                time: 4000,
            });

            return response.data;
        })
        .catch(error => {
            console.log(error);
            toast({
                type: "error",
                icon: "warning",
                title: "Modificarea nu a fost salvată!",
                time: 4000,
            });
        })
}

const cautareDocument = async (obiect) => {
    return await axios
        .post(`Registratura/DocumentCautare`, obiect)
        .then(response => {
            return response.data;
        })
        .catch(error => {
            console.log(error);
        })
}

const getDocumentByID_Document = async (ID_Document) => {
    return await axios
        .get(`Registratura/DocumentGet?ID_Document=${ID_Document}`)
        .then(response => {
            return response.data;
        })
        .catch(error => {
            console.log(error);
        })
}

const getSetariRegistratura = async () => {
    return await axios
        .get(`Registratura/SetariRegistraturaGet`)
        .then(response => {
            return response.data;
        })
        .catch(error => {
            console.log(error);
        })
}

const getTraseuDocumentByID_Document = async (ID_Document) => {
    return await axios
        .get(`Registratura/TraseuDocumentListByID_Document?ID_Document=${ID_Document}`)
        .then(response => {
            return response.data
        })
}

export {
    getTipuriSolicitari,
    getContinutCereri,
    getModuriTrimitere,
    getDepartamente,
    getStatusuriCerere,
    estePersonalRegistratura,
    EsteInTimpulProgramului,
    getProgramRegistratura,
    formatarePtDropdown,
    getNumeEmailByUsername,
    getDepartamenteDinCareFaceParte,
    getCereriPanaLaTermenLimita,
    getCereriIntrateByDepartament,
    aniPtDropdown,
    getCountCereriIntrate,
    formatareDepPtDropdown,
    getZileLibere,
    getSugestiiColoane,
    getCountCereriNeprocesate,
    getCountCereriTermenLimita,
    getCereriNeprocesate,
    getDocumentByNrOrdineAni,
    inregistreazaCerereNeprocesata,
    formatareNumePtDropdown,
    formatareEmailPtDropdown,
    getSugestiiNumeEmail,
    getCereriIstoricByDepartament,
    formatareDepUserPtDropdown,
    setariRegistratura,
    getCountCereriIstoric,
    updateRegistraturaSetari,
    cautareDocument,
    getDocumentByID_Document,
    getSetariRegistratura,
    getTraseuDocumentByID_Document
}
