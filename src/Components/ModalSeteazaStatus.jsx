import PropTypes from "prop-types";
import {Button, Icon, Modal, Popup} from "semantic-ui-react";
import React, {Component} from "react";

class ModalSeteazaStatus extends Component {

    render() {
        return(
            <Modal
                trigger={
                    <Popup content={"Apăsați pentru a actualiza statusul solicitării curente"} trigger={
                        <Button
                            color={this.props.color}
                            type="button"
                            onClick={this.props.handleModalOpen}>
                            <Icon name={this.props.iconName}/>
                            {this.props.textBtnTriggerModal}
                        </Button>}/>
                }
                open={this.props.modalOpen}
                onClose={this.props.handleModalClose}
                size='fullscreen'
                className="afisareModal"
                closeIcon>
                <Modal.Header icon='right arrow'>{this.props.modalTitle}</Modal.Header>
                <Modal.Content>
                    {this.props.tabelComponent}
                </Modal.Content>
                <Modal.Actions>
                    <Button
                        color='red'
                        type="button"
                        basic
                        onClick={this.props.handleModalClose}>
                        <Icon name='x'/> Închide
                    </Button>
                </Modal.Actions>
            </Modal>
        )}
}

ModalSeteazaStatus.propTypes = {
    handleModalOpen: PropTypes.func.isRequired,
    handleModalClose: PropTypes.func.isRequired,
    modalOpen: PropTypes.bool.isRequired,
    textBtnTriggerModal: PropTypes.string.isRequired,
    modalTitle: PropTypes.string.isRequired,
    tabelComponent: PropTypes.node.isRequired,
    iconName: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired
}

export default ModalSeteazaStatus;