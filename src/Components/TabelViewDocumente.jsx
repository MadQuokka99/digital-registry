import React, {Component} from "react";

import {Button, Dropdown, Icon, Input, Table, Popup, Form, Label} from "semantic-ui-react";
import PropTypes from "prop-types";
import moment from "moment";

import '../Design/PaginaDepuneriCerere.css'; //style pentru Autosuggest.js
import '../Design/TableStyle.css';

import axios from "../Utils/axios-AGSIS-API";
import {toast} from "react-semantic-toasts";
import Autosuggest from "react-autosuggest";

import ModalVizualizareInregistrare from "./ModalVizualizareInregistrare";
import {
    formatarePtDropdown, getDocumentByID_Document,
    getTipuriSolicitari,
    inregistreazaCerereNeprocesata
} from "../Utils/ApiCalls";
import TabelAddItemTraseu from "./TabelAddItemTraseu";
import TabelVeziTraseu from "./TabelVeziTraseu";
import {
    formatareTextEsteRevenireRaspuns,
    getSugestii,
    getSugestiiNumeEmailAutosuggest
} from "../Utils/FunctiiAutosuggest";
import {cautaCerereServerSide} from "../Utils/FunctiiIstoricCereriIntrate";
import ModalEsteRaspunsRevenire from "./ModalEsteRaspunsRevenire";
import ModalSeteazaTraseu from "./ModalSeteazaTraseu";
import ModalVeziTraseu from "./ModalVeziTraseu";


function getSuggestionValueName(suggestion) {
    return suggestion.nume;
}

function renderSuggestionValueName(suggestion) {
    return (
        <span>{suggestion.nume}</span>
    );
}

function getSuggestionValueEmail(suggestion) {
    return suggestion.email;
}

function renderSuggestionValueEmail(suggestion) {
    return (
        <span>{suggestion.email}</span>
    );
}

function getSuggestionValue(suggestion) {
    return suggestion;
}

function renderSuggestion(suggestion) {
    return (
        <span>{suggestion}</span>
    );
}

class TabelViewDocumente extends Component {

    state = {
        idCerereInEditare: null,
        btnEditeazaClicked: false,
        cerere: null,
        calendarFocused: false,

        idDocumentVeziTraseu: null,

        modalActulPleacaLaOpen: false,
        modalVeziTraseuOpen: false,
        modalEsteRaspunsLaOpen: false,
        modalEsteRevenireLaOpen: false,

        //pentru Autosuggest pentru nume angajat
        numeAngajatValue: '',
        optiuniNumeAngajat: [],

        //pentru Autosuggest pentru nume deponent
        numeDeponentValue: '',
        optiuniNumeDeponent: [],

        //pentru Autosuggest pentru email deponent
        emailDeponentValue: '',
        optiuniEmailDeponent: [],

        //pentru Autosuggest pentru email angajat
        emailAngajatValue: '',
        optiuniEmailAngajat: [],

        continutValue: '', //pentru implementarea Autosuggest.js pentru continut
        optiuniContinut: [], //array ce contine continutul pentru dropdown-ul de 'Alege continut cerere' pentru depunerile catre Rectorat

        autoritateValue: '',
        optiuniAutoritate: [],

        catreCinePleacaValue: '',
        optiuniCatreCinePleaca: [],

        listaCereriServerSide: [],
        aniPtMetodaGetDocByNrDeOrdine: {
            an1: moment().year(),
            an2: moment().year() - 1
        },
        esteRaspunsLaText: '',
        esteRevenireLaText: '',

        tipuriCereri: []
    }

    componentDidMount() {
        getTipuriSolicitari()
            .then(tipuriSolicitari => {
                this.setState({
                    tipuriCereri: formatarePtDropdown(tipuriSolicitari, "ID_TipDocument", "DenumireTipDocument"),
                })
            })
    }


    handleBtnEditeazaClicked = (cerere) => {

        this.setState({
            esteRevenireLaText: '',
            esteRaspunsLaText: ''
        })

        let mesajAlert;

        if (this.props.ePersonalRegistratura) {
            mesajAlert = "Sunteți sigur/ă că renunțați la modificări?"
        } else {
            mesajAlert = "Ați terminat editarea solicitării?"
        }

        if (this.state.idCerereInEditare !== null) {
            let ok = window.confirm(mesajAlert);
            if (ok === false) {
                this.setState({
                    cerere: null,
                    emailDeponentValue: '',
                    numeDeponentValue: '',
                    numeAngajatValue: '',
                    emailAngajatValue: '',
                    continutValue: '',
                    autoritateValue: '',
                    catreCinePleacaValue: '',
                    esteRevenireLaText: '',
                    esteRaspunsLaText: ''
                })
                return;
            } else {
                getDocumentByID_Document(cerere.ID_Document)
                    .then(documentUpdated => {
                        this.updateObiectCerereInIstoric(documentUpdated);
                    })
            }
        }

        if (cerere.EsteRaspunsLa_ID_Document !== null) {
            getDocumentByID_Document(cerere.EsteRaspunsLa_ID_Document)
                .then(cerereRaspunsRevenireInfo => {
                    this.setState({
                        esteRaspunsLaText: formatareTextEsteRevenireRaspuns(cerereRaspunsRevenireInfo)
                    })
                })
                .catch(error => console.log(error))

        }

        if (cerere.EsteRevenireLa_ID_Document !== null) {
            getDocumentByID_Document(cerere.EsteRevenireLa_ID_Document)
                .then(cerereRaspunsRevenireInfo => {
                    this.setState({
                        esteRevenireLaText: formatareTextEsteRevenireRaspuns(cerereRaspunsRevenireInfo)
                    })
                })
                .catch(error => console.log(error))
        }

        this.setState(prevstate => ({
            cerere: cerere,
            emailDeponentValue: cerere.MailDeponent,
            numeDeponentValue: cerere.NumeDeponent,
            numeAngajatValue: cerere.NumeAngajatDeponent,
            emailAngajatValue: cerere.MailAngajatDeponent,
            continutValue: cerere.ContinutulActului,
            autoritateValue: cerere.CineAEmis_Autoritate,
            catreCinePleacaValue: cerere.TrimiteLaInstitutia,
            idCerereInEditare: prevstate.idCerereInEditare === cerere.ID_Document ? null : cerere.ID_Document,
        }))
    }

    handleActualizeazaForm = (key, value) => {
        const cerereCopy = {...this.state.cerere};

        if (key === "departamentEmitent") {
            let listaDepartamenteCopy = this.props.listaDepartamente.filter(dep => value === dep.key);
            cerereCopy["CineAEmis_ID_Departament"] = listaDepartamenteCopy[0].value;
            this.setState({
                cerere: cerereCopy
            })
        } else {
            if (key === "TermenLimita") {
                value = moment(value).format("YYYY-MM-DD");
            }

            cerereCopy[key] = value;
            this.setState({
                cerere: cerereCopy
            })
        }
    }

    handleRezultatAlesDinDropdownRaspunsRevenire = (cerere, tipSolicitare) => {
        let cerereCopy = {...this.state.cerere};

        cerereCopy[tipSolicitare] = cerere;
        this.setState({
            cerere: cerereCopy,
            listaCereriServerSide: []
        })

        if (tipSolicitare === "EsteRevenireLa_ID_Document") {
            this.setState({
                esteRevenireLaText: "",
            })
        } else if (tipSolicitare === 'EsteRaspunsLa_ID_Document') {
            this.setState({
                esteRaspunsLaText: ""
            })
        }

        let cerereAleasa = null;
        let listaIstoricCopy = [...this.state.listaCereriServerSide];

        for (let i = 0; i < listaIstoricCopy.length; i++) {
            if (listaIstoricCopy[i].key === cerere) {
                cerereAleasa = listaIstoricCopy[i].text;
                if (tipSolicitare === "EsteRevenireLa_ID_Document") {
                    this.setState({
                        esteRevenireLaText: cerereAleasa
                    })
                } else if (tipSolicitare === 'EsteRaspunsLa_ID_Document') {
                    this.setState({
                        esteRaspunsLaText: cerereAleasa
                    })
                }
            }
        }
    }

    cautaCerereServerSide = (nrDeOrdine) => {
        cautaCerereServerSide(nrDeOrdine, this.state.aniPtMetodaGetDocByNrDeOrdine)
            .then(listaCereri => {
                this.setState({
                    listaCereriServerSide: listaCereri
                })
            })
    }

    handleActualizeazaNumeEmail = (key1, value1, key2, value2) => {

        const cerereCopy = {...this.state.cerere};
        let updatedFormElementName = {
            ...cerereCopy[key1]
        };
        updatedFormElementName = value1;
        cerereCopy[key1] = updatedFormElementName;
        let updatedFormElementEmail = {
            ...cerereCopy[key2]
        };
        updatedFormElementEmail = value2;
        cerereCopy[key2] = updatedFormElementEmail;
        this.setState({
            cerere: cerereCopy
        });
    }

    handleBtnFinalizeazaClicked = () => {
        this.setState({
            idCerereInEditare: null
        })
        let cerereDeActualizat = {...this.state.cerere};
        cerereDeActualizat.ID_Status = null;
        this.updateCerere(cerereDeActualizat);
    }


    updateCerere = (cerereDeActualizat) => {

        axios
            .post(`Registratura/UpdateDocument`, cerereDeActualizat)
            .then(response => {
                let succesfulUpdate = response.data;

                this.setState({
                    emailValue: '',
                    nameValue: '',
                    continutValue: ''
                })

                toast({
                    type: "success",
                    icon: "check",
                    title: "Editarea a fost realizată cu succes!",
                    time: 4000,
                });
                this.updateObiectCerereInIstoric(succesfulUpdate);
            })
            .catch(error => {
                console.log(error);
                toast({
                    type: "error",
                    icon: "warning",
                    title: "Modificările nu au fost salvate!",
                    time: 4000,
                });
            })
    }

    updateObiectCerereInIstoric = (responsePost) => {

        let listaIstoricCopy = [...this.props.istoricCereri];
        for (let i = 0; i < listaIstoricCopy.length; i++) {
            if (responsePost.ID_Document === listaIstoricCopy[i].ID_Document) {
                listaIstoricCopy[i] = responsePost;
            }
        }
        this.props.actualizeazaInIstoricCerere(listaIstoricCopy);
    }


    handleInregistreazaCerereNeprocesata = (ID_Document) => {
        inregistreazaCerereNeprocesata(ID_Document)
            .then(response => {
                this.props.getInfoSolicitariNeprocesate();
                toast({
                    type: "success",
                    icon: "check",
                    title: "Procesarea a fost realizată cu succes!",
                    time: 4000,
                });
            })
            .catch(error => {
                toast({
                    type: "error",
                    icon: "warning",
                    title: error.response.data,
                    time: 4000,
                });
                console.log(error.response.data);
            })
    }

    handleModalActulPleacaLaOpen = () => {
        this.setState({
            modalActulPleacaLaOpen: true
        })
    }

    handleModalActulPleacaLaClose = () => {
        this.setState({
            modalActulPleacaLaOpen: false
        })
    }

    handleModalEsteRaspunsLaOpen = () => {
        this.setState({
            modalEsteRaspunsLaOpen: true
        })
    }
    handleModalEsteRaspunsLaClose = () => {
        this.setState({
            modalEsteRaspunsLaOpen: false
        })
    }

    handleModalEsteRevenireLaOpen = () => {
        this.setState({
            modalEsteRevenireLaOpen: true
        })
    }
    handleModalEsteRevenireLaClose = () => {
        this.setState({
            modalEsteRevenireLaOpen: false
        })
    }

    handleModalVeziTraseuLaOpen = (ID_Document) => {
        this.setState({
            modalVeziTraseuOpen: true,
            idDocumentVeziTraseu: ID_Document
        })
    }

    handleModalVeziTraseuClose = () => {
        this.setState({
            modalVeziTraseuOpen: false
        })
    }

    //pentru Autosuggest de nume deponent
    getSugestiiNumeDeponent = ({value}) => {
        getSugestii(value, "NumeDeponent", this.props.detaliiUser.email)
            .then(optiuniNumeDeponentResponse => {
                this.setState({
                    optiuniNumeDeponent: optiuniNumeDeponentResponse
                })
            })
    }

    clearSugestiiNumeDeponent = () => {
        this.setState({
            optiuniNumeDeponent: []
        })
    }

    //pentru Autosuggest de email deponent
    getSugestiiEmailDeponent = ({value}) => {
        getSugestii(value, "MailDeponent", this.props.detaliiUser.email)
            .then(optiuniEmailDeponentResponse => {
                this.setState({
                    optiuniEmailDeponent: optiuniEmailDeponentResponse
                })
            })
    }

    clearSugestiiEmailDeponent = () => {
        this.setState({
            optiuniEmailDeponent: []
        })
    }

    //pentru Autosuggest nume angajat
    getSugestiiNume = ({value}) => {
        getSugestiiNumeEmailAutosuggest(value, 'nume')
            .then(sugestiiNume => {
                this.setState({
                    optiuniNumeAngajat: sugestiiNume
                })
            })
    }

    clearSugestiiNume = () => {
        this.setState({
            optiuniNumeAngajat: []
        })
    }

    onNameAngajatSuggestionSelected = (event, {suggestion}) => {
        this.handleActualizeazaNumeEmail("NumeAngajatDeponent", suggestion.nume, "MailAngajatDeponent", suggestion.email);
        this.setState({
            emailAngajatValue: suggestion.email
        });
    };

    //pentru Autosuggest email angajat
    getSugestiiEmail = ({value}) => {
        getSugestiiNumeEmailAutosuggest(value, 'username')
            .then(sugestiiEmail => {
                this.setState({
                    optiuniEmailAngajat: sugestiiEmail
                })
            })
    }

    clearSugestiiEmail = () => {
        this.setState({
            optiuniEmailAngajat: []
        })
    }

    onEmailAngajatSuggestionSelected = (event, {suggestion}) => {
        this.handleActualizeazaNumeEmail("NumeAngajatDeponent", suggestion.nume, "MailAngajatDeponent", suggestion.email);
        this.setState({
            numeAngajatValue: suggestion.nume
        });
    };

    getSugestiiContinut = ({value}) => {
        getSugestii(value, "ContinutulActului", this.props.detaliiUser.email)
            .then(optiuniContinutResponse => {
                this.setState({
                    optiuniContinut: optiuniContinutResponse
                })
            })
    }

    clearSugestiiContinut = () => {
        this.setState({
            optiuniContinut: []
        })
    }

    getSugestiiAutoritateEmitenta = ({value}) => {
        getSugestii(value, "CineAEmis_Autoritate", this.props.detaliiUser.email)
            .then(optiuniAutoritateResponse => {
                this.setState({
                    optiuniAutoritate: optiuniAutoritateResponse
                })
            })
    }

    clearSugestiiAutoritateEmitenta = () => {
        this.setState({
            optiuniAutoritate: []
        })
    }

    getSugestiiCatreCinePleaca = ({value}) => {

        getSugestii(value, "TrimiteLaInstitutia", this.props.detaliiUser.email)
            .then(optiuniCatreCinePleacaResponse => {
                this.setState({
                    optiuniCatreCinePleaca: optiuniCatreCinePleacaResponse
                })
            })
    }

    clearSugestiiCatreCinePleaca = () => {
        this.setState({
            optiuniCatreCinePleaca: []
        })
    }

    handleStergeEsteRaspunsRevenireLa = (esteRaspuns) => {
        let cerereCopy = {...this.state.cerere};

        if (esteRaspuns) {
            cerereCopy.EsteRaspunsLa_ID_Document = null;
            cerereCopy.EsteRaspunsLa_NrOrdine = null

            this.setState({
                cerere: cerereCopy,
                esteRaspunsLaText: ''
            })
        }

        if (!esteRaspuns) {
            cerereCopy.EsteRevenireLa_ID_Document = null;
            cerereCopy.EsteRevenireLa_NrOrdine = null

            this.setState({
                cerere: cerereCopy,
                esteRevenireLaText: ''
            })
        }
    }

    handleReseteazaEsteRaspunsRevenireLa = (esteRaspuns) => {
        let cerereCopy = {...this.state.cerere};
        getDocumentByID_Document(cerereCopy.ID_Document)
            .then(cerereInfo => {
                if (esteRaspuns) {
                    cerereCopy.EsteRaspunsLa_ID_Document = cerereInfo.EsteRaspunsLa_ID_Document;
                    cerereCopy.EsteRaspunsLa_NrOrdine = cerereInfo.EsteRaspunsLa_NrOrdine;

                    if (cerereInfo.EsteRaspunsLa_ID_Document !== null) {
                        getDocumentByID_Document(cerereCopy.EsteRaspunsLa_ID_Document)
                            .then(cerereRaspunsInfo => {
                                this.setState({
                                    cerere: cerereCopy,
                                    esteRaspunsLaText: formatareTextEsteRevenireRaspuns(cerereRaspunsInfo)
                                })
                            })
                    } else {
                        this.setState({
                            cerere: cerereCopy,
                            esteRaspunsLaText: ''
                        })
                    }
                }
                if (!esteRaspuns) {
                    cerereCopy.EsteRevenireLa_ID_Document = cerereInfo.EsteRevenireLa_ID_Document;
                    cerereCopy.EsteRevenireLa_NrOrdine = cerereInfo.EsteRevenireLa_NrOrdine;

                    if (cerereInfo.EsteRevenireLa_ID_Document !== null) {
                        getDocumentByID_Document(cerereCopy.EsteRevenireLa_ID_Document)
                            .then(cerereRevenireInfo => {
                                this.setState({
                                    cerere: cerereCopy,
                                    esteRevenireLaText: formatareTextEsteRevenireRaspuns(cerereRevenireInfo)
                                })
                            })
                    } else {
                        this.setState({
                            cerere: cerereCopy,
                            esteRevenireLaText: ''
                        })
                    }
                }
            })
    }

    trimiteReminder = (ID_Document) => {
        axios
            .post(`Registratura/TrimiteMailReminder?ID_Document=${ID_Document}`)
            .then(response => {

                toast({
                    type: "success",
                    icon: "check",
                    title: "Reminderul a fost trimis cu succes!",
                    time: 4000,
                });

            })
            .catch(error => {
                console.log(error);
                toast({
                    type: "error",
                    icon: "warning",
                    title: error.response.data + ". Reminderul nu a fost trimis!",
                    time: 4000,
                });
            })
    }

    render() {
        const {
            numeDeponentValue,
            optiuniNumeDeponent,
            emailDeponentValue,
            optiuniEmailDeponent,
            numeAngajatValue,
            optiuniNumeAngajat,
            emailAngajatValue,
            optiuniEmailAngajat,
            continutValue,
            optiuniContinut,
            autoritateValue,
            optiuniAutoritate,
            optiuniCatreCinePleaca,
            catreCinePleacaValue
        } = this.state;

        const numeDeponentInputProps = {
            placeholder: "Nume solicitant",
            value: numeDeponentValue,
            onChange: (event, {newValue}) => {
                this.handleActualizeazaForm("NumeDeponent", newValue)
                this.setState({
                    numeDeponentValue: newValue
                })
            }
        };
        const emailDeponentInputProps = {
            placeholder: "Email solicitant",
            value: emailDeponentValue,
            onChange: (event, {newValue}) => {
                this.handleActualizeazaForm("MailDeponent", newValue)
                this.setState({
                    emailDeponentValue: newValue
                })
            }
        };
        const numeAngajatInputProps = {
            placeholder: "Nume angajat deponent",
            value: numeAngajatValue,
            onChange: (event, {newValue}) => {
                this.handleActualizeazaForm("NumeAngajatDeponent", newValue)
                this.setState({
                    numeAngajatValue: newValue
                })
            }
        };
        const emailAngajatInputProps = {
            placeholder: "Email angajat deponent",
            value: emailAngajatValue,
            onChange: (event, {newValue}) => {
                this.handleActualizeazaForm("MailAngajatDeponent", newValue)
                this.setState({
                    emailAngajatValue: newValue
                })
            }
        };

        const continutInputProps = {
            placeholder: "Conținutul actului",
            value: continutValue,
            onChange: (event, {newValue}) => {
                this.handleActualizeazaForm("ContinutulActului", newValue)
                this.setState({
                    continutValue: newValue
                })
            }
        };

        const autoritateInputProps = {
            placeholder: "Autoritate emitentă",
            value: autoritateValue,
            onChange: (event, {newValue}) => {
                this.handleActualizeazaForm("CineAEmis_Autoritate", newValue)
                this.setState({
                    autoritateValue: newValue
                })
            }
        };

        const catreCinePleacaInputProps = {
            placeholder: "Către cine pleacă",
            value: catreCinePleacaValue,
            onChange: (event, {newValue}) => {
                this.handleActualizeazaForm("TrimiteLaInstitutia", newValue)
                this.setState({
                    catreCinePleacaValue: newValue
                })
            }
        };
        return (
            <Table compact className="tableScroll">
                <Table.Header>
                    <Table.Row textAlign="center">
                        {this.props.afiseazaBtnTrimiteReminder &&
                        <Table.HeaderCell></Table.HeaderCell>}
                        {this.props.afiseazaBtnInregistreaza ?
                            <Table.HeaderCell></Table.HeaderCell>
                            : <Table.HeaderCell></Table.HeaderCell> &&
                            <Table.HeaderCell></Table.HeaderCell>}
                        <Table.HeaderCell>Data înregistrare</Table.HeaderCell>
                        <Table.HeaderCell>Tipul solicitării</Table.HeaderCell>
                        <Table.HeaderCell>Numărul de ordine</Table.HeaderCell>
                        <Table.HeaderCell>Este răspuns la</Table.HeaderCell>
                        <Table.HeaderCell>Departament emitent/facultate emitentă</Table.HeaderCell>
                        <Table.HeaderCell>Stadiul actului</Table.HeaderCell>
                        <Table.HeaderCell>Conținutul actului</Table.HeaderCell>
                        <Table.HeaderCell>Termen limită</Table.HeaderCell>
                        <Table.HeaderCell>Traseu intern document</Table.HeaderCell>
                        <Table.HeaderCell>Actul se află la</Table.HeaderCell>
                        <Table.HeaderCell>Nume solicitant</Table.HeaderCell>
                        <Table.HeaderCell>E-mail solicitant</Table.HeaderCell>
                        <Table.HeaderCell>Autoritate emitentă</Table.HeaderCell>
                        <Table.HeaderCell>Este revenire la</Table.HeaderCell>
                        <Table.HeaderCell>Numărul actului intrat</Table.HeaderCell>
                        <Table.HeaderCell>Către cine pleacă extern</Table.HeaderCell>
                        <Table.HeaderCell>Modalitate de trimitere</Table.HeaderCell>
                        <Table.HeaderCell>Observații solicitant</Table.HeaderCell>
                        <Table.HeaderCell>Mențiuni Registratură</Table.HeaderCell>
                        <Table.HeaderCell>Nume angajat care înregistrează</Table.HeaderCell>
                        <Table.HeaderCell>E-mail angajat care înregistrează</Table.HeaderCell>
                        {this.props.ePersonalRegistratura &&
                        <Table.HeaderCell></Table.HeaderCell>}
                        {/*<Table.HeaderCell>*/}
                        {/*    Încarcă fișier*/}
                        {/*</Table.HeaderCell>*/}
                        {/*<Table.HeaderCell>Istoric fișiere încărcate</Table.HeaderCell>*/}
                    </Table.Row>
                </Table.Header>

                <Table.Body>
                    {this.props.istoricCereri.map((item) => {
                        return (
                            <Table.Row key={item.ID_Document}
                                       className={(moment(item.TermenLimita) <= moment() && item.ID_Status !== 8) ? "depasitTermenLimita" :
                                           (item.ID_Status === 8 || item.ID_Status === 9) ? "finalizat" :
                                               (moment(item.TermenLimita) > moment() && (item.ID_Status !== 8)) ? "inTermenLimita" : null}>
                                {this.props.afiseazaBtnTrimiteReminder &&
                                <Table.Cell>
                                    <Button type="button" color="green" compact
                                            onClick={() => this.trimiteReminder(item.ID_Document)}>
                                        <Icon name="send"/>
                                        Trimite reminder
                                    </Button>
                                </Table.Cell>}
                                {this.props.afiseazaBtnInregistreaza && !item.EsteInregistrat &&
                                <Table.Cell>
                                    <Button color="green" type="button"
                                            onClick={() => this.handleInregistreazaCerereNeprocesata(item.ID_Document)}>
                                        <Icon name="check"/>
                                        Înregistrează
                                    </Button>
                                </Table.Cell>}

                                {(!item.EsteInregistrat && this.props.afiseazaBtnInregistreaza) ? null
                                    : (!item.EsteInregistrat && !this.props.ePersonalRegistratura) ? null
                                        : !item.EsteInregistrat ?
                                            <Table.Cell></Table.Cell>
                                            : (<Table.Cell>
                                                    <Popup disabled={this.props.ePersonalRegistratura}
                                                           content={(!this.props.ePersonalRegistratura && this.state.idCerereInEditare === item.ID_Document) ?
                                                               "Apăsați pentru a termina editarea solicitării curente" : "Apăsați butonul pentru a putea edita traseul și statusul documentului"}
                                                           trigger={
                                                               <Button
                                                                   type="button"
                                                                   color={this.state.idCerereInEditare === item.ID_Document ? "red" : "green"}
                                                                   disabled={(this.state.idCerereInEditare === null ? false : this.state.idCerereInEditare !== item.ID_Document)}
                                                                   onClick={() => this.handleBtnEditeazaClicked(item)}>
                                                                   {
                                                                       (this.props.ePersonalRegistratura && this.state.idCerereInEditare === item.ID_Document) ? "Anulează" :
                                                                           (!this.props.ePersonalRegistratura && this.state.idCerereInEditare === item.ID_Document) ? "Termină editarea"
                                                                               : "Editează"
                                                                   }
                                                               </Button>}/>
                                                </Table.Cell>)}
                                <Table.Cell>{item.EsteInregistrat ? moment(item.DataInregistrare).format("DD-MM-YYYY") : "-"}</Table.Cell>
                                <Table.Cell>{(this.props.ePersonalRegistratura && this.state.idCerereInEditare === item.ID_Document) ?
                                    <Dropdown
                                        selection
                                        defaultValue={item.ID_TipDocument}
                                        label="Tipul solicitării" search
                                        options={this.state.tipuriCereri} width={1}
                                        onChange={((e, data) => this.handleActualizeazaForm('ID_TipDocument', data.value))}/>
                                    : item.DenumireTipDocument
                                }</Table.Cell>
                                <Table.Cell>{this.props.afiseazaBtnInregistreaza ? null : !item.EsteInregistrat ?
                                    <Popup trigger={
                                        <Icon name="warning circle"
                                              color="red"
                                        />
                                    }
                                           content="Cererea dvs a fost salvată cu succes, va fi înregistrată și va primi un număr de înregistrare în următoarea zi lucrătoare"
                                    />
                                    : item.EsteInregistrat ? item.NrDeOrdine : null}</Table.Cell>
                                <Table.Cell>
                                    {(this.props.ePersonalRegistratura && this.state.idCerereInEditare === item.ID_Document) ?
                                        <ModalEsteRaspunsRevenire handleModalOpen={this.handleModalEsteRaspunsLaOpen}
                                                                  handleModalClose={this.handleModalEsteRaspunsLaClose}
                                                                  messageBtn={"Este răspuns la"}
                                                                  messageHeader={"Actul este răspuns la"}
                                                                  modalOpen={this.state.modalEsteRaspunsLaOpen}
                                                                  listaCereriServerSide={this.state.listaCereriServerSide}
                                                                  handleRezultatAlesDinDropdownRaspunsRevenire={this.handleRezultatAlesDinDropdownRaspunsRevenire}
                                                                  tipSolicitare={"EsteRaspunsLa_ID_Document"}
                                                                  cautaCerereServerSide={this.cautaCerereServerSide}
                                                                  labelMessage={'Solicitarea este răspuns la:'}
                                                                  textRevenireRaspuns={this.state.esteRaspunsLaText}
                                                                  popupContent={"Apăsarea acestui buton va șterge mențiunea că solicitarea actuală este răspuns la o altă solicitare"}
                                                                  handleReseteazaEsteRaspunsRevenireLa={this.handleReseteazaEsteRaspunsRevenireLa}
                                                                  handleStergeEsteRaspunsRevenireLa={this.handleStergeEsteRaspunsRevenireLa}
                                        />

                                        : (item.EsteRaspunsLa_NrOrdine !== null &&
                                            <ModalVizualizareInregistrare
                                                ID_Document={item.EsteRaspunsLa_ID_Document}
                                                NrDeOrdineTitlu={item.EsteRaspunsLa_NrOrdine}
                                            />)}
                                </Table.Cell>

                                <Table.Cell
                                    className={(this.props.ePersonalRegistratura && this.state.idCerereInEditare === item.ID_Document) ? "" : "textDepasesteDimensiune"}
                                    title={item.CineAEmis_NumeDepartament}>
                                    {(this.props.ePersonalRegistratura && this.state.idCerereInEditare === item.ID_Document) ?
                                        <Dropdown selection
                                                  width={4}
                                                  options={this.props.listaDepartamente}
                                                  search
                                                  defaultValue={item.CineAEmis_ID_Departament}
                                                  onChange={((e, data) => this.handleActualizeazaForm('departamentEmitent', data.value))}/>
                                        : item.CineAEmis_NumeDepartament}</Table.Cell>


                                <Table.Cell>{item.DenumireStatus}</Table.Cell>

                                <Table.Cell
                                    className={(this.props.ePersonalRegistratura && this.state.idCerereInEditare === item.ID_Document) ? "" : "textDepasesteDimensiune"}
                                    title={item.ContinutulActului}>
                                    {(this.props.ePersonalRegistratura && this.state.idCerereInEditare === item.ID_Document)
                                        ? <Form>
                                            <Autosuggest
                                                suggestions={optiuniContinut}
                                                onSuggestionsFetchRequested={this.getSugestiiContinut}
                                                onSuggestionsClearRequested={this.clearSugestiiContinut}
                                                inputProps={continutInputProps}
                                                getSuggestionValue={getSuggestionValue}
                                                renderSuggestion={renderSuggestion}
                                            />
                                        </Form>
                                        : item.ContinutulActului}
                                </Table.Cell>
                                <Table.Cell>
                                    {(this.props.ePersonalRegistratura && this.state.idCerereInEditare === item.ID_Document) ?
                                        <input type="date" min={moment().format("YYYY-MM-DD")}
                                               defaultValue={moment(item.TermenLimita).format("YYYY-MM-DD")}
                                               onChange={(event) => this.handleActualizeazaForm("TermenLimita", event.target.value)}
                                        />
                                        : item.EsteInregistrat ? moment(item.TermenLimita).format("DD-MM-YYYY") : "-"}
                                </Table.Cell>

                                {this.state.idCerereInEditare === item.ID_Document ?
                                    <Table.Cell>
                                        <ModalSeteazaTraseu
                                            handleModalClose={this.handleModalActulPleacaLaClose}
                                            handleModalOpen={this.handleModalActulPleacaLaOpen}
                                            modalOpen={this.state.modalActulPleacaLaOpen}
                                            modalTitle={"Setează Traseu Intern Document"}
                                            color={"green"}
                                            extraInfo={" -> Apasă butonul 'Setează status' pentru a actualiza statusul!"}
                                            tabelComponent={
                                                <TabelAddItemTraseu listaDepartamente={this.props.listaDepartamente}
                                                                    ID_Document={item.ID_Document}
                                                                    username={this.props.detaliiUser.email}
                                                                    ePersonalRegistratura={this.props.ePersonalRegistratura}
                                                />
                                            }
                                            stadiiCereri={this.props.stadiiCereri}
                                            ID_Document={item.ID_Document}
                                            username={this.props.detaliiUser.email}
                                            ePersonalRegistratura={this.props.ePersonalRegistratura}
                                            iconName={"pin"}
                                            textBtnTriggerModal={"Setează traseu"}/>
                                    </Table.Cell>
                                    :
                                    <Table.Cell>
                                        <Button
                                            compact
                                            type="button"
                                            onClick={() => this.handleModalVeziTraseuLaOpen(item.ID_Document)}
                                            size="tiny">
                                            <Icon name="sitemap"/>
                                            Vezi traseu
                                        </Button>

                                        {this.state.idDocumentVeziTraseu === item.ID_Document &&
                                        <ModalVeziTraseu
                                            extraInfo={" -> Apasă butonul 'Editează' din dreptul solicitării pentru a actualiza traseul!"}
                                            handleModalClose={this.handleModalVeziTraseuClose}
                                            modalOpen={this.state.modalVeziTraseuOpen}
                                            tabelComponent={
                                                <TabelVeziTraseu ID_Document={item.ID_Document}/>
                                            }
                                            modalTitle={"Vezi Traseu Intern Document"}
                                            ID_Document={item.ID_Document}
                                        />
                                        }
                                    </Table.Cell>}
                                <Table.Cell
                                    className={(this.props.ePersonalRegistratura && this.state.idCerereInEditare === item.ID_Document) ? "" : "textDepasesteDimensiune"}
                                    title={item.PredatLa_NumeDepartament}>
                                    {item.PredatLa_NumeDepartament}
                                </Table.Cell>
                                <Table.Cell
                                    className={(this.props.ePersonalRegistratura && this.state.idCerereInEditare === item.ID_Document) ? "" : "textDepasesteDimensiune"}
                                    title={item.NumeDeponent}>{(this.props.ePersonalRegistratura && this.state.idCerereInEditare === item.ID_Document)
                                    ?
                                    <Form>
                                        <Autosuggest
                                            suggestions={optiuniNumeDeponent}
                                            onSuggestionsFetchRequested={this.getSugestiiNumeDeponent}
                                            onSuggestionsClearRequested={this.clearSugestiiNumeDeponent}
                                            inputProps={numeDeponentInputProps}
                                            getSuggestionValue={getSuggestionValue}
                                            renderSuggestion={renderSuggestion}
                                        />
                                    </Form>

                                    : item.NumeDeponent}</Table.Cell>
                                <Table.Cell>{(this.props.ePersonalRegistratura && this.state.idCerereInEditare === item.ID_Document)
                                    ?
                                    <Form>
                                        <Autosuggest
                                            suggestions={optiuniEmailDeponent}
                                            onSuggestionsFetchRequested={this.getSugestiiEmailDeponent}
                                            onSuggestionsClearRequested={this.clearSugestiiEmailDeponent}
                                            inputProps={emailDeponentInputProps}
                                            getSuggestionValue={getSuggestionValue}
                                            renderSuggestion={renderSuggestion}
                                        />
                                    </Form>
                                    : item.MailDeponent}</Table.Cell>

                                <Table.Cell
                                    className={(this.props.ePersonalRegistratura && this.state.idCerereInEditare === item.ID_Document) ? "" : "textDepasesteDimensiune"}
                                    title={item.CineAEmis_Autoritate}>
                                    {(this.props.ePersonalRegistratura && this.state.idCerereInEditare === item.ID_Document) ?
                                        <Form>
                                            <Autosuggest
                                                suggestions={optiuniAutoritate}
                                                onSuggestionsFetchRequested={this.getSugestiiAutoritateEmitenta}
                                                onSuggestionsClearRequested={this.clearSugestiiAutoritateEmitenta}
                                                inputProps={autoritateInputProps}
                                                getSuggestionValue={getSuggestionValue}
                                                renderSuggestion={renderSuggestion}
                                            />
                                        </Form>
                                        : item.CineAEmis_Autoritate}
                                </Table.Cell>

                                <Table.Cell>
                                    {(this.props.ePersonalRegistratura && this.state.idCerereInEditare === item.ID_Document) ?

                                        <ModalEsteRaspunsRevenire handleModalOpen={this.handleModalEsteRevenireLaOpen}
                                                                  handleModalClose={this.handleModalEsteRevenireLaClose}
                                                                  messageBtn={"Este revenire la"}
                                                                  messageHeader={"Actul este revenire la"}
                                                                  modalOpen={this.state.modalEsteRevenireLaOpen}
                                                                  listaCereriServerSide={this.state.listaCereriServerSide}
                                                                  handleRezultatAlesDinDropdownRaspunsRevenire={this.handleRezultatAlesDinDropdownRaspunsRevenire}
                                                                  tipSolicitare={"EsteRevenireLa_ID_Document"}
                                                                  cautaCerereServerSide={this.cautaCerereServerSide}
                                                                  labelMessage={'Solicitarea este revenire la:'}
                                                                  textRevenireRaspuns={this.state.esteRevenireLaText}
                                                                  popupContent={"Apăsarea acestui buton va șterge mențiunea că solicitarea actuală este revenire la o altă solicitare"}
                                                                  handleReseteazaEsteRaspunsRevenireLa={this.handleReseteazaEsteRaspunsRevenireLa}
                                                                  handleStergeEsteRaspunsRevenireLa={this.handleStergeEsteRaspunsRevenireLa}
                                        />
                                        : (item.EsteRevenireLa_NrOrdine !== null &&
                                            <ModalVizualizareInregistrare
                                                ID_Document={item.EsteRevenireLa_ID_Document}
                                                NrDeOrdineTitlu={item.EsteRevenireLa_NrOrdine}
                                            />)
                                    }
                                </Table.Cell>

                                <Table.Cell title={item.NrActuluiIntrat}
                                            className={(this.props.ePersonalRegistratura && this.state.idCerereInEditare === item.ID_Document) ? "" : "textDepasesteDimensiune"}>
                                    {(this.props.ePersonalRegistratura && this.state.idCerereInEditare === item.ID_Document) ?
                                        <Input width={2}
                                               defaultValue={item.NrActuluiIntrat}
                                               onChange={(event) => this.handleActualizeazaForm('NrActuluiIntrat', event.target.value)}/>
                                        : item.NrActuluiIntrat}</Table.Cell>
                                <Table.Cell
                                    className={(this.props.ePersonalRegistratura && this.state.idCerereInEditare === item.ID_Document) ? "" : "textDepasesteDimensiune"}
                                    title={item.TrimiteLaInstitutia}>
                                    {(this.props.ePersonalRegistratura && this.state.idCerereInEditare === item.ID_Document) ?
                                        <Form>
                                            <Autosuggest
                                                suggestions={optiuniCatreCinePleaca}
                                                onSuggestionsFetchRequested={this.getSugestiiCatreCinePleaca}
                                                onSuggestionsClearRequested={this.clearSugestiiCatreCinePleaca}
                                                inputProps={catreCinePleacaInputProps}
                                                getSuggestionValue={getSuggestionValue}
                                                renderSuggestion={renderSuggestion}
                                            />
                                        </Form>
                                        : item.TrimiteLaInstitutia
                                    }</Table.Cell>

                                <Table.Cell>{
                                    (this.props.ePersonalRegistratura && this.state.idCerereInEditare === item.ID_Document)
                                        ?
                                        <Dropdown
                                            selection label="Modalitate de trimitere"
                                            width={6}
                                            options={this.props.modalitatiTrimitere}
                                            search
                                            defaultValue={item.ID_ModTrimitereRaspuns}
                                            onChange={((e, data) => this.handleActualizeazaForm('ID_ModTrimitereRaspuns', data.value))}
                                        /> :
                                        item.DenumireModTrimitereRaspuns}</Table.Cell>

                                <Table.Cell className="textDepasesteDimensiune" title={item.ObservatiiSolicitant}>
                                    {(this.props.ePersonalRegistratura && this.state.idCerereInEditare === item.ID_Document) ?
                                        <Input fluid
                                               width={3}
                                               defaultValue={item.ObservatiiSolicitant}
                                               onChange={(event) => this.handleActualizeazaForm('ObservatiiSolicitant', event.target.value)}
                                        /> :
                                        item.ObservatiiSolicitant}</Table.Cell>

                                <Table.Cell className="textDepasesteDimensiune" title={item.MentiuniRegistratura}>
                                    {(this.props.ePersonalRegistratura && this.state.idCerereInEditare === item.ID_Document) ?
                                        <Input fluid
                                               width={3}
                                               defaultValue={item.MentiuniRegistratura}
                                               onChange={(event) => this.handleActualizeazaForm('MentiuniRegistratura', event.target.value)}
                                        /> : item.MentiuniRegistratura}</Table.Cell>

                                <Table.Cell className={(this.props.ePersonalRegistratura && this.state.idCerereInEditare === item.ID_Document) ? "" : "textDepasesteDimensiune"}
                                            title={item.NumeAngajatDeponent}>
                                    {(this.props.ePersonalRegistratura && this.state.idCerereInEditare === item.ID_Document) ?
                                        <Form>
                                            <Autosuggest
                                                suggestions={optiuniNumeAngajat}
                                                onSuggestionsFetchRequested={this.getSugestiiNume}
                                                onSuggestionsClearRequested={this.clearSugestiiNume}
                                                inputProps={numeAngajatInputProps}
                                                getSuggestionValue={getSuggestionValueName}
                                                renderSuggestion={renderSuggestionValueName}
                                                onSuggestionSelected={this.onNameAngajatSuggestionSelected}
                                            />
                                        </Form>
                                     : item.NumeAngajatDeponent}</Table.Cell>
                                <Table.Cell className={(this.props.ePersonalRegistratura && this.state.idCerereInEditare === item.ID_Document) ? "" : "textDepasesteDimensiune"}
                                            title={item.MailAngajatDeponent}>
                                     {(this.props.ePersonalRegistratura && this.state.idCerereInEditare === item.ID_Document) ?
                                         <Form>
                                             <Autosuggest
                                                 suggestions={optiuniEmailAngajat}
                                                 onSuggestionsFetchRequested={this.getSugestiiEmail}
                                                 onSuggestionsClearRequested={this.clearSugestiiEmail}
                                                 inputProps={emailAngajatInputProps}
                                                 getSuggestionValue={getSuggestionValueEmail}
                                                 renderSuggestion={renderSuggestionValueEmail}
                                                 onSuggestionSelected={this.onEmailAngajatSuggestionSelected}
                                             />
                                         </Form> : item.MailAngajatDeponent}</Table.Cell>
                                {this.props.ePersonalRegistratura &&
                                <Table.Cell>
                                    {this.state.idCerereInEditare === item.ID_Document &&
                                    <Button type="button" onClick={this.handleBtnFinalizeazaClicked} color="green">
                                        Salvează
                                    </Button>}
                                </Table.Cell>}

                                {/*<Table.Cell>*/}
                                {/*    <input*/}
                                {/*        type="file"*/}
                                {/*        disabled={item.Stadiul !== "Inregistrat"}*/}
                                {/*        onChange={(event) => this.handleFileUpload('fisier', event.target.files[0], index)}/>*/}
                                {/*    <Button disabled={item.Stadiul !== "Inregistrat"}>*/}
                                {/*        Trimite*/}
                                {/*    </Button>*/}
                                {/*</Table.Cell>*/}
                                {/*<Table.Cell>*/}
                                {/*    <Dropdown text='Documente încărcate' pointing='left'*/}
                                {/*              className='link item'>*/}
                                {/*        <Dropdown.Menu>*/}
                                {/*            <Dropdown.Item><Icon name={'file'}/>Document 1</Dropdown.Item>*/}
                                {/*            <Dropdown.Item><Icon name={'file'}/>Document 2</Dropdown.Item>*/}
                                {/*        </Dropdown.Menu>*/}
                                {/*    </Dropdown>*/}
                                {/*</Table.Cell>*/}
                            </Table.Row>)
                    })}
                </Table.Body>

                <Table.Footer>
                    <Table.Row>
                        <Table.HeaderCell colSpan="22">
                            <Label circular className="depasitTermenLimita borderLabel" >Depășit termen limită și nefinalizat</Label>
                            <Label circular className="inTermenLimita borderLabel" content= "În termen limită și nefinalizat"/>
                            <Label circular className="finalizat borderLabel" content= "Finalizat"/>
                        </Table.HeaderCell>
                    </Table.Row>
                </Table.Footer>
            </Table>
        )
    }
}

TabelViewDocumente
    .propTypes = {
    ePersonalRegistratura: PropTypes.bool,
    istoricCereri: PropTypes.array.isRequired,
    stadiiCereri: PropTypes.array,
    modalitatiTrimitere: PropTypes.array,
    listaDepartamente: PropTypes.array,
    actualizeazaInIstoricCerere: PropTypes.func,
    afiseazaBtnInregistreaza: PropTypes.bool,
    afiseazaBtnTrimiteReminder: PropTypes.bool

};

TabelViewDocumente
    .defaultProps = {
    afiseazaBtnInregistreaza: false,
    afiseazaBtnTrimiteReminder: false
};

export default TabelViewDocumente;