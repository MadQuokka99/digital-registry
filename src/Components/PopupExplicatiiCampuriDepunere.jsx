import {Icon, Popup} from "semantic-ui-react";
import PropTypes from "prop-types";
import React from "react";

const PopupExplicatiiCampuriDepunere = (props) => {
    if(props.continutPopup !== undefined){
        return (
            <Popup trigger={
                <Icon
                    name="info circle" color="grey"/>}
                   content={props.continutPopup}
                   size="large"
                   wide
            />)
    }
     else return null
}

PopupExplicatiiCampuriDepunere.propTypes = {
    continutPopup: PropTypes.string,
}
export default PopupExplicatiiCampuriDepunere;