import React, {Component} from "react";
import {Table, Icon} from "semantic-ui-react";
import axios from "../Utils/axios-AGSIS-API";
import moment from 'moment'
import MessageComponent from "./MessageComponent";


const optiuniDropdownPredatOriginalCopie = [{
    key: 1,
    text: "Original",
    value: 1,
},
    {
        key: 2,
        text: "Copie",
        value: 2
    }]


class TabelVeziTraseu extends Component {

    state = {
        traseuDocument: [],
        traseuNesetat: false
    }

    componentDidMount() {
        axios
            .get(`Registratura/TraseuDocumentListByID_Document?ID_Document=${this.props.ID_Document}`)
            .then(response => {
                if(response.data.length === 0){

                    this.setState({
                        traseuNesetat: true
                    })
                }


                this.setState({
                    traseuDocument: response.data
                })
            })
    }

    render() {
        return (
            this.state.traseuNesetat ?
                <MessageComponent iconName={"settings"} message1={"Traseul nu a fost setat pentru acest document!"} size={"large"} info={true} compact={true}/>
                    : <Table celled striped>
                        <Table.Header>
                            <Table.Row textAlign="center">
                                <Table.HeaderCell>Ordine element</Table.HeaderCell>
                                <Table.HeaderCell>Departament</Table.HeaderCell>
                                <Table.HeaderCell>Predat în</Table.HeaderCell>
                                <Table.HeaderCell>Predat</Table.HeaderCell>
                                <Table.HeaderCell>Data Predare</Table.HeaderCell>
                                <Table.HeaderCell>Username Predare</Table.HeaderCell>
                                <Table.HeaderCell>Preluat</Table.HeaderCell>
                                <Table.HeaderCell>Data Preluare</Table.HeaderCell>
                                <Table.HeaderCell>Username Preluare</Table.HeaderCell>
                                <Table.HeaderCell>Cine a setat elementul din traseu</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        <Table.Body>
                            {this.state.traseuDocument.map(item => {
                                return (
                                    <Table.Row key={item.OrdineTraseu}>
                                        <Table.Cell>{item.OrdineTraseu}</Table.Cell>
                                        <Table.Cell>{item.PredatLa_NumeDepartament}</Table.Cell>
                                        <Table.Cell>{item.PredatOriginal ? optiuniDropdownPredatOriginalCopie[0].text : optiuniDropdownPredatOriginalCopie[1].text}</Table.Cell>
                                        <Table.Cell>{item.DataPredare === null ? null : <Icon name="check" color="green"/>}</Table.Cell>
                                        <Table.Cell>{item.DataPredare === null ? null : moment(item.DataPredare).format("DD-MM-YYYY")}</Table.Cell>
                                        <Table.Cell>{item.UsernamePredare}</Table.Cell>
                                        <Table.Cell>{item.DataPreluareInRegistratura === null ? null : <Icon name="check" color="green"/>}</Table.Cell>
                                        <Table.Cell>{item.DataPreluareInRegistratura === null ? null : moment(item.DataPreluareInRegistratura).format("DD-MM-YYYY")}</Table.Cell>
                                        <Table.Cell>{item.UsernamePreluare}</Table.Cell>
                                        <Table.Cell>{item.UsernameModificareTraseu}</Table.Cell>
                                    </Table.Row>)
                            })}
                        </Table.Body>
                    </Table>

        )
    }
}

export default TabelVeziTraseu;