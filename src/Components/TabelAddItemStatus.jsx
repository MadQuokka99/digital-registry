import React, {Component} from 'react'
import axios from "../Utils/axios-AGSIS-API";
import {Table, Button, Icon, Dropdown} from 'semantic-ui-react'
import {toast} from "react-semantic-toasts";
import moment from 'moment';


class TabelAddItemStatus extends Component {

    state = {
        statusuriDocument: [],
        btnAdaugaStatusNouDisabled: false,
        ID_StatusAles: null,
        btnSalveazaStatusDisable: false
    }

    statusNouCopy = {
        Status: {
            key: null,
            text: null,
            value: null
        },
        ID_StatusDocument: null,
        ID_Document: this.props.ID_Document,
        UsernameModificareStatus: null,
        DataModificareStatus: moment()
    } //copie a obiectului status nou din lista de statusuri ale documentului

    componentDidMount() {
        /**
         * metoda returneaza statusurile documentului trimis ca parametru cu ID_Document
         */
        axios
            .get(`Registratura/StatusDocumentListByID_Document?ID_Document=${this.props.ID_Document}`)
            .then(response => {

                let raspuns = response.data;
                let statusuriDocumentCopy = [];

                for (let i = 0; i < raspuns.length; i++) {
                    let status = {
                        Status: {
                            key: raspuns[i].ID_Status,
                            text: raspuns[i].DenumireStatus,
                            value: raspuns[i].ID_Status
                        },
                        ID_StatusDocument: raspuns[i].ID_StatusDocument,
                        ID_Document: raspuns[i].ID_Document,
                        UsernameModificareStatus: raspuns[i].UsernameModificareStatus,
                        DataModificareStatus: raspuns[i].DataModificareStatus,
                    }
                    statusuriDocumentCopy.push(status);
                }

                this.setState({
                    statusuriDocument: statusuriDocumentCopy
                })
            })
    }

    /**
     * metoda trimite obiectul cu atributele setate pentru a se actualiza statusul nou din lista de statusuri
     *
     * @param ID_Document
     * @param status
     */
    handleAdaugareStatusNou = (ID_Document, status) => {

        axios
            .post(`Registratura/StatusDocumentAdd?ID_Document=${ID_Document}&ID_Status=${status}`)
            .then(response => {

                toast({
                    type: "success",
                    icon: "check",
                    title: "Statusul documentului a fost actualizat cu succes!",
                    time: 4000,
                });

                let updated = false;
                let statusuriDocumentCopy = [...this.state.statusuriDocument];

                for (let i = 0; i < statusuriDocumentCopy.length; i++) {
                    if (statusuriDocumentCopy[i].ID_StatusDocument === response.data.ID_StatusDocument) {
                        updated = true;
                        statusuriDocumentCopy[i].Status.key = response.data.ID_Status;
                        statusuriDocumentCopy[i].Status.text = response.data.DenumireStatus;
                        statusuriDocumentCopy[i].Status.value = response.data.ID_Status;
                        statusuriDocumentCopy[i].ID_StatusDocument = response.data.ID_StatusDocument;
                        statusuriDocumentCopy[i].ID_Document = response.data.ID_Document;
                        statusuriDocumentCopy[i].UsernameModificareStatus = response.data.UsernameModificareStatus;
                        statusuriDocumentCopy[i].DataModificareStatus = response.data.DataModificareStatus;
                    }
                }

                let statusNou = {
                    Status: {
                        key: response.data.ID_Status,
                        text: response.data.DenumireStatus,
                        value: response.data.ID_Status
                    },
                    ID_StatusDocument: response.data.ID_StatusDocument,
                    ID_Document: this.props.ID_Document,
                    UsernameModificareStatus: response.data.UsernameModificareStatus,
                    DataModificareStatus: response.data.DataModificareStatus
                };

                if (!updated) {
                    statusuriDocumentCopy.pop();
                    statusuriDocumentCopy.push(statusNou);
                }

                this.setState({
                    statusuriDocument: statusuriDocumentCopy,
                    btnAdaugaStatusNouDisabled: false,
                    btnSalveazaStatusDisable: false,
                    ID_StatusAles: null
                })

            })
            .catch(error => {
                console.log(error);
                toast({
                    type: "error",
                    icon: "warning",
                    title: "Modificarea nu a fost salvată!",
                    time: 4000,
                });
            })
    }


    handleAdaugareRandTabel = () => {

        let statusuriDocumentCopy = [...this.state.statusuriDocument];
        statusuriDocumentCopy.push(JSON.parse(JSON.stringify(this.statusNouCopy)));

        this.setState({
            statusuriDocument: statusuriDocumentCopy,
            btnAdaugaStatusNouDisabled: true,
            btnSalveazaStatusDisable: true
        })
    }

    retineStatus = (ID_Status) => {

        let statusuriDocumentCopy = [...this.state.statusuriDocument];
        let statusuriListCopy = [...this.props.listaStatusuri];

        let statusAlesInfo = statusuriListCopy.filter(statusInfo => statusInfo.key === ID_Status)[0];

        for (let i = 0; i < statusuriDocumentCopy.length; i++) {
            if (statusuriDocumentCopy[i].ID_StatusDocument === null) {
                statusuriDocumentCopy[i].Status.value = statusAlesInfo.value;
                statusuriDocumentCopy[i].Status.text = statusAlesInfo.text;
                statusuriDocumentCopy[i].Status.key = statusAlesInfo.key;
            }
        }
        this.setState({
            ID_StatusAles: ID_Status,
            statusuriDocument: statusuriDocumentCopy
        })
    }

    /**
     * metoda sterge din tabel un rand si sterge si statusul respectiv din BD
     * @param ID_StatusDocument
     */
    handleStergereRandTabel = (ID_StatusDocument) => {

        axios
            .post(`Registratura/StatusDocumentDelete?ID_StatusDocument=${ID_StatusDocument}`)
            .then(response => {

                let statusuriDocumentCopy = [...this.state.statusuriDocument];
                for (let i = 0; i < statusuriDocumentCopy.length; i++) {
                    if (statusuriDocumentCopy[i].ID_StatusDocument === ID_StatusDocument) {
                        statusuriDocumentCopy.splice(i, 1);
                    }
                }

                this.setState({statusuriDocument: statusuriDocumentCopy})
                toast({
                    type: "success",
                    icon: "check",
                    title: "Statusul documentului a fost actualizat cu succes!",
                    time: 4000,
                });

            })
            .catch(error => {
                console.log(error);
                toast({
                    type: "error",
                    icon: "warning",
                    title: error.response.data + " Modificarea nu a fost salvată!",
                    time: 4000,
                });
            })
    }

    render() {
        return (
            <Table compact celled>
                <Table.Header>
                    <Table.Row textAlign="center">
                        <Table.HeaderCell>Status</Table.HeaderCell>
                        <Table.HeaderCell></Table.HeaderCell>
                        <Table.HeaderCell>Data modificării</Table.HeaderCell>
                        <Table.HeaderCell>Cine a făcut modificarea</Table.HeaderCell>
                        <Table.HeaderCell></Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {this.state.statusuriDocument.map((item, index) => {
                        return (
                            <Table.Row key={index}>
                                <Table.Cell>
                                    <Dropdown
                                        upward
                                        selection label="Status" width={6}
                                        options={this.props.listaStatusuri}
                                        search
                                        disabled={item.ID_StatusDocument !== null}
                                        value={item.Status.value}
                                        onChange={(e, data) => {
                                            this.retineStatus(data.value)
                                        }}/>
                                </Table.Cell>
                                <Table.Cell collapsing>
                                    <Button
                                        type="button"
                                        color="green"
                                        icon size="small"
                                        disabled={item.ID_StatusDocument !== null || this.state.ID_StatusAles === null}
                                        compact
                                        onClick={() => {
                                            this.handleAdaugareStatusNou(item.ID_Document, this.state.ID_StatusAles)
                                        }}><Icon name="check"/> Salvează status
                                    </Button>
                                </Table.Cell>
                                <Table.Cell>
                                    <p>{moment(item.DataModificareStatus).format("DD-MM-YYYY")}</p>
                                </Table.Cell>
                                <Table.Cell>
                                    <p>{item.UsernameModificareStatus}</p>
                                </Table.Cell>
                                <Table.Cell collapsing>
                                    <Button
                                        disabled={item.UsernameModificareStatus !== this.props.username && !this.props.ePersonalRegistratura}
                                        type="button"
                                        floated='right'
                                        icon
                                        color={'red'}
                                        size='small'
                                        onClick={() => this.handleStergereRandTabel(item.ID_StatusDocument)}
                                    >
                                        <Icon name='trash'/>
                                    </Button>
                                </Table.Cell>
                            </Table.Row>
                        )
                    })}
                </Table.Body>

                <Table.Footer>
                    <Table.Row>
                        <Table.HeaderCell/>
                        <Table.HeaderCell colSpan='4'>
                            <Button
                                type="button"
                                floated='right'
                                icon
                                disabled={this.state.btnAdaugaStatusNouDisabled}
                                labelPosition='left'
                                color="green"
                                size='small'
                                onClick={this.handleAdaugareRandTabel}
                            >
                                <Icon name='plus'/> Adaugă
                            </Button>
                        </Table.HeaderCell>
                    </Table.Row>
                </Table.Footer>

            </Table>
        )
    }

}

export default TabelAddItemStatus;