import {Button, Icon, Modal} from "semantic-ui-react";
import React from "react";
import PropTypes from "prop-types";
import '../Design/ModalStyle.css';

const ModalEstiSigurCare = (props) => {

    return(
        <Modal
            trigger={
                <Button
                    type="button"
                    size="big"
                    color='green'
                    style={{
                        marginRight: "auto",
                        marginLeft: "auto",
                        marginTop: "80px",
                        display: "flex",
                        textAlign: "center",
                    }}
                    disabled={!props.formIsValid}
                    onClick={props.handleOpenModal}>{props.textBtnTriggerModal}</Button>
            }
            open={props.openModal}
            onOpen={props.handleOpenModal}
            onClose={props.handleCloseModal}
            size='small'
            closeIcon
            className="afisareModal"
        >
            <Modal.Header icon='right arrow' content={props.modalTitle}/>
            <Modal.Content> {props.modalContent} </Modal.Content>
            <Modal.Actions>
                <Button
                    type="button"
                    basic
                    color='red'
                    icon="x"
                    content='Nu'
                    onClick={props.handleCloseModal}/>
                <Button
                    disabled={props.stateBtnOk}
                    color='green'
                    type="button"
                    basic
                    onClick={props.actiuneApasareBtnOk}>
                    <Icon name='checkmark'/> Da
                </Button>
            </Modal.Actions>
        </Modal>
    )
}

ModalEstiSigurCare.propTypes = {
    formIsValid: PropTypes.bool,
    handleOpenModal: PropTypes.func,
    openModal: PropTypes.bool,
    handleCloseModal: PropTypes.func,
    actiuneApasareBtnOk: PropTypes.func,
    stateBtnOk: PropTypes.bool,
    modalContent: PropTypes.string,
    modalTitle: PropTypes.string,
    textBtnTriggerModal: PropTypes.string
}

export default ModalEstiSigurCare;