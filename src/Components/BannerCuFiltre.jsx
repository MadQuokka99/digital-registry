import React, {Component} from "react";
import PropTypes from "prop-types";
import {Button, Form, Segment} from "semantic-ui-react";
import PopupExplicatiiCampuriDepunere from "./PopupExplicatiiCampuriDepunere";

class BannerCuFiltre extends Component {


    render() {
        return (
            <Segment.Group horizontal style={{
                textAlign: "center",
                width: "98%",
                paddingBottom: "0%",
                paddingTop: "0%"
            }}>
                <Segment color={"green"} style={{
                    width: "8%"
                }}>
                    <Form>
                        <Form.Dropdown
                            label="Solicitări din"
                            fluid
                            selection
                            value={this.props.anAlesDinDropdownPentruIstoric}
                            placeholder="An istoric"
                            options={this.props.aniIstoric}
                            onChange={((e, data) => this.props.handleAnAlesDinDropdown(data.value))}/>
                    </Form>
                </Segment>
                <Segment style={{
                    width: "10%"
                }}>
                    <Form>
                        <Form.Input
                            label="Nr de ordine"
                            style={{
                                width: "90%"
                            }}
                            action
                            type="number"
                            value={this.props.filtrariCautare.NrDeOrdine || ''}
                            autoFocus={true}
                            placeholder="NrDeOrdine"
                            onChange={(e,) => {

                                this.props.handleInputIntrodusDeUser("NrDeOrdine", e.target.value)
                            }}>
                            <input/>
                        </Form.Input>
                    </Form>
                </Segment>
                <Segment style={{
                    width: "10%"
                }}>
                    <Form>
                        <Form.Dropdown
                            label="Dep. emitent"
                            fluid
                            selection
                            placeholder="Departament"
                            options={this.props.listaDepartamente}
                            search
                            value={this.props.filtrariCautare.CineAEmis_ID_Departament}
                            onChange={(e, data) => {
                                this.props.handleInputIntrodusDeUser("cineAEmis", data.value)
                            }}/>
                    </Form>

                </Segment>
                <Segment style={{
                    width: "10%"
                }}>
                    <Form>
                        <Form.Input
                            label="Conținutul actului"
                            style={{
                                width: "100%"
                            }}
                            action
                            value={this.props.filtrariCautare.ContinutulActului}
                            autoFocus={true}
                            placeholder="ConținutAct"
                            onChange={(e,) => {

                                this.props.handleInputIntrodusDeUser("ContinutulActului", e.target.value)
                            }}>
                            <input/>
                        </Form.Input>
                    </Form>
                </Segment>
                <Segment style={{
                    width: "10%"
                }}>
                    <Form>
                        <Form.Input
                            label="Nume solicitant"
                            style={{
                                width: "100%"
                            }}
                            action
                            value={this.props.filtrariCautare.NumeDeponent}
                            autoFocus={true}
                            placeholder="NumeSolicitant"
                            onChange={(e,) => {

                                this.props.handleInputIntrodusDeUser("NumeDeponent", e.target.value)
                            }}>
                            <input/>
                        </Form.Input>
                    </Form>
                </Segment>
                <Segment style={{
                    width: "9%"
                }}>
                    <Form>
                        <Form.Input
                            label="Email solicitant"
                            style={{
                                width: "90%"
                            }}
                            action
                            value={this.props.filtrariCautare.MailDeponent}
                            autoFocus={true}
                            placeholder="EmailSolicitant"
                            onChange={(e,) => {

                                this.props.handleInputIntrodusDeUser("MailDeponent", e.target.value)
                            }}>
                            <input/>
                        </Form.Input>
                    </Form>
                </Segment>
                <Segment style={{
                    width: "9%"
                }}>
                    <Form>
                        <Form.Input
                            label="Autoritatea emitentă"
                            style={{
                                width: "90%"
                            }}
                            action
                            value={this.props.filtrariCautare.CineAEmis_Autoritate}
                            autoFocus={true}
                            placeholder="AutoritateSolicitantă"
                            onChange={(e) => {
                                this.props.handleInputIntrodusDeUser("CineAEmis_Autoritate", e.target.value)
                            }}>
                            <input/>
                        </Form.Input>
                    </Form>

                </Segment>
                <Segment style={{
                    width: "9%"
                }}>
                    <Form>
                        <Form.Input
                            label="Nr actului intrat"
                            style={{
                                width: "90%"
                            }}
                            action
                            value={this.props.filtrariCautare.NrActuluiIntrat}
                            autoFocus={true}
                            placeholder="NrActuluiIntrat"
                            onChange={(e,) => {

                                this.props.handleInputIntrodusDeUser("NrActuluiIntrat", e.target.value)
                            }}>
                            <input/>
                        </Form.Input>
                    </Form>
                </Segment>

                <Segment style={{
                    width: "10%"
                }}>
                    <Form>
                        <span>
            <p>
                <strong>De la
                <sup>
                    <PopupExplicatiiCampuriDepunere
                        continutPopup={"Pentru afișarea solicitărilor dintr-o anumită zi se alege doar data în acest filtru și se apasă butonul verde de căutare!"}/>
                </sup>
                </strong>
            </p>
                        </span>
                        <Form.Input type="date"
                                    value={this.props.filtrariCautare.DataInregistrare_Inceput}
                                    style={{
                                        width: "100%"
                                    }}
                                    onChange={(e) => {

                                        this.props.handleInputIntrodusDeUser("DataInregistrare_Inceput", e.target.value)
                                    }}/>
                    </Form>
                </Segment>

                <Segment style={{
                    width: "10%"
                }}>
                    <Form>
                        <span>
                            <p><strong>Până la </strong></p>
                        </span>
                        <Form.Input type="date"
                                    value={this.props.filtrariCautare.DataInregistrare_Sfarsit}
                                    style={{
                                        width: "100%"
                                    }}
                                    onChange={(e) => {

                                        this.props.handleInputIntrodusDeUser("DataInregistrare_Sfarsit", e.target.value)
                                    }}/>
                    </Form>
                </Segment>

                <Segment style={{
                    display: "grid",
                    margin: "auto"
                }}>
                        <Button type="button"
                                onClick={() => {
                                    this.props.filtrareIstoricDupaParametri(this.props.filtrariCautare);
                                }}
                                icon="search" color="green" size="tiny"
                                disabled={!this.props.macarUnFiltruActiv}
                        />
                        <Button type="button" icon="x" color="red" size="tiny" onClick={() => this.props.resetareFiltre()}
                        disabled={!this.props.macarUnFiltruActiv}/>
                </Segment>
            </Segment.Group>
        )
    }
}

BannerCuFiltre.propTypes = {
    handleInputIntrodusDeUser: PropTypes.func.isRequired,
    filtrareIstoricDupaParametri: PropTypes.func.isRequired,
    resetareFiltre: PropTypes.func.isRequired,
    handleAnAlesDinDropdown: PropTypes.func.isRequired,
    aniIstoric: PropTypes.array.isRequired,
    anAlesDinDropdownPentruIstoric: PropTypes.any.isRequired,
    macarUnFiltruActiv: PropTypes.bool.isRequired,
    filtrariCautare: PropTypes.object.isRequired,
    listaDepartamente: PropTypes.array.isRequired
}

export default BannerCuFiltre;