import {Icon, Message} from "semantic-ui-react";
import React from "react";
import PropTypes from "prop-types";


const MessageComponent = (props) => {
    return (
        <Message info={props.info} size={props.size} icon style={props.style} negative={props.negative} positive={props.positive} compact={props.compact}>
            <Icon name={props.iconName}/>
            <Message.Content>
                <Message.Header>
                    {props.message1} {props.nrZileTermenLimita} {props.message2}
                </Message.Header>
                {props.messageContent}
            </Message.Content>
        </Message>
    )
}

MessageComponent.propTypes = {
    iconName: PropTypes.string.isRequired,
    size: PropTypes.string,
    message1: PropTypes.string.isRequired,
    message2: PropTypes.string,
    messageContent: PropTypes.string,
    nrZileTermenLimita: PropTypes.any,
    style: PropTypes.object,
    negative: PropTypes.bool,
    compact: PropTypes.bool,
    info: PropTypes.bool,
    positive: PropTypes.bool
}


export default MessageComponent;