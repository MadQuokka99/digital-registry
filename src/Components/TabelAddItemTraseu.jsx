import React, {Component} from 'react'
import axios from "../Utils/axios-AGSIS-API";
import {Table, Button, Icon, Dropdown, Checkbox, Popup} from 'semantic-ui-react'
import {toast} from "react-semantic-toasts";
import moment from "moment";
import {getTraseuDocumentByID_Document} from "../Utils/ApiCalls";

const optiuniDropdownPredatOriginalCopie = [{
    key: 1,
    text: "Original",
    value: 1,
},
    {
        key: 2,
        text: "Copie",
        value: 2
    }]


class TabelAddItemTraseu extends Component {

    state = {
        traseuDocument: [],
        btnAdaugaElementNouTraseuDisabled: false,
        ID_DepartamentAles: null,
        btnSalveazaElementTraseuDisabled: true
    }

    elementTraseuCopy = {
        OrdineTraseu: undefined,
        Departament: {
            key: null,
            text: null,
            value: null
        },
        ID_TraseuDocument: null,
        ID_Document: this.props.ID_Document,
        DataPredare: null,
        DataPreluareInRegistratura: null,
        checkedPredare: false,
        checkedPreluare: false,
        UsernameModificareTraseu: null,
        UsernamePredare: null,
        UsernamePreluare: null,
        PredatOriginal: true,
        ElementTraseuModificat: false
    } //copie a obiectului element din traseul documentului

    componentDidMount() {
        /**
         * metoda returneaza traseul documentului trimis ca parametru cu ID_Document
         */
        getTraseuDocumentByID_Document(this.props.ID_Document)
            .then(raspuns => {
                let traseuDocumentCopy = [];

                for (let i = 0; i < raspuns.length; i++) {
                    let elementTraseu = {
                        OrdineTraseu: raspuns[i].OrdineTraseu,
                        Departament: {
                            key: raspuns[i]["PredatLa_ID_Departament"],
                            text: raspuns[i]["PredatLa_NumeDepartament"],
                            value: raspuns[i]["PredatLa_ID_Departament"],
                        },
                        ID_TraseuDocument: raspuns[i].ID_TraseuDocument,
                        ID_Document: raspuns[i].ID_Document,
                        DataPredare: raspuns[i].DataPredare,
                        DataPreluareInRegistratura: raspuns[i].DataPreluareInRegistratura,
                        checkedPredare: raspuns[i].DataPredare !== null,
                        checkedPreluare: raspuns[i].DataPreluareInRegistratura !== null,
                        UsernameModificareTraseu: raspuns[i].UsernameModificareTraseu,
                        UsernamePredare: raspuns[i].UsernamePredare,
                        UsernamePreluare: raspuns[i].UsernamePreluare,
                        PredatOriginal: raspuns[i].PredatOriginal
                    }
                    traseuDocumentCopy.push(elementTraseu);
                }

                this.setState({
                    traseuDocument: traseuDocumentCopy
                })
            })
    }

    actualizareObiectTraseu = (traseuDocumentCopy, i, raspunsUpdate) => {

        traseuDocumentCopy[i].Departament.key = raspunsUpdate.PredatLa_ID_Departament;
        traseuDocumentCopy[i].Departament.text = raspunsUpdate.PredatLa_NumeDepartament;
        traseuDocumentCopy[i].Departament.value = raspunsUpdate.PredatLa_ID_Departament;

        traseuDocumentCopy[i].OrdineTraseu = raspunsUpdate.OrdineTraseu;
        traseuDocumentCopy[i].ID_TraseuDocument = raspunsUpdate.ID_TraseuDocument;
        traseuDocumentCopy[i].checkedPredare = raspunsUpdate.DataPredare !== null;
        traseuDocumentCopy[i].checkedPreluare = raspunsUpdate.DataPreluareInRegistratura !== null;
        traseuDocumentCopy[i].DataPredare = raspunsUpdate.DataPredare;
        traseuDocumentCopy[i].DataPreluareInRegistratura = raspunsUpdate.DataPreluareInRegistratura;
        traseuDocumentCopy[i].UsernameModificareTraseu = raspunsUpdate.UsernameModificareTraseu;
        traseuDocumentCopy[i].PredatOriginal = raspunsUpdate.PredatOriginal;
        traseuDocumentCopy[i].UsernamePredare = raspunsUpdate.UsernamePredare;
        traseuDocumentCopy[i].UsernamePreluare = raspunsUpdate.UsernamePreluare;
        traseuDocumentCopy[i].ElementTraseuModificat = false;
    }

    retineDepartamentAles = (ID_TraseuDocument, ID_Departament) => {

        let traseuDocumentCopy = [...this.state.traseuDocument];
        let listaDepartamenteCopy = [...this.props.listaDepartamente];
        let departamentAlesInfo = listaDepartamenteCopy.filter(dep => ID_Departament === dep.key)[0];

        for (let i = 0; i < traseuDocumentCopy.length; i++) {
            if (traseuDocumentCopy[i].ID_TraseuDocument === ID_TraseuDocument) {
                if (traseuDocumentCopy[i].Departament.key !== ID_Departament) {
                    traseuDocumentCopy[i].Departament.key = departamentAlesInfo.key;
                    traseuDocumentCopy[i].Departament.text = departamentAlesInfo.text;
                    traseuDocumentCopy[i].Departament.value = departamentAlesInfo.value;
                    traseuDocumentCopy[i].ElementTraseuModificat = true;

                    this.setState({
                        ID_DepartamentAles: ID_Departament,
                        traseuDocument: traseuDocumentCopy
                    })
                }
            }
        }
    }

    /**
     * metoda trimite obiectul cu atributele setate pentru a se actualiza elementul din traseu
     *
     * @param ID_TraseuDocument
     * @param ID_Document
     * @param value
     */
    handleAdaugareElementTraseu = (ID_TraseuDocument, ID_Document, value) => {

        let listaDepartamenteCopy = this.props.listaDepartamente.filter(dep => value === dep.key);

        let obiectDeTrimis = {
            ID_TraseuDocument: ID_TraseuDocument,
            ID_Document: ID_Document,
            PredatLa_ID_Departament: listaDepartamenteCopy[0].value,
        }
        axios
            .post(`Registratura/TraseuDocumentMerge`, obiectDeTrimis)
            .then(response => {

                toast({
                    type: "success",
                    icon: "check",
                    title: "Traseul documentului a fost actualizat cu succes!",
                    time: 4000,
                });

                let updated = false;
                let traseuDocumentCopy = [...this.state.traseuDocument];

                for (let i = 0; i < traseuDocumentCopy.length; i++) {
                    if (traseuDocumentCopy[i].ID_TraseuDocument === response.data.ID_TraseuDocument) {
                        updated = true;
                        this.actualizareObiectTraseu(traseuDocumentCopy, i, response.data);
                    }
                }

                let elementTraseu = {
                    OrdineTraseu: response.data.OrdineTraseu,
                    Departament: {
                        key: response.data["PredatLa_ID_Departament"],
                        text: response.data["PredatLa_NumeDepartament"],
                        value: response.data["PredatLa_ID_Departament"],
                    },
                    DataPreluareInRegistratura: response.data.DataPreluareInRegistratura,
                    DataPredare: response.data.DataPredare,
                    ID_TraseuDocument: response.data.ID_TraseuDocument,
                    ID_Document: this.props.ID_Document,
                    checkedPredare: response.data.DataPredare !== null,
                    checkedPreluare: response.data.DataPreluareInRegistratura !== null,
                    UsernameModificareTraseu: response.data.UsernameModificareTraseu,
                    UsernamePreluare: response.data.UsernamePreluare,
                    UsernamePredare: response.data.UsernamePredare,
                    PredatOriginal: response.data.PredatOriginal,
                    ElementTraseuModificat: false
                };

                if (!updated) {
                    traseuDocumentCopy.pop();
                    traseuDocumentCopy.push(elementTraseu);
                }

                this.setState({
                    traseuDocument: traseuDocumentCopy,
                    btnAdaugaElementNouTraseuDisabled: false,
                    ID_DepartamentAles: null,
                    btnSalveazaElementTraseuDisabled: false
                })

            })
            .catch(error => {
                console.log(error.response);
                toast({
                    type: "error",
                    icon: "warning",
                    title: error.response.data + " Modificarea nu a fost salvată!",
                    time: 4000,
                });

            })
    }

    handleAdaugareRandTabel = () => {

        let traseuDocumentCopy = [...this.state.traseuDocument];
        let elementTraseu = JSON.parse(JSON.stringify(this.elementTraseuCopy));
        traseuDocumentCopy.push(elementTraseu);

        this.setState({
            traseuDocument: traseuDocumentCopy,
            btnAdaugaElementNouTraseuDisabled: true,
            btnSalveazaElementTraseuDisabled: true
        })
    }


    /**
     * metoda sterge din tabel un rand si sterge si elementul din traseul documentului din BD
     * @param ID_TraseuDocument
     */
    handleStergereRandTabel = (ID_TraseuDocument) => {

        axios
            .post(`Registratura/TraseuDocumentDelete?ID_TraseuDocument=${ID_TraseuDocument}`)
            .then(response => {
                let traseuDocumentCopy = [...this.state.traseuDocument];
                for (let i = 0; i < traseuDocumentCopy.length; i++) {
                    if (traseuDocumentCopy[i].ID_TraseuDocument === ID_TraseuDocument) {
                        traseuDocumentCopy.splice(i, 1);
                    }
                }

                this.setState({traseuDocument: traseuDocumentCopy})

                toast({
                    type: "success",
                    icon: "check",
                    title: "Traseul documentului a fost actualizat cu succes!",
                    time: 4000,
                });

            })
            .catch(error => {
                console.log(error);
                toast({
                    type: "error",
                    icon: "warning",
                    title: error.response.data + " Modificarea nu a fost salvată!",
                    time: 4000,
                });
            })
    }

    /**
     * metoda actualizeaza traseul daca un document a fost predat catre un departament
     * @param ID_TraseuDocument
     */
    handleBifareCheckBoxPredare = (ID_TraseuDocument) => {

        let estePredat;
        let traseuDocumentCopy = [...this.state.traseuDocument];
        for (let i = 0; i < traseuDocumentCopy.length; i++) {
            if (traseuDocumentCopy[i].ID_TraseuDocument === ID_TraseuDocument) {
                estePredat = !traseuDocumentCopy[i].checkedPredare;
            }
        }

        if ((!estePredat && this.props.ePersonalRegistratura) || estePredat) {
            axios
                .post(`Registratura/TraseuDocumentAdaugaPredare?ID_TraseuDocument=${ID_TraseuDocument}&estePredat=${estePredat}`)
                .then(response => {

                    toast({
                        type: "success",
                        icon: "check",
                        title: "Predarea a fost reținută cu succes!",
                        time: 4000,
                    });

                    let traseuDocumentCopy = [...this.state.traseuDocument];

                    for (let i = 0; i < traseuDocumentCopy.length; i++) {
                        if (traseuDocumentCopy[i].ID_TraseuDocument === response.data.ID_TraseuDocument) {
                            this.actualizareObiectTraseu(traseuDocumentCopy, i, response.data);
                        }
                    }

                    this.setState({
                        traseuDocument: traseuDocumentCopy
                    })

                })
                .catch(error => {
                    console.log(error);
                    toast({
                        type: "error",
                        icon: "warning",
                        title: "Modificarea nu a fost salvată! " + error.response.data,
                        time: 4000,
                    });
                })
        }
    }

    /**
     * metoda actualizeaza traseul daca un document a fost preluat de la un departament
     * @param ID_TraseuDocument
     */
    handleBifareCheckBoxPreluare = (ID_TraseuDocument) => {

        var estePreluat;
        let traseuDocumentCopy = [...this.state.traseuDocument];
        for (let i = 0; i < traseuDocumentCopy.length; i++) {
            if (traseuDocumentCopy[i].ID_TraseuDocument === ID_TraseuDocument) {
                estePreluat = !traseuDocumentCopy[i].checkedPreluare;
            }
        }
        if ((!estePreluat && this.props.ePersonalRegistratura) || estePreluat) {

            axios
                .post(`Registratura/TraseuDocumentAdaugaPreluare?ID_TraseuDocument=${ID_TraseuDocument}&estePreluat=${estePreluat}`)
                .then(response => {

                    toast({
                        type: "success",
                        icon: "check",
                        title: "Preluarea a fost reținută cu succes! ",
                        time: 4000,
                    });


                    let traseuDocumentCopy = [...this.state.traseuDocument];

                    for (let i = 0; i < traseuDocumentCopy.length; i++) {
                        if (traseuDocumentCopy[i].ID_TraseuDocument === response.data.ID_TraseuDocument) {
                            this.actualizareObiectTraseu(traseuDocumentCopy, i, response.data);
                        }
                    }

                    this.setState({
                        traseuDocument: traseuDocumentCopy
                    })

                })
                .catch(error => {

                    toast({
                        type: "error",
                        icon: "warning",
                        title: 'Modificarea nu a fost salvată! ' + error.response.data,
                        time: 4000,
                    });
                })
        }
    }


    handlePredatOriginalUpdate = (ID_TraseuDocument, predatOriginal) => {

        var PredatOriginal;

        if (predatOriginal === 1) {
            PredatOriginal = true;
        } else PredatOriginal = false;

        axios
            .post(`Registratura/TraseuDocumentPredatOriginalUpdate?ID_TraseuDocument=${ID_TraseuDocument}&PredatOriginal=${PredatOriginal}`)
            .then(response => {
                toast({
                    type: "success",
                    icon: "check",
                    title: "Tipul predării a fost setat cu succes!",
                    time: 4000,
                });

                let traseuDocumentCopy = [...this.state.traseuDocument];

                for (let i = 0; i < traseuDocumentCopy.length; i++) {
                    if (traseuDocumentCopy[i].ID_TraseuDocument === response.data.ID_TraseuDocument) {
                        this.actualizareObiectTraseu(traseuDocumentCopy, i, response.data);
                    }
                }

                this.setState({
                    traseuDocument: traseuDocumentCopy
                })
            })
            .catch(error => {
                toast({
                    type: "error",
                    icon: "warning",
                    title: error.response.data + '. Modificarea nu a fost salvată!',
                    time: 4000,
                });
            })
    }

    render() {
        return (
            <Table compact celled>
                <Table.Header>
                    <Table.Row textAlign="center">
                        <Table.HeaderCell>Ordine element</Table.HeaderCell>
                        <Table.HeaderCell>Departament</Table.HeaderCell>
                        <Table.HeaderCell></Table.HeaderCell>
                        <Table.HeaderCell>Predat în</Table.HeaderCell>
                        <Table.HeaderCell>Predat</Table.HeaderCell>
                        <Table.HeaderCell>Data Predare</Table.HeaderCell>
                        <Table.HeaderCell>Username Predare</Table.HeaderCell>
                        <Table.HeaderCell>Preluat</Table.HeaderCell>
                        <Table.HeaderCell>Data Preluare</Table.HeaderCell>
                        <Table.HeaderCell>Username Preluare</Table.HeaderCell>
                        <Table.HeaderCell>Cine a setat elementul din traseu</Table.HeaderCell>
                        <Table.HeaderCell></Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {this.state.traseuDocument.map((item, index) => {
                        return (
                            <Table.Row key={index}>
                                <Table.Cell>
                                    <p>{item.OrdineTraseu}</p>
                                </Table.Cell>
                                <Table.Cell>
                                    <Dropdown
                                        selection label="Departament" width={6}
                                        options={this.props.listaDepartamente}
                                        search
                                        disabled={item.DataPredare !== null || item.DataPreluareInRegistratura !== null || item.checkedPreluare || item.checkedPredare}
                                        value={item.Departament.value}
                                        onChange={(e, data) => {
                                            this.retineDepartamentAles(item.ID_TraseuDocument, data.value)
                                        }}/>
                                </Table.Cell>
                                <Table.Cell>
                                    <Popup content="Salvați departamentul ales" trigger={
                                        <Button
                                            type="button"
                                            color="green"
                                            icon size="small"
                                            disabled={!item.ElementTraseuModificat}
                                            compact
                                            onClick={() => {
                                                this.handleAdaugareElementTraseu(item.ID_TraseuDocument, item.ID_Document, this.state.ID_DepartamentAles)
                                            }}><Icon name="check"/>
                                        </Button>}
                                    />
                                </Table.Cell>
                                <Table.Cell>
                                    <Dropdown selection
                                              options={optiuniDropdownPredatOriginalCopie}
                                              disabled={item.DataPredare !== null || item.checkedPredare || item.ID_TraseuDocument === null}
                                              value={item.PredatOriginal ? optiuniDropdownPredatOriginalCopie[0].value : optiuniDropdownPredatOriginalCopie[1].value}
                                              onChange={(e, data) => {
                                                  this.handlePredatOriginalUpdate(item.ID_TraseuDocument, data.value)
                                              }}/>
                                </Table.Cell>
                                <Table.Cell>
                                    <Checkbox onChange={() => this.handleBifareCheckBoxPredare(item.ID_TraseuDocument)}
                                              checked={item.checkedPredare}
                                              disabled={(!this.props.ePersonalRegistratura && (item.checkedPredare || item.ID_TraseuDocument === null)) ||
                                              (this.props.ePersonalRegistratura && (item.checkedPreluare))}
                                    />
                                </Table.Cell>
                                <Table.Cell>{item.DataPredare !== null ? moment(item.DataPredare).format("DD-MM-YYYY") : null}</Table.Cell>
                                <Table.Cell>{item.UsernamePredare}</Table.Cell>
                                <Table.Cell>
                                    <Checkbox onChange={() => this.handleBifareCheckBoxPreluare(item.ID_TraseuDocument)}
                                              checked={item.checkedPreluare}
                                              disabled={(!this.props.ePersonalRegistratura && (item.checkedPreluare || item.DataPredare === null))
                                              || (this.props.ePersonalRegistratura && item.DataPredare === null)}/>
                                </Table.Cell>
                                <Table.Cell>{item.DataPreluareInRegistratura !== null ? moment(item.DataPreluareInRegistratura).format("DD-MM-YYYY") : null}</Table.Cell>
                                <Table.Cell>{item.UsernamePreluare}</Table.Cell>
                                <Table.Cell>{item.UsernameModificareTraseu}</Table.Cell>
                                <Table.Cell collapsing>
                                    <Button
                                        disabled={item.DataPredare !== null
                                        || item.DataPreluareInRegistratura !== null
                                        || item.checkedPreluare || item.checkedPredare
                                        || (item.UsernameModificareTraseu !== this.props.username && !this.props.ePersonalRegistratura)}
                                        type="button"
                                        floated='right'
                                        icon
                                        color={'red'}
                                        size='small'
                                        onClick={() => this.handleStergereRandTabel(item.ID_TraseuDocument)}>
                                        <Icon name='trash'/>
                                    </Button>
                                </Table.Cell>
                            </Table.Row>
                        )
                    })}
                </Table.Body>

                <Table.Footer>
                    <Table.Row>
                        <Table.HeaderCell/>
                        <Table.HeaderCell colSpan='10'>
                            <Button
                                type="button"
                                floated='right'
                                icon
                                disabled={this.state.btnAdaugaElementNouTraseuDisabled}
                                labelPosition='left'
                                color="green"
                                size='small'
                                onClick={this.handleAdaugareRandTabel}
                            >
                                <Icon name='plus'/> Adaugă
                            </Button>
                        </Table.HeaderCell>
                    </Table.Row>
                </Table.Footer>

            </Table>
        )
    }

}

export default TabelAddItemTraseu;