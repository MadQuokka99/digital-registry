import React from "react";
import {Icon, Label, Menu} from "semantic-ui-react";
import PropTypes from "prop-types";


const MenuItem = (props) => {
    return (

        <Menu.Item
            onClick={props.depunereTicket}
            name={props.titlu}
            active={props.match.path === props.path}
        >
            <Icon name={props.iconName}/>
            {(props.nrCereri !== undefined) &&
            <Label color='red' floating>
                {props.nrCereri}
            </Label>}
            {props.titlu}
        </Menu.Item>
    )
}

MenuItem.propTypes = {
    titlu: PropTypes.string.isRequired,
    iconName: PropTypes.string.isRequired,
    match: PropTypes.object.isRequired,
    nrCereri: PropTypes.number,
    path: PropTypes.string.isRequired,
    depunereTicket: PropTypes.func
}

export default MenuItem;

// Te pupix, Alex
