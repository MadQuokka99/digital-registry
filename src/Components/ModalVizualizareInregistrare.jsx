import React, {Component} from "react";
import {Button, Icon, Loader, Modal} from "semantic-ui-react";
import PropTypes from "prop-types";
import TabelSimplu from "./TabelSimplu";
import '../Design/ModalStyle.css';
import {getDocumentByID_Document} from "../Utils/ApiCalls";

/**
 * este utilizat pentru butonul de Vezi cerere originala
 */
class ModalVizualizareInregistrare extends Component {
    state = {
        document: null,
        modalOpen: false
    }

    handleModalOpen = () => {
        this.documentGet(this.props.ID_Document);
        this.setState({
            modalOpen: true
        })
    }

    handleModalClose = () => {
        this.setState({
            document: null,
            modalOpen: false
        })
    }

    /**
     * metoda returneaza documentul cautat dupa ID_Document
     * @param ID_Document
     */
    documentGet = (ID_Document) => {
        getDocumentByID_Document(ID_Document)
            .then(raspuns => {
                this.setState({document: raspuns})
            })
    }

    render() {
        return (
            <Modal
                style={{
                    width: "90%"
                }}
                trigger={
                    <Button
                        compact
                        type="button"
                        onClick={this.handleModalOpen}
                        size="tiny">Vezi cerere {this.props.NrDeOrdineTitlu}
                    </Button>
                }
                open={this.state.modalOpen}
                onClose={this.handleModalClose}
                size='large'
                className="afisareModal"
                closeIcon>
                <Modal.Header icon='right arrow'>Detalii cerere</Modal.Header>
                <Modal.Content>
                    {
                        this.state.document === null
                            ? <Loader active inline="centered"/>
                            : <TabelSimplu listaDocumente={[this.state.document]}/>
                    }
                </Modal.Content>
                <Modal.Actions>
                    <Button
                        color='green'
                        type="button"
                        basic
                        onClick={this.handleModalClose}>
                        <Icon name='checkmark'/> Ok
                    </Button>
                </Modal.Actions>
            </Modal>
        )
    }
}

ModalVizualizareInregistrare.propTypes = {
    /**
     * ID-ul documentului care va fi afisat
     */
    ID_Document: PropTypes.number.isRequired,
    /**
     * Numarul de ordine care este afisat pe buton
     */
    NrDeOrdineTitlu: PropTypes.number
}

export default ModalVizualizareInregistrare;

