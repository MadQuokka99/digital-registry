import PropTypes from "prop-types";
import React, {Component} from "react";
import {Button, Dropdown, Icon, Table} from "semantic-ui-react";
import {v4 as uuidv4} from 'uuid';

const optiuniDropdownPredatOriginalCopie = [{
    key: 1,
    text: "Original",
    value: 1,
},
    {
        key: 2,
        text: "Copie",
        value: 2
    }]


class CatreCinePleacaInternComponent extends Component {

    state = {
        traseuDocument: [],
        btnAdaugaElementNouTraseuDisabled: false
    }

    elementTraseuCopie = {
        ID_TraseuDocument: null,
        OrdineTraseu: undefined,
        Departament: {
            key: null,
            text: null,
            value: null
        },
        PredatOriginal: true
    }

    componentDidMount() {
        if(this.props.traseuDocument !== ''){
            this.setState({
                traseuDocument: this.props.traseuDocument
            })
        }
    }

    handleAdaugareElementTraseu = (ID_TraseuDocument, value) => {
        let listaDepartamenteCopy = this.props.listaDepartamente.filter(dep => value === dep.key);

        let traseuDocumentCopy = [...this.state.traseuDocument];

        for (let i = 0; i < traseuDocumentCopy.length; i++) {
            if (traseuDocumentCopy[i].ID_TraseuDocument === ID_TraseuDocument) {
                traseuDocumentCopy[i].Departament.key = listaDepartamenteCopy[0].key;
                traseuDocumentCopy[i].Departament.text = listaDepartamenteCopy[0].text;
                traseuDocumentCopy[i].Departament.value = listaDepartamenteCopy[0].value;
            }
        }

        this.setState({
            traseuDocument: traseuDocumentCopy,
            btnAdaugaElementNouTraseuDisabled: false
        })

        this.props.actualizeazaStateCerere(traseuDocumentCopy);
    }

    handleAdaugareRandTabel = () => {

        let traseuDocumentCopy = [...this.state.traseuDocument];
        let elementTraseu = JSON.parse(JSON.stringify(this.elementTraseuCopie));
        elementTraseu.ID_TraseuDocument = uuidv4();
        elementTraseu.OrdineTraseu = traseuDocumentCopy.length + 1;

        traseuDocumentCopy.push(elementTraseu);

        this.setState({
            traseuDocument: traseuDocumentCopy,
            btnAdaugaElementNouTraseuDisabled: true
        })
    }

    handleStergereRandTabel = (ID_TraseuDocument) => {
        let traseuDocumentCopy = [...this.state.traseuDocument];
        for (let i = 0; i < traseuDocumentCopy.length; i++) {
            if (traseuDocumentCopy[i].ID_TraseuDocument === ID_TraseuDocument) {
                traseuDocumentCopy.splice(i, 1);

                //pentru actualizarea ordinii elementelor din traseu, daca nu sunt sterse in ordine descrescatoare, ci aleatoriu
                for(let j = 0; j < traseuDocumentCopy.length; j++) {
                    traseuDocumentCopy[j].OrdineTraseu = j + 1;
                }
            }
        }
        this.setState({
            traseuDocument: traseuDocumentCopy
        })

        this.props.actualizeazaStateCerere(traseuDocumentCopy);
    }


    handlePredatOriginalUpdate = (ID_TraseuDocument, predatOriginal) => {

        var PredatOriginal;

        if (predatOriginal === 1) {
            PredatOriginal = true;
        } else PredatOriginal = false;

        let traseuDocumentCopy = [...this.state.traseuDocument];

        for (let i = 0; i < traseuDocumentCopy.length; i++) {
            if (traseuDocumentCopy[i].ID_TraseuDocument === ID_TraseuDocument) {
                traseuDocumentCopy[i].PredatOriginal = PredatOriginal;
            }
        }

        this.setState({
            traseuDocument: traseuDocumentCopy
        })

        this.props.actualizeazaStateCerere(traseuDocumentCopy);
    }

    render() {
        return (
            <Table compact celled>
                <Table.Header>
                    <Table.Row textAlign="center">
                        <Table.HeaderCell>Ordine element</Table.HeaderCell>
                        <Table.HeaderCell>Departament</Table.HeaderCell>
                        <Table.HeaderCell>Predat în</Table.HeaderCell>
                        <Table.HeaderCell></Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {this.state.traseuDocument.map((item, index) => {
                        return (
                            <Table.Row key={index}>
                                <Table.Cell>
                                    <p>{item.OrdineTraseu}</p>
                                </Table.Cell>
                                <Table.Cell>
                                    <Dropdown
                                        selection label="Departament" width={6}
                                        options={this.props.listaDepartamente}
                                        search
                                        value={item.Departament.value}
                                        onChange={(e, data) => {
                                            this.handleAdaugareElementTraseu(item.ID_TraseuDocument, data.value)
                                        }}/>
                                </Table.Cell>
                                <Table.Cell>
                                    <Dropdown selection
                                              options={optiuniDropdownPredatOriginalCopie}
                                              value={item.PredatOriginal ? optiuniDropdownPredatOriginalCopie[0].value : optiuniDropdownPredatOriginalCopie[1].value}
                                              onChange={(e, data) => {
                                                  this.handlePredatOriginalUpdate(item.ID_TraseuDocument, data.value)
                                              }}/>
                                </Table.Cell>
                                <Table.Cell collapsing>
                                    <Button
                                        type="button"
                                        floated='right'
                                        icon
                                        color={'red'}
                                        size='small'
                                        onClick={() => this.handleStergereRandTabel(item.ID_TraseuDocument)}>
                                        <Icon name='trash'/>
                                    </Button>
                                </Table.Cell>
                            </Table.Row>
                        )
                    })}
                </Table.Body>

                <Table.Footer>
                    <Table.Row>
                        <Table.HeaderCell/>
                        <Table.HeaderCell colSpan='5'>
                            <Button
                                type="button"
                                floated='right'
                                icon
                                disabled={this.state.btnAdaugaElementNouTraseuDisabled}
                                labelPosition='left'
                                color="green"
                                size='small'
                                onClick={this.handleAdaugareRandTabel}
                            >
                                <Icon name='plus'/> Adaugă
                            </Button>
                        </Table.HeaderCell>
                    </Table.Row>
                </Table.Footer>
            </Table>
        )
    }
}


CatreCinePleacaInternComponent.propTypes = {
    listaDepartamente: PropTypes.array.isRequired,
    emailUser: PropTypes.any.isRequired, //pt personalRegistratura se trimitea -1 si era eroarea de expected string got number
    actualizeazaStateCerere: PropTypes.func.isRequired,
    traseuDocument: PropTypes.any.isRequired
}

export default CatreCinePleacaInternComponent;