import React from "react";
import PropTypes from "prop-types";

import '../Design/PaginaDepuneriCerere.css';
import PopupExplicatiiCampuriDepunere from "./PopupExplicatiiCampuriDepunere";

const RequiredStar = (props) => {
    return(
            <label className={props.esteFieldsObservatii ? "labelStyleFieldObservatii" : props.esteFormGroup ? "labelStyleFormGroup" : "labelStyleForm"}>
            <strong>{props.labelTitle}
                {props.required ?
                <span style={{
                    color: "red"
                }}> * </span> : <span> </span>}
            </strong>
                <PopupExplicatiiCampuriDepunere continutPopup={props.continutPopup}/>
        </label>
    )
}

RequiredStar.propTypes = {
    labelTitle : PropTypes.string.isRequired,
    continutPopup: PropTypes.string,
    required: PropTypes.bool.isRequired,
    esteFormGroup: PropTypes.bool,
    esteFieldsObservatii: PropTypes.bool

}

export default RequiredStar;