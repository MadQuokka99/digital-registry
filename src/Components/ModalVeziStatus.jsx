import PropTypes from "prop-types";
import {Button, Icon, Modal} from "semantic-ui-react";
import React from "react";

const ModalVeziStatus = (props) => {
    return (
        <Modal
            open={props.modalOpen}
            onClose={props.handleModalClose}
            size='fullscreen'
            className="afisareModal"
            closeIcon>
            <Modal.Header icon='right arrow'>{props.modalTitle}</Modal.Header>
            <Modal.Content>
                {props.tabelComponent}
            </Modal.Content>
            <Modal.Actions>
                <Button
                    color='red'
                    type="button"
                    basic
                    onClick={props.handleModalClose}>
                    <Icon name='x'/> Închide
                </Button>
            </Modal.Actions>
        </Modal>
    )
}

ModalVeziStatus.propTypes = {
    handleModalClose: PropTypes.func.isRequired,
    modalOpen: PropTypes.bool.isRequired,
    tabelComponent: PropTypes.node.isRequired,
    modalTitle: PropTypes.string.isRequired
}

export default ModalVeziStatus;