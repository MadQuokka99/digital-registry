import PropTypes from "prop-types";
import {Button, Icon, Modal, Popup} from "semantic-ui-react";
import React, {Component} from "react";
import TabelVeziStatus from "./TabelVeziStatus";
import ModalVeziStatus from "./ModalVeziStatus";

class ModalVeziTraseu extends Component {

    state = {
        modalVeziStatusOpen: false
    }

    handleModalVeziStatusOpen = () => {
        this.setState({
            modalVeziStatusOpen: true
        })
    }

    handleModalVeziStatusClose = () => {
        this.setState({
            modalVeziStatusOpen: false
        })
    }

    render() {
        return (
            <Modal
                open={this.props.modalOpen}
                onClose={this.props.handleModalClose}
                size='fullscreen'
                className="afisareModal"
                closeIcon>
                <Modal.Header icon='right arrow'>{this.props.modalTitle} {this.props.extraInfo !== undefined && this.props.extraInfo}</Modal.Header>
                <Modal.Content>
                    {this.props.tabelComponent}
                </Modal.Content>
                <Modal.Actions>
                    <Popup content={"Apăsați pentru a vedea statusul solicitării curente"} trigger={
                    <Button
                        type="button"
                        color={"green"}
                        onClick={this.handleModalVeziStatusOpen}>
                        <Icon name="chart line"/>
                        Vezi status
                    </Button>}/>
                    <ModalVeziStatus
                        handleModalClose={this.handleModalVeziStatusClose}
                        modalOpen={this.state.modalVeziStatusOpen}
                        tabelComponent={
                            <TabelVeziStatus ID_Document={this.props.ID_Document}/>
                        }
                        modalTitle={"Vezi Status Document"}
                    />
                    <Button
                        color='red'
                        type="button"
                        basic
                        onClick={this.props.handleModalClose}>
                        <Icon name='x'/> Închide
                    </Button>
                </Modal.Actions>
            </Modal>
        )
    }
}

ModalVeziTraseu.propTypes = {
    handleModalClose: PropTypes.func.isRequired,
    modalOpen: PropTypes.bool.isRequired,
    tabelComponent: PropTypes.node.isRequired,
    modalTitle: PropTypes.string.isRequired,
    ID_Document: PropTypes.number.isRequired,
    extraInfo: PropTypes.string
}

export default ModalVeziTraseu;