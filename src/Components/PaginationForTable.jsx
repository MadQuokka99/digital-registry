import PropTypes from "prop-types";
import {Icon, Label, Pagination} from "semantic-ui-react";
import React from "react";

const PaginationForTable = (props) => {
    return (
        <div style={{
            textAlign: "center",
            paddingBottom: "2%"
        }}>
                <span style={{
                    paddingRight: "1%"
                }}>
                    <Label color="green">
                            <p style={{
                                fontSize: "120%"
                            }}><Icon
                                name="folder open outline"/><strong>{props.labelMessage} {props.nrCereri}</strong></p>
                        </Label>
                </span>
            <span>
                            <Pagination
                                defaultActivePage={1}
                                totalPages={Math.ceil(props.nrCereri / 8)}
                                onPageChange={(event, data) => {
                                    if (props.filtruTipUser === undefined) {
                                        props.getCereri(props.detaliiUser.ID_Departament, props.anAlesDinDropdown, data.activePage);
                                    } else {
                                        if (props.filtruTipUser === 'Persoana fizica') {
                                            props.getIstoricCereriByUsername(data.activePage, props.anAlesDinDropdown)
                                        } else if (props.filtruTipUser === 'Departament') {
                                            props.getCereri(props.detaliiUser.ID_Departament, props.anAlesDinDropdown, data.activePage)
                                        }
                                    }
                                }}
                            />
            </span>
        </div>
    )
}

PaginationForTable.propTypes = {
    labelMessage: PropTypes.string.isRequired,
    nrCereri: PropTypes.number,
    detaliiUser: PropTypes.object.isRequired,
    anAlesDinDropdown: PropTypes.number?.isRequired,
    filtruTipUser: PropTypes.string,
    getIstoricCereriByUsername: PropTypes.func,
    getCereri: PropTypes.func.isRequired

}
export default PaginationForTable;