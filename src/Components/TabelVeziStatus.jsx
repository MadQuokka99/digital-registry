import React, {Component} from "react";
import axios from "../Utils/axios-AGSIS-API";
import {Table} from "semantic-ui-react";
import moment from 'moment';
import MessageComponent from "./MessageComponent";

class TabelVeziStatus extends Component {

    state = {
        statusuriDocument: [],
        statusNesetat: false
    }

    componentDidMount() {
        axios
            .get(`Registratura/StatusDocumentListByID_Document?ID_Document=${this.props.ID_Document}`)
            .then(response => {
                if(response.data.length === 0){
                    this.setState({
                        statusNesetat: true
                    })
                }

                this.setState({
                    statusuriDocument: response.data
                })
            })
    }

    render() {
        return (
            this.state.statusNesetat ?
                <MessageComponent iconName={"settings"} message1={"Acest document nu are un status setat!"} size={"large"} info={true} compact={true}/>
                : <Table celled compact striped>
                    <Table.Header>
                        <Table.Row textAlign="center">
                            <Table.HeaderCell>Status</Table.HeaderCell>
                            <Table.HeaderCell>Data modificării</Table.HeaderCell>
                            <Table.HeaderCell>Cine a făcut modificarea</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        {this.state.statusuriDocument.map(item => {
                            return (
                                <Table.Row key={item.ID_StatusDocument}>
                                    <Table.Cell collapsing>{item.DenumireStatus}</Table.Cell>
                                    <Table.Cell>{moment(item.DataModificareStatus).format("DD-MM-YYYY")}</Table.Cell>
                                    <Table.Cell>{item.UsernameModificareStatus}</Table.Cell>
                                </Table.Row>)
                        })}
                    </Table.Body>
                </Table>

        )
    }
}

export default TabelVeziStatus;