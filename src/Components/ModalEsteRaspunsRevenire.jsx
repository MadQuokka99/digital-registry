import {Button, Dropdown, Form, Grid, Icon, Modal, Popup} from "semantic-ui-react";
import PropTypes from "prop-types";
import React from "react";

const ModalEsteRaspunsRevenire = (props) => {
    let esteRaspuns = false;
    if(props.tipSolicitare === "EsteRaspunsLa_ID_Document"){
        esteRaspuns = true;
    }
    return (
        <Modal
            trigger={
                <Button
                    compact
                    color="green"
                    type="button"
                    onClick={props.handleModalOpen}
                    size="tiny">
                    <Icon name="edit"/>
                    {props.messageBtn}
                </Button>
            }
            open={props.modalOpen}
            onClose={props.handleModalClose}
            size='small'
            className="afisareModal"
            closeIcon>
            <Modal.Header icon='right arrow'>{props.messageHeader}</Modal.Header>
            <Modal.Content>
                <Grid>
                    <Grid.Row columns={3}>
                        <Grid.Column width={6}>
                            <p>Numărul de ordine:</p>
                            <Dropdown search deburr selection width={4}
                                      options={props.listaCereriServerSide}
                                      placeholder="Introduceți numărul de ordine"
                                      onChange={(e, data) =>
                                          props.handleRezultatAlesDinDropdownRaspunsRevenire(data.value, props.tipSolicitare)}
                                      onSearchChange={(e) => props.cautaCerereServerSide(e.target.value)}/>
                        </Grid.Column>
                        <Grid.Column width={3}>
                            <br/>
                            <br/>
                            <Popup
                                content="Apăsarea acestui buton va reseta alegerea făcută la valoarea inițială"
                                trigger={
                                    <Button icon color="teal" type="button" basic
                                            onClick={() => props.handleReseteazaEsteRaspunsRevenireLa(esteRaspuns)}
                                    >
                                        <Icon name="redo"/> Resetează
                                    </Button>
                                }/>
                        </Grid.Column>
                        <Grid.Column width={3}>
                            <br/>
                            <br/>
                            <Popup
                                content={props.popupContent}
                                trigger={
                                    <Button icon color="red" type="button" basic
                                            onClick={() => props.handleStergeEsteRaspunsRevenireLa(esteRaspuns)}
                                    >
                                        <Icon name="x"/> Șterge
                                    </Button>
                                }/>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column>
                            <Form.Input fluid label={props.labelMessage}
                                        readOnly width={6}
                                        value={props.textRevenireRaspuns}/>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Modal.Content>
            <Modal.Actions>
                <Popup
                    content="Salvarea modificării se face prin apăsarea butonului 'Salvează'!"
                    trigger={
                        <Button
                            color='green'
                            type="button"
                            basic
                            onClick={props.handleModalClose}>
                            <Icon name='checkmark'/> Ok
                        </Button>}
                />
            </Modal.Actions>
        </Modal>
    )
}

ModalEsteRaspunsRevenire.propTypes = {
    handleModalOpen: PropTypes.func.isRequired,
    handleModalClose: PropTypes.func.isRequired,
    messageBtn: PropTypes.string.isRequired,
    messageHeader: PropTypes.string.isRequired,
    modalOpen: PropTypes.bool.isRequired,
    listaCereriServerSide: PropTypes.array.isRequired,
    handleRezultatAlesDinDropdownRaspunsRevenire: PropTypes.func.isRequired,
    tipSolicitare: PropTypes.string.isRequired,
    cautaCerereServerSide: PropTypes.func.isRequired,
    labelMessage: PropTypes.string.isRequired,
    textRevenireRaspuns: PropTypes.string.isRequired,
    popupContent: PropTypes.string.isRequired,
    handleReseteazaEsteRaspunsRevenireLa: PropTypes.func.isRequired,
    handleStergeEsteRaspunsRevenireLa: PropTypes.func.isRequired
}

 export default ModalEsteRaspunsRevenire;