import React, {Component} from 'react'
import PropTypes from "prop-types";
import {Table, Button, Icon, Popup} from "semantic-ui-react";
import moment from "moment";
import '../Design/TableStyle.css';
import ModalVeziTraseu from "./ModalVeziTraseu";
import TabelVeziTraseu from "./TabelVeziTraseu";

class TabelSimplu extends Component {

    state = {
        modalVeziTraseuOpen: false,
        idDocumentVeziTraseu: null
    }


    handleModalVeziTraseuLaOpen = (ID_Document) => {
        this.setState({
            modalVeziTraseuOpen: true,
            idDocumentVeziTraseu: ID_Document
        })
    }

    handleModalVeziTraseuClose = () => {
        this.setState({
            modalVeziTraseuOpen: false
        })
    }

    render() {
        return (
            <Table className="tableScroll">
                <Table.Header>
                    <Table.Row textAlign="center">
                        <Table.HeaderCell>Data înregistrare</Table.HeaderCell>
                        <Table.HeaderCell>Tipul solicitării</Table.HeaderCell>
                        <Table.HeaderCell>Nr de ordine</Table.HeaderCell>
                        <Table.HeaderCell>Este răspuns la</Table.HeaderCell>
                        <Table.HeaderCell>Departament emitent/facultate emitentă</Table.HeaderCell>
                        <Table.HeaderCell>Stadiu solicitare</Table.HeaderCell>
                        <Table.HeaderCell>Conținutul solicitării</Table.HeaderCell>
                        <Table.HeaderCell>Termen limită</Table.HeaderCell>
                        <Table.HeaderCell>Traseu intern document</Table.HeaderCell>
                        <Table.HeaderCell>Actul se află la</Table.HeaderCell>
                        <Table.HeaderCell>Nume solicitant</Table.HeaderCell>
                        <Table.HeaderCell>E-mail solicitant</Table.HeaderCell>
                        <Table.HeaderCell>Autoritate emitentă</Table.HeaderCell>
                        <Table.HeaderCell>Este revenire la</Table.HeaderCell>
                        <Table.HeaderCell>Nr actului intrat</Table.HeaderCell>
                        <Table.HeaderCell>Către cine pleacă extern</Table.HeaderCell>
                        <Table.HeaderCell>Modalitate de trimitere</Table.HeaderCell>
                        <Table.HeaderCell>Observații solicitant</Table.HeaderCell>
                        <Table.HeaderCell>Mențiuni Registratură</Table.HeaderCell>
                        <Table.HeaderCell>Nume angajat care înregistrează</Table.HeaderCell>
                        <Table.HeaderCell>E-mail angajat care înregistrează</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {this.props.listaDocumente.map((item) => {
                        return (
                            <Table.Row key={item.ID_Document}>
                                <Table.Cell>{item.EsteInregistrat ? moment(item.DataInregistrare).format("DD-MM-YYYY") : "-"}</Table.Cell>
                                <Table.Cell>{item.DenumireTipDocument}</Table.Cell>
                                <Table.Cell>{!item.EsteInregistrat ?
                                    <Popup trigger={
                                        <Icon name="warning circle"
                                              color="red"
                                        />}
                                           content="Cererea dvs a fost salvată cu succes, va fi înregistrată și va primi un număr de înregistrare în următoarea zi lucrătoare"
                                    />
                                    : item.EsteInregistrat ? item.NrDeOrdine : null}</Table.Cell>
                                <Table.Cell>{item.EsteRaspunsLa_NrOrdine}</Table.Cell>
                                <Table.Cell className="textDepasesteDimensiune" title={item.CineAEmis_NumeDepartament}>{item.CineAEmis_NumeDepartament}</Table.Cell>
                                <Table.Cell>{item.DenumireStatus}</Table.Cell>
                                <Table.Cell>{item.ContinutulActului}</Table.Cell>
                                <Table.Cell>{item.TermenLimita === null ? "-" : moment(item.TermenLimita).format("DD-MM-YYYY")}</Table.Cell>
                                <Table.Cell>
                                    <Button
                                        compact
                                        type="button"
                                        onClick={() => this.handleModalVeziTraseuLaOpen(item.ID_Document)}
                                        size="tiny">
                                        <Icon name="sitemap"/>
                                        Vezi traseu
                                    </Button>

                                    {this.state.idDocumentVeziTraseu === item.ID_Document &&
                                    <ModalVeziTraseu
                                        handleModalClose={this.handleModalVeziTraseuClose}
                                        modalOpen={this.state.modalVeziTraseuOpen}
                                        tabelComponent={
                                            <TabelVeziTraseu ID_Document={item.ID_Document}/>
                                        }
                                        modalTitle={"Vezi Traseu Intern Document"}
                                    />}
                                </Table.Cell>
                                <Table.Cell className="textDepasesteDimensiune" title={item.PredatLa_NumeDepartament}>{item.PredatLa_NumeDepartament}</Table.Cell>
                                <Table.Cell className="textDepasesteDimensiune" title={item.NumeDeponent}>{item.NumeDeponent}</Table.Cell>
                                <Table.Cell>{item.MailDeponent}</Table.Cell>
                                <Table.Cell className="textDepasesteDimensiune" title={item.CineAEmis_Autoritate}>{item.CineAEmis_Autoritate}</Table.Cell>
                                <Table.Cell>{item.EsteRevenireLa_NrOrdine}</Table.Cell>
                                <Table.Cell className="textDepasesteDimensiune" title={item.NrActuluiIntrat}>{item.NrActuluiIntrat}</Table.Cell>
                                <Table.Cell className="textDepasesteDimensiune" title={item.TrimiteLaInstitutia}>{item.TrimiteLaInstitutia}</Table.Cell>
                                <Table.Cell>{item.DenumireModTrimitereRaspuns}</Table.Cell>
                                <Table.Cell className="textDepasesteDimensiune" title={item.ObservatiiSolicitant}>{item.ObservatiiSolicitant}</Table.Cell>
                                <Table.Cell className="textDepasesteDimensiune" title={item.MentiuniRegistratura}>{item.MentiuniRegistratura}</Table.Cell>
                                <Table.Cell className="textDepasesteDimensiune" title={item.NumeAngajatDeponent}>{item.NumeAngajatDeponent}</Table.Cell>
                                <Table.Cell className="textDepasesteDimensiune" title={item.MailAngajatDeponent}>{item.MailAngajatDeponent}</Table.Cell>
                            </Table.Row>)
                    })}
                </Table.Body>
            </Table>
        )
    }
}

TabelSimplu.propTypes = {
    listaDocumente: PropTypes.array.isRequired
}

export default TabelSimplu;
