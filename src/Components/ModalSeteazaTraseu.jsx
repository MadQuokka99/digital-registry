import PropTypes from "prop-types";
import {Button, Icon, Modal} from "semantic-ui-react";
import React, {Component} from "react";
import TabelAddItemStatus from "./TabelAddItemStatus";
import ModalSeteazaStatus from "./ModalSeteazaStatus";
import "../Design/ModalStyle.css";

class ModalSeteazaTraseu extends Component {

    state = {
        modalStatusOpen: false
    }

    handleModalStatusOpen = () => {
        this.setState({
            modalStatusOpen: true
        })
    }

    handleModalStatusClose = () => {
        this.setState({
            modalStatusOpen: false
        })
    }

    render() {
    return(
        <Modal
            trigger={
                <Button
                    color={this.props.color}
                    type="button"
                    compact
                    size="tiny"
                    onClick={this.props.handleModalOpen}>
                    <Icon name={this.props.iconName}/>
                    {this.props.textBtnTriggerModal}
                </Button>
            }
            open={this.props.modalOpen}
            onClose={this.props.handleModalClose}
            size='fullscreen'
            className="afisareModal"
            closeIcon>
            <Modal.Header icon='right arrow'>{this.props.modalTitle} {this.props.extraInfo !== undefined && this.props.extraInfo}</Modal.Header>
            <Modal.Content>
                {this.props.tabelComponent}
            </Modal.Content>
            <Modal.Actions>
                {(this.props.stadiiCereri !== undefined && this.props.ID_Document !== undefined && this.props.ePersonalRegistratura !== undefined && this.props.username !== undefined) &&
                <ModalSeteazaStatus
                    handleModalOpen={this.handleModalStatusOpen}
                    handleModalClose={this.handleModalStatusClose}
                    modalOpen={this.state.modalStatusOpen}
                    textBtnTriggerModal={"Setează status"}
                    modalTitle={"Setează Status Document"}
                    iconName={"chart line"}
                    color={"green"}
                    tabelComponent={
                        <TabelAddItemStatus listaStatusuri={this.props.stadiiCereri}
                                            ID_Document={this.props.ID_Document}
                                            username={this.props.username}
                                            ePersonalRegistratura={this.props.ePersonalRegistratura}/>
                    }
                />}
                <Button
                    color='red'
                    type="button"
                    basic
                    onClick={this.props.handleModalClose}>
                    <Icon name='x'/> Închide
                </Button>
            </Modal.Actions>
        </Modal>
    )}
}

ModalSeteazaTraseu.propTypes = {
    handleModalOpen: PropTypes.func.isRequired,
    handleModalClose: PropTypes.func.isRequired,
    modalOpen: PropTypes.bool.isRequired,
    textBtnTriggerModal: PropTypes.string.isRequired,
    modalTitle: PropTypes.string.isRequired,
    tabelComponent: PropTypes.node.isRequired,
    iconName: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
    stadiiCereri: PropTypes.array,
    ID_Document: PropTypes.number,
    username: PropTypes.any,
    ePersonalRegistratura: PropTypes.bool,
    extraInfo: PropTypes.string
}

export default ModalSeteazaTraseu;