import React, {Component} from "react";
import descarcaFisier from "../Utils/descarcareFisier";

import MainPage from "./MainPage";
import {Checkbox, Grid, Item, Icon, Table, Button, Form} from "semantic-ui-react";
import {getSetariRegistratura, getZileLibere, updateRegistraturaSetari} from "../Utils/ApiCalls";

class PaginaSetariRegistratura extends Component {

    state = {
        ziLibereList: [],
        setariRegistratura: {},

        permiteDepuneriInAfaraProgramului: false,
        paginaInMentenanta: false,

        descarcaRegistru: {
            dataDeCand: '',
            dataPanaCand: '',
            loading: false
        }
    }

    componentDidMount() {

        getZileLibere().then(response => {
            this.setState({
                ziLibereList: response
            })
        })
        getSetariRegistratura()
            .then(setari => {
                this.setState({
                    setariRegistratura: setari,
                    permiteDepuneriInAfaraProgramului: setari.PermiteDepuneriInAfaraProgramului,
                    paginaInMentenanta: setari.UnderMaintenance.isUnderMaintenance
                })
            })
    }

    descarcaRegistru = () => {
        let descarcaRegistru = {...this.state.descarcaRegistru};
        descarcaRegistru.loading = true;
        this.setState({descarcaRegistru: descarcaRegistru});

        descarcaFisier(`Registratura/DocumentPrintGet?dataDeCand=${this.state.descarcaRegistru.dataDeCand}&dataPanaCand=${this.state.descarcaRegistru.dataPanaCand}`)
            .catch(() => {
                alert("A apărut o eroare la descărcarea documentului!");
            })
            .finally(() => {
                let descarcaRegistru = {...this.state.descarcaRegistru};
                descarcaRegistru.loading = false;
                this.setState({descarcaRegistru: descarcaRegistru});
            })
    }

    seteaza = (denumireSetare) => {
        let obiectDeTrimis = {...this.state.setariRegistratura};

        if (denumireSetare === "PermiteDepuneriInAfaraProgramului") {
            obiectDeTrimis.PermiteDepuneriInAfaraProgramului = !this.state.permiteDepuneriInAfaraProgramului;
        } else if (denumireSetare === "UnderMaintenance") {
            obiectDeTrimis.UnderMaintenance.isUnderMaintenance = !this.state.paginaInMentenanta;
        }

        updateRegistraturaSetari(obiectDeTrimis)
            .then(raspuns => {

                if (denumireSetare === "PermiteDepuneriInAfaraProgramului") {
                    this.setState({
                        permiteDepuneriInAfaraProgramului: raspuns.PermiteDepuneriInAfaraProgramului
                    })
                } else if (denumireSetare === "UnderMaintenance") {
                    this.setState({
                        paginaInMentenanta: raspuns.UnderMaintenance.isUnderMaintenance
                    })
                }
            })
    }

    seteazaDataRegistruUnic = () => {

    }

    render() {
        return (
            <Grid>
                <MainPage
                    match={this.props.match}
                    titluPagina="Pagina Setări Registratură"
                    afiseazaDropdownDepartamente={false}/>
                <Grid.Row style={{
                    paddingLeft: "5%",
                    paddingTop: "3%"
                }}>
                    <Grid.Column>
                        <Item.Group divided relaxed>
                            <Item>
                                <Item.Content>
                                    <Item.Header>
                                        <Icon name="cloud download" color="green"/>
                                        Descarcă Registru Unic în format pdf
                                    </Item.Header>
                                    <Item.Description>
                                        <div>Descarcă înregistrările din Registrul Unic între cele 2 date specificate:
                                        </div>
                                        <Form>
                                            <Form.Group inline>
                                                <Form.Input
                                                    type="date"
                                                    label="De la"
                                                    value={this.state.descarcaRegistru.dataDeCand}
                                                    onChange={(e) => {
                                                        let data = {...this.state.descarcaRegistru};
                                                        data.dataDeCand = e.target.value;
                                                        this.setState({descarcaRegistru: data})
                                                    }}
                                                />
                                                <Form.Input
                                                    type="date"
                                                    label="Până la"
                                                    value={this.state.descarcaRegistru.dataPanaCand}
                                                    onChange={(e) => {
                                                        let data = {...this.state.descarcaRegistru};
                                                        data.dataPanaCand = e.target.value;
                                                        this.setState({descarcaRegistru: data})
                                                    }}
                                                />
                                            </Form.Group>
                                        </Form>
                                    </Item.Description>
                                    <Item.Extra>
                                        <Button
                                            color={"green"}
                                            disabled={this.state.descarcaRegistru.dataDeCand === null || this.state.descarcaRegistru.dataPanaCand === null}
                                            type="button"
                                            loading={this.state.descarcaRegistru.loading}
                                            onClick={this.descarcaRegistru}>
                                            Descarcă
                                        </Button>
                                    </Item.Extra>
                                </Item.Content>
                            </Item>
                            <Item>
                                <Item.Content>
                                    <Item.Header>
                                        <Icon name="upload" color="green"/>
                                        Permite depuneri în afara programului
                                    </Item.Header>
                                    <Item.Description>
                                        Prin apăsarea butonului se va da solicitanților opțiunea de depunere a
                                        solicitărilor în afara programului de lucru
                                    </Item.Description>
                                    <Item.Extra>
                                        <Checkbox toggle
                                                  label="Permite depuneri în afara programului - nu se vor genera numere de înregistrare!"
                                                  checked={this.state.permiteDepuneriInAfaraProgramului}
                                                  onClick={() => this.seteaza("PermiteDepuneriInAfaraProgramului")}
                                        />
                                    </Item.Extra>
                                </Item.Content>
                            </Item>
                            <Item>
                                <Item.Content>
                                    <Item.Header>
                                        <Icon name="clock"/>
                                        Setare program lucru Registratură
                                    </Item.Header>
                                    <Item.Description>
                                        Permite setarea programului de lucru pentru Registratură
                                    </Item.Description>
                                    <Item.Extra>
                                        <p>Oră început de program: </p> <input type="time"/>
                                        <p>Oră sfârșit de program: </p> <input type="time"/>
                                    </Item.Extra>
                                </Item.Content>
                            </Item>
                            <Item>
                                <Item.Content>
                                    <Item.Header>
                                        <Icon name="hotel" color="blue"/>
                                        Setare zile libere
                                    </Item.Header>
                                    <Item.Description>
                                        Permite setarea zilelor naționale libere
                                    </Item.Description>
                                    <Item.Extra>
                                        <Table compact celled definition>
                                            <Table.Header>
                                                <Table.Row>
                                                    <Table.HeaderCell></Table.HeaderCell>
                                                    <Table.HeaderCell>Denumire</Table.HeaderCell>
                                                    <Table.HeaderCell>Zi</Table.HeaderCell>
                                                    <Table.HeaderCell>Lună</Table.HeaderCell>
                                                    <Table.HeaderCell>An</Table.HeaderCell>
                                                    <Table.HeaderCell></Table.HeaderCell>
                                                </Table.Row>
                                            </Table.Header>

                                            <Table.Body>
                                                {this.state.ziLibereList.map(item => {
                                                    return (
                                                        <Table.Row key={item.ID_ZiLibera}>
                                                            <Table.Cell><Button type="button"
                                                                                content="Editează"/></Table.Cell>
                                                            <Table.Cell>{item.DenumireZiLibera}</Table.Cell>
                                                            <Table.Cell>{item.Zi}</Table.Cell>
                                                            <Table.Cell>{item.Luna}</Table.Cell>
                                                            <Table.Cell>{item.An}</Table.Cell>
                                                            <Table.Cell><Button type="button"
                                                                                content="Finalizează"/></Table.Cell>
                                                        </Table.Row>)
                                                })}
                                            </Table.Body>

                                            <Table.Footer fullWidth>
                                                <Table.Row>
                                                    <Table.HeaderCell/>
                                                    <Table.HeaderCell colSpan='5'>
                                                        <Button
                                                            type="button"
                                                            floated='right'
                                                            icon
                                                            labelPosition='left'
                                                            primary
                                                            size='small'
                                                        >
                                                            <Icon name='user'/> Adaugă
                                                        </Button>
                                                    </Table.HeaderCell>
                                                </Table.Row>
                                            </Table.Footer>
                                        </Table>
                                    </Item.Extra>
                                </Item.Content>
                            </Item>
                            <Item>
                                <Item.Content>
                                    <Item.Header>
                                        <Icon name="warning sign" color="red"/> Afișează pagina „În mentenanță”
                                    </Item.Header>

                                    <Item.Extra>
                                        <Checkbox toggle
                                                  label="Panic button"
                                                  checked={this.state.paginaInMentenanta}
                                                  onClick={() => this.seteaza("UnderMaintenance")}
                                        />
                                    </Item.Extra>
                                </Item.Content>
                            </Item>
                        </Item.Group>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        )
    }

}

export default PaginaSetariRegistratura;