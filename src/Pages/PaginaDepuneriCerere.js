import {
    Button,
    Icon,
    Grid,
    Step,
    Form,
} from 'semantic-ui-react';
import React, {Component} from 'react';
import moment from "moment";
import 'moment/locale/ro';

import Autosuggest from 'react-autosuggest'

import 'react-dates/lib/css/_datepicker.css';
import 'react-dates/initialize';

import axios from '../Utils/axios-AGSIS-API';
import {
    getTipuriSolicitari,
    getDepartamente,
    EsteInTimpulProgramului,
    estePersonalRegistratura,
    formatarePtDropdown,
    formatareDepUserPtDropdown,
    formatareDepPtDropdown,
    getDocumentByID_Document,
    getModuriTrimitere,
    formatareNumePtDropdown,
    formatareEmailPtDropdown, getNumeEmailByUsername, getDepartamenteDinCareFaceParte, getSetariRegistratura
} from '../Utils/ApiCalls'

import {
    formatareTextEsteRevenireRaspuns,
    getSugestii,
    getSugestiiNumeEmailAutosuggest
} from '../Utils/FunctiiAutosuggest';

import "../Design/PaginaDepuneriCerere.css";
import {getUsername} from "../Utils/GetUsername";
import MainPage from "./MainPage";
import TabelSimplu from "../Components/TabelSimplu";
import MessageComponent from "../Components/MessageComponent";
import ModalEstiSigurCa from "../Components/ModalEstiSigurCa";
import {cautaCerereServerSide} from "../Utils/FunctiiIstoricCereriIntrate";
import RequiredStar from "../Components/RequiredStar";
import ModalSeteazaTraseu from "../Components/ModalSeteazaTraseu";
import CatreCinePleacaInternComponent from "../Components/CatreCinePleacaInternComponent";

/**
 * optiunile pentru dropdown-ul de alegere a tipului de solicitant
 */
const optiuniDropdownCineAEmisCererea = [{
    key: 1,
    text: "Persoana fizica",
    value: "Persoana fizica",
},
    {
        key: 2,
        text: "Autoritate emitenta",
        value: "Autoritate emitenta",
    }
]


const optiuniDropdownEsteRaspunsLa = [
    {
        key: 1,
        text: "Da",
        value: "Da",
        disabled: false
    },
    {
        key: 2,
        text: "Nu",
        value: "Nu",
        disabled: false
    }
]

/**
 *
 * @param info
 * component Step.Group pentru afisarea pasilor pe care utilizatorul trebuie sa ii urmeze
 */
const pasiDeUrmatUtilizator = (info) => (
    <Step.Group ordered stackable="tablet">
        <Step active>
            <Step.Content>
                <Step.Title>Alege {info}</Step.Title>
            </Step.Content>
        </Step>

        <Step active>
            <Step.Content>
                <Step.Title>Completează restul informațiilor</Step.Title>
            </Step.Content>
        </Step>

        <Step active>
            <Step.Content>
                <Step.Title>Depune solicitarea</Step.Title>
            </Step.Content>
        </Step>
    </Step.Group>
)

function getSuggestionValue(suggestion) {
    return suggestion;
}

function renderSuggestion(suggestion) {
    return (
        <span>{suggestion}</span>
    );
}

function getSuggestionValueName(suggestion) {
    return suggestion.nume;
}

function renderSuggestionValueName(suggestion) {
    return (
        <span>{suggestion.nume}</span>
    );
}

function getSuggestionValueEmail(suggestion) {
    return suggestion.email;
}

function renderSuggestionValueEmail(suggestion) {
    return (
        <span>{suggestion.email}</span>
    );
}


class PaginaDepuneriCerere extends Component {

    state = {
        cerere: {
            dataDepunerii: moment().format("DD-MM-YYYY"),
            ID_Document: null,
            esteRaspuns: {
                value: undefined,
                validation: {
                    required: false
                },
                valid: true,
                touched: false,
                error: false
            }, //daca actul de iesire este raspuns la o alta solicitare sau nu
            esteRevenire: {
                value: undefined,
                validation: {
                    required: false
                },
                valid: true,
                touched: false,
                error: false
            }, //daca solicitarea depusa are acelasi subiect cu o solicitare depusa anterior de acelasi solicitant
            tipulActului: {
                value: "",
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                error: false
            }, //act de intrare sau iesire
            numarActIntrare: {
                value: "",
                validation: {
                    required: false
                },
                valid: true,
                touched: false,
                error: false
            }, //"NrActului Intrat"
            esteRaspunsLaSolicitarea: {
                value: undefined,
                validation: {
                    required: false
                },
                valid: true,
                touched: false,
                error: false
            }, //nr de ordine daca actul de iesire este raspuns la o alta solicitare
            esteRevenireLaSolicitarea: {
                value: undefined,
                validation: {
                    required: false
                },
                valid: true,
                touched: false,
                error: false
            }, //nr de ordine al solicitarii catre care face referire solicitarea depusa
            tipUser: {
                value: "",
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                error: false
            }, //de ales dintre Autoritatea emitentă sau Persoana fizica
            departamentEmitent: {
                value: "",
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                error: false
            }, //cine a depus solicitarea
            autoritateEmitenta: {
                value: "",
                validation: {
                    required: false
                },
                valid: true,
                touched: false,
                error: false
            }, //cine a depus solicitarea
            emailSolicitant: {
                value: "",
                validation: {
                    required: false
                },
                valid: true,
                touched: false,
                error: false
            },  // "Cine_a_depus"
            numeIntreg: {
                value: "",
                validation: {
                    required: false
                },
                valid: true,
                touched: false,
                error: false
            }, //numele si prenumele utilizatorului care depune cererea
            numeAngajat: {
                value: "",
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                error: false
            },
            emailAngajat: {
                value: "",
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                error: false
            },
            continutCerere: {
                value: "",
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                error: false
            }, //"Continutul actului"
            catreCinePleaca: {
                value: "",
                validation: {
                    required: false
                },
                valid: true,
                touched: false,
                error: false
            },
            catreCinePleacaIntern: {
                value: [],
                validation: {
                    required: false
                },
                valid: true,
                touched: false,
                error: false
            },
            modalitateTrimitere: {
                value: "",
                validation: {
                    required: false
                },
                valid: true,
                touched: false,
                error: false
            },
            termenLimita: {
                value: "",
                validation: {
                    required: false
                },
                valid: true,
                touched: false,
                error: false
            },
            solicitareFinalizata: false,
            solicitareExpediata: false,
            observatiiUtilizator: "", //observatiile pe care solicitantul le specifica la depunerea documentului
        },

        listaDepartamente: [], //lista departamentelor din Universitate
        departamenteUser: [], //lista departamentelor userului

        tipuriCereri: [],  //lista de tipuri de solicitari

        openModalConfirmare: false, //state pentru Modalul de confirmare a depunerii cererilor
        succesfulPost: false, //state pentru realizarea cu succes sau nu a POST-ului in SharePoint a cererilor
        responsePost: [], //obiect pentru afisare rezumat dupa post
        btnOkDepune: false,

        listaCereriServerSide: [], //lista cererilor returnate din functia de cautare server side

        formIsValid: false, //state boolean pentru verificare daca formularul este valid sau nu

        esteRaspunsLaText: "",  //pentru campul read-only Este raspuns la solicitarea
        esteRevenireLaText: "", //pentru campul read-only Este revenire la solicitarea

        detaliiUser: {     //informatiile despre utilizatorul logat
            email: "",
            nume: "",
        },
        listaUsers: [],

        esteInTimpulProgramului: true, //state boolean pentru a vedea daca este in afara programului sau nu sau in zilele libere
        ePersonalRegistratura: false, //state boolean pentru a verifica daca utilizatorul este personal din Registratura

        continutValue: '', //pentru implementarea Autosuggest.js pentru continut
        optiuniContinut: [], //array ce contine continutul pentru dropdown-ul de 'Alege continut cerere' pentru depunerile catre Rectorat

        //pentru Autosuggest pentru nume angajat
        numeAngajatValue: '',
        optiuniNumeAngajat: [],

        //pentru Autosuggest pentru nume deponent
        numeDeponentValue: '',
        optiuniNumeDeponent: [],

        //pentru Autosuggest pentru email deponent
        emailDeponentValue: '',
        optiuniEmailDeponent: [],

        //pentru Autosuggest pentru email angajat
        emailAngajatValue: '',
        optiuniEmailAngajat: [],

        autoritateValue: '',    //pentru Autosuggest pentru autoritatea
        optiuniAutoritate: [],

        catreCinePleacaValue: '',    //pentru Autosuggest pentru catreCinePleaca
        optiuniCatreCinePleaca: [],

        aniPtMetodaGetDocByNrDeOrdine: {
            an1: null,
            an2: null
        },

        modalitatiTrimitere: [],
        modalActulPleacaLaOpen: false,
        setariRegistratura: undefined,
        afiseazaCampTermenLimita: false
    }


    async componentDidMount() {

        let username = getUsername();
        const rol = 'PersonalRegistratura';

        /**
         * este actualizat emailul utilizatorului si in campul emailSolicitant
         */
        let cerereCopy = {...this.state.cerere};

        this.setState({
            cerere: cerereCopy
        })

        await Promise.all([
                getTipuriSolicitari(),
                getDepartamente(),
                EsteInTimpulProgramului(),
                estePersonalRegistratura(rol, username),
                getModuriTrimitere(), getSetariRegistratura()
            ]
        ).then(([tipuriSolicitari,
                    departamente, esteInTimpulProgramului,
                    estePersonalRegistratura, modalitatiTrimitere, setariRegistratura]) => {

            if (estePersonalRegistratura) {
                username = -1;
            }

            this.setState({
                tipuriCereri: formatarePtDropdown(tipuriSolicitari, "ID_TipDocument", "DenumireTipDocument"),
                listaDepartamente: formatareDepPtDropdown(false, departamente), //este false, deoarece la true ID_Departament se pune -1
                esteInTimpulProgramului: esteInTimpulProgramului,
                ePersonalRegistratura: estePersonalRegistratura,
                detaliiUser: {
                    email: username
                },
                modalitatiTrimitere: formatarePtDropdown(modalitatiTrimitere, "ID_ModTrimitereRaspuns", "DenumireModTrimitereRaspuns"),
                setariRegistratura: setariRegistratura

            })
        })
            .catch(error => {
                console.log(error);
            })

        getNumeEmailByUsername(username)
            .then(infoUser => {
                getDepartamenteDinCareFaceParte(username)
                    .then(departamenteUser => {
                        estePersonalRegistratura(rol, getUsername())
                            .then(response => {
                                if (!response) {
                                    this.setState({
                                        detaliiUser: {
                                            nume: formatareNumePtDropdown(infoUser)[0].value,
                                            email: formatareEmailPtDropdown(infoUser)[0].value
                                        }
                                    })
                                    cerereCopy.numeAngajat.value = formatareNumePtDropdown(infoUser)[0].value;
                                    cerereCopy.numeAngajat.valid = true;
                                    cerereCopy.numeAngajat.touched = true;
                                    cerereCopy.emailAngajat.value = formatareEmailPtDropdown(infoUser)[0].value;
                                    cerereCopy.emailAngajat.valid = true;
                                    cerereCopy.emailAngajat.touched = true;
                                    cerereCopy.departamentEmitent.value = formatareDepUserPtDropdown(departamenteUser)[0].value;
                                    cerereCopy.departamentEmitent.valid = true;
                                    cerereCopy.departamentEmitent.touched = true;
                                    //this.handleActualizeazaForm("numeAngajat", formatareNumePtDropdown(infoUser)[0].value);
                                    //this.handleActualizeazaForm("emailAngajat", formatareEmailPtDropdown(infoUser)[0].value);
                                    //this.handleActualizeazaForm("departamentEmitent", formatareDepUserPtDropdown(departamenteUser)[0].value);


                                    let afiseazaCamp = this.afiseazaCampTermenLimita(cerereCopy.departamentEmitent.value);

                                    this.setState({
                                        cerere: cerereCopy,
                                        departamenteUser: formatareDepUserPtDropdown(departamenteUser),
                                        afiseazaCampTermenLimita: afiseazaCamp
                                    })
                                }
                            })
                    })
            })

        this.setState({
            aniPtMetodaGetDocByNrDeOrdine: {
                an1: moment().year(),
                an2: moment().year() - 1
            }
        })
    }

    afiseazaCampTermenLimita = (departamentAlesValue) => {

        let departamentCuTermenLimita = this.state.setariRegistratura.DepartamenteCarePotActualizaTermenLimita;
        let afiseaza = false;
        for(let i= 0; i < departamentCuTermenLimita.length; i++) {
            if (departamentCuTermenLimita[i] === departamentAlesValue) {
                afiseaza = true;
            }
        }
        return afiseaza;
    }

    /**
     * metoda care deschide Modalul pentru confirmarea depunerii solicitarii
     */
    handleOpenModalConfirmare = () => this.setState({
        openModalConfirmare: true,
    })

    /**
     * metoda care inchide Modalul pentru confirmarea depunerii solicitarii
     */
    handleCloseModalConfirmare = () => {
        this.setState({openModalConfirmare: false})
    }


    /**
     *
     * @param e
     * @param value
     *
     * pentru fileUpload
     *
     */
    // handleClick = (e, titleProps) => {
    //     const {index} = titleProps
    //     const {activeIndex} = this.state
    //     const newIndex = activeIndex === index ? -1 : index
    //
    //     this.setState({activeIndex: newIndex})
    // }

    /**
     *
     * @param rules
     * @param value
     *
     * pentru fileUpload
     */
    // handleFileUpload = (key, value, index) => {
    //
    //     const formData = new FormData();
    //
    //     // Update the formData object
    //     formData.append(
    //         "myFile",
    //         value,
    //         value.name
    //     );
    //
    //     let listaCereriCopy = [...this.state.listaCereri];
    //     listaCereriCopy[index][key] = value;
    //
    //     this.setState({
    //         listaCereri: listaCereriCopy
    //     })
    // }


    checkValidity(value, rules) {
        let isValid = true;
        if (rules.required) {
            if (isNaN(value)) {
                isValid = (value.trim() !== '' && isValid) || (value.trim() !== undefined && isValid);
            } else {
                isValid = value !== '' && isValid;
            }
        }
        if (rules.minLength) {
            isValid = value.length >= rules.minLength && isValid
        }
        if (rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid
        }
        return isValid;
    }

    setFieldsTrueFalse = (cerere, fieldsArray, value) => {
        let bool = false;

        if (value) {
            bool = true;
        }

        for (let i = 0; i < fieldsArray.length; i++) {
            cerere[fieldsArray[i]]["valid"] = bool;
            cerere[fieldsArray[i]]["validation"]["required"] = !bool;
        }
    }

    setFieldValueTo = (cerere, fieldArray, value) => {
        for (let i = 0; i < fieldArray.length; i++) {
            cerere[fieldArray[i]]["value"] = value;
        }
    }

    setFieldValueToTrueFalse = (cerere, fieldArray, value) => {
        for (let i = 0; i < fieldArray.length; i++) {
            cerere[fieldArray[i]] = value;
        }
    }

    /**
     *
     * @param key
     * @param value
     *
     * metoda actualizeaza informatiile completate in formular in state-ul cerere
     */
    handleActualizeazaForm = (key, value) => {
        let cerereCopy = {...this.state.cerere};

        if (key === "tipulActului") {

            if (value === 5 || value === 6) {

                let fieldsArrayTrue = ["esteRevenireLaSolicitarea", "esteRevenire", "autoritateEmitenta", "numeIntreg", "modalitateTrimitere", "departamentEmitent", "tipUser"];
                this.setFieldsTrueFalse(cerereCopy, fieldsArrayTrue, true);

                let fieldsArrayFalse = ["esteRaspuns", "continutCerere"];
                this.setFieldsTrueFalse(cerereCopy, fieldsArrayFalse, false);

                let fieldsArrayValueUndefined = ["esteRevenire", "esteRevenireLaSolicitarea"];
                this.setFieldValueTo(cerereCopy, fieldsArrayValueUndefined, undefined);

                let fieldsArrayValueDepartament = ["departamentEmitent"];
                let departamentValue;
                if (this.state.ePersonalRegistratura) {
                    departamentValue = '';
                    this.setFieldsTrueFalse(cerereCopy, fieldsArrayValueDepartament, false);
                } else {
                    departamentValue = this.state.departamenteUser[0].value;
                }
                this.setFieldValueTo(cerereCopy, fieldsArrayValueDepartament, departamentValue);

                let fieldsArrayValueEmptyString = ["tipUser", "numarActIntrare", "catreCinePleaca", "numeIntreg", "continutCerere", "autoritateEmitenta", "modalitateTrimitere", "emailSolicitant", "termenLimita"];
                this.setFieldValueTo(cerereCopy, fieldsArrayValueEmptyString, "");

                let fieldsArrayValueToFalse = ["solicitareFinalizata", "solicitareExpediata"];
                this.setFieldValueToTrueFalse(cerereCopy, fieldsArrayValueToFalse, false);

                if (cerereCopy.esteRaspuns.value !== undefined) {
                    let fieldsArrayValueUndefined = ["esteRaspunsLaSolicitarea"];
                    this.setFieldValueTo(cerereCopy, fieldsArrayValueUndefined, undefined);

                    let fieldsArrayTrue = ["esteRaspuns"];
                    this.setFieldsTrueFalse(cerereCopy, fieldsArrayTrue, true);

                } else {
                    let fieldsArrayFalse = ["esteRaspuns"];
                    this.setFieldsTrueFalse(cerereCopy, fieldsArrayFalse, false);
                    this.setFieldValueTo(cerereCopy, fieldsArrayFalse, undefined);
                }

                if (cerereCopy.numeAngajat.value === "") {
                    let fieldsArrayFalse = ["numeAngajat", "emailAngajat"];
                    this.setFieldsTrueFalse(cerereCopy, fieldsArrayFalse, false);
                    this.setFieldValueTo(cerereCopy, fieldsArrayFalse, '');

                    this.setState({
                        emailAngajatDeponentValue: "",
                        numeAngajatDeponentValue: ""
                    })
                }

                this.setState({
                    cerere: cerereCopy,
                    emailDeponentValue: "",
                    numeDeponentValue: "",
                    autoritateValue: "",
                    continutValue: "",
                    esteRaspunsLaText: "",
                    esteRevenireLaText: "",
                    catreCinePleacaValue: '',
                })

                if (value === 5) {
                    let fieldsArrayTrue = ["catreCinePleacaIntern"];
                    this.setFieldsTrueFalse(cerereCopy, fieldsArrayTrue, true);
                    this.setFieldValueTo(cerereCopy, fieldsArrayTrue, []);

                    let fieldsArrayFalse = ["catreCinePleaca"];
                    this.setFieldsTrueFalse(cerereCopy, fieldsArrayFalse, false);
                }

                if (value === 6) {
                    let fieldsArrayTrue = ["catreCinePleaca"];
                    this.setFieldsTrueFalse(cerereCopy, fieldsArrayTrue, true);
                    this.setFieldValueTo(cerereCopy, fieldsArrayTrue, "");

                    let fieldsArrayFalse = ["catreCinePleacaIntern"];
                    this.setFieldsTrueFalse(cerereCopy, fieldsArrayFalse, false);

                }

            } else if (value === 4) {


                let fieldsArrayTrue = ["esteRaspuns", "esteRaspunsLaSolicitarea", "catreCinePleaca", "modalitateTrimitere", "departamentEmitent", "catreCinePleacaIntern", "numeIntreg"];
                this.setFieldsTrueFalse(cerereCopy, fieldsArrayTrue, true);

                let fieldsArrayFalse = ["esteRevenire", "continutCerere", "tipUser"];
                this.setFieldsTrueFalse(cerereCopy, fieldsArrayFalse, false);

                this.setFieldValueTo(cerereCopy, ["catreCinePleacaIntern"], []);

                let fieldsArrayValueEmptyString = ["catreCinePleaca", "continutCerere", "numeIntreg", "tipUser", "modalitateTrimitere"];
                this.setFieldValueTo(cerereCopy, fieldsArrayValueEmptyString, "");

                let fieldsArrayValueUndefined = ["esteRaspuns", "esteRaspunsLaSolicitarea"];
                this.setFieldValueTo(cerereCopy, fieldsArrayValueUndefined, undefined);

                let fieldsArrayValueToFalse = ["solicitareFinalizata", "solicitareExpediata"];
                this.setFieldValueToTrueFalse(cerereCopy, fieldsArrayValueToFalse, false);

                let fieldsArrayValueDepartament = ["departamentEmitent"];

                let departamentValue;
                if (this.state.ePersonalRegistratura) {
                    departamentValue = '';
                    this.setFieldsTrueFalse(cerereCopy, fieldsArrayValueDepartament, false);
                } else {
                    departamentValue = this.state.departamenteUser[0].value;
                }
                this.setFieldValueTo(cerereCopy, fieldsArrayValueDepartament, departamentValue);

                this.setState({
                    cerere: cerereCopy,
                    numeDeponentValue: "",
                    continutValue: "",
                    catreCinePleacaValue: '',
                    esteRevenireLaText: '',
                    esteRaspunsLaText: ''
                })
            }
        }

        if (key === 'esteRaspuns') {

            if ((value === 'Nu' && cerereCopy.tipulActului.value === 5) || (value === 'Nu' && cerereCopy.tipulActului.value === 6)) {

                let fieldsArrayValueEmptyString = ["numeIntreg", "continutCerere", "emailSolicitant"];
                this.setFieldValueTo(cerereCopy, fieldsArrayValueEmptyString, "");

                let fieldsArrayTrue = ["numeIntreg"];
                this.setFieldsTrueFalse(cerereCopy, fieldsArrayTrue, true);

                let fieldsArrayFalse = ["continutCerere"];
                this.setFieldsTrueFalse(cerereCopy, fieldsArrayFalse, false);

                this.setState({
                    cerere: cerereCopy,
                    emailDeponentValue: "",
                    numeDeponentValue: "",
                    continutValue: "",
                    esteRevenireLaText: "",
                    esteRaspunsLaText: ""
                })
            }

            if (value === 'Da') {

                let fieldsArrayTrue = ["esteRaspuns"];
                this.setFieldsTrueFalse(cerereCopy, fieldsArrayTrue, true);

                let fieldsArrayFalse = ["esteRaspunsLaSolicitarea", "continutCerere"];
                this.setFieldsTrueFalse(cerereCopy, fieldsArrayFalse, false);

                let fieldsArrayValueEmptyString = ["continutCerere"];
                this.setFieldValueTo(cerereCopy, fieldsArrayValueEmptyString, "");

                let fieldsArrayValueUndefined = ["esteRaspunsLaSolicitarea",];
                this.setFieldValueTo(cerereCopy, fieldsArrayValueUndefined, undefined);

                this.setState({
                    cerere: cerereCopy,
                    esteRaspunsLaText: "",
                    esteRevenireLaText: "",
                    continutValue: "",
                })

            } else if (value === 'Nu') {

                let fieldsArrayTrue = ["esteRaspunsLaSolicitarea"];
                this.setFieldsTrueFalse(cerereCopy, fieldsArrayTrue, true);

                let fieldsArrayValueUndefined = ["esteRaspunsLaSolicitarea"];
                this.setFieldValueTo(cerereCopy, fieldsArrayValueUndefined, undefined);

                let fieldsArrayFalse = ["continutCerere"];
                this.setFieldsTrueFalse(cerereCopy, fieldsArrayFalse, false);

                let fieldsArrayValueEmptyString = ["continutCerere"];
                this.setFieldValueTo(cerereCopy, fieldsArrayValueEmptyString, "");

                this.setState({
                    cerere: cerereCopy,
                    esteRaspunsLaText: "",
                    esteRevenireLaText: "",
                    continutValue: "",
                })
            }
        }

        if (key === 'esteRevenire') {

            if (value === 'Da') {

                let fieldsArrayFalse = ["esteRevenireLaSolicitarea"];
                this.setFieldsTrueFalse(cerereCopy, fieldsArrayFalse, false);

                this.setState({
                    cerere: cerereCopy,
                    esteRaspunsLaText: "",
                    esteRevenireLaText: ""
                })

            } else if (value === 'Nu') {

                let fieldsArrayValueUndefined = ["esteRevenireLaSolicitarea"];
                this.setFieldValueTo(cerereCopy, fieldsArrayValueUndefined, undefined);

                let fieldsArrayTrue = ["esteRevenireLaSolicitarea", "autoritateEmitenta"];
                this.setFieldsTrueFalse(cerereCopy, fieldsArrayTrue, true);

                let fieldsArrayValueEmptyString = ["continutCerere", "autoritateEmitenta", "tipUser", "numeIntreg", "emailSolicitant"];
                this.setFieldValueTo(cerereCopy, fieldsArrayValueEmptyString, "");

                let fieldsArrayFalse = ["tipUser", "numeIntreg"];
                this.setFieldsTrueFalse(cerereCopy, fieldsArrayFalse, true);

                this.setState({
                    cerere: cerereCopy,
                    emailDeponentValue: "",
                    numeDeponentValue: "",
                    autoritateValue: "",
                    continutValue: "",
                    esteRaspunsLaText: "",
                    esteRevenireLaText: ""
                })
            }
        }

        if(key === "departamentEmitent"){
            let afiseazaCamp = this.afiseazaCampTermenLimita(value);
            this.setState({
                afiseazaCampTermenLimita: afiseazaCamp
            })
            if(!afiseazaCamp){
                cerereCopy.termenLimita.value = "";
            }
        }

        if (key === "tipUser") {
            if (value === "Autoritate emitenta") {
                if (cerereCopy.tipulActului.value === 6 || cerereCopy.tipulActului.value === 5) {
                    let fieldsArrayTrue = ["autoritateEmitenta"];
                    this.setFieldsTrueFalse(cerereCopy, fieldsArrayTrue, true);
                } else if (cerereCopy.tipulActului.value === 4) {
                    let fieldsArrayFalse = ["autoritateEmitenta"];
                    this.setFieldsTrueFalse(cerereCopy, fieldsArrayFalse, false);
                }

                let fieldsArrayValueEmptyString = ["autoritateEmitenta"];
                this.setFieldValueTo(cerereCopy, fieldsArrayValueEmptyString, "");

                let fieldsArrayTrue = ["numeIntreg"];
                this.setFieldsTrueFalse(cerereCopy, fieldsArrayTrue, true);
                this.setFieldValueTo(cerereCopy, fieldsArrayTrue, "");


                this.setState({
                    cerere: cerereCopy,
                    autoritateValue: '',
                    numeDeponentValue: ''
                })

            } else if (value === 'Persoana fizica') {

                let fieldsArrayTrue = ["autoritateEmitenta"];
                this.setFieldsTrueFalse(cerereCopy, fieldsArrayTrue, true);

                let fieldsArrayFalse = ["numeIntreg"];
                this.setFieldsTrueFalse(cerereCopy, fieldsArrayFalse, false);

                let fieldsArrayValueEmptyString = ["autoritateEmitenta", "numeIntreg"];
                this.setFieldValueTo(cerereCopy, fieldsArrayValueEmptyString, "");

                this.setState({
                    cerere: cerereCopy,
                    numeDeponentValue: '',
                    autoritateValue: ''
                })
            }
        }

        if (key === "solicitareFinalizata" && !value) {
            let fieldsArrayValueToFalse = ["solicitareExpediata"];
            this.setFieldValueToTrueFalse(cerereCopy, fieldsArrayValueToFalse, false);

            let fieldsArrayValueEmptyString = ["modalitateTrimitere"];
            this.setFieldValueTo(cerereCopy, fieldsArrayValueEmptyString, "");

            let fieldsArrayTrue = ["modalitateTrimitere"];
            this.setFieldsTrueFalse(cerereCopy, fieldsArrayTrue, true);
        }

        if (key === "solicitareExpediata" && value) {
            let fieldsArrayFalse = ["modalitateTrimitere"];
            this.setFieldsTrueFalse(cerereCopy, fieldsArrayFalse, false);

        } else if (key === "solicitareExpediata" && !value) {

            let fieldsArrayValueEmptyString = ["modalitateTrimitere"];
            this.setFieldValueTo(cerereCopy, fieldsArrayValueEmptyString, "");

            let fieldsArrayTrue = ["modalitateTrimitere"];
            this.setFieldsTrueFalse(cerereCopy, fieldsArrayTrue, true);
        }


        let updatedFormElement = {
            ...cerereCopy[key]
        };

        if (typeof updatedFormElement === 'object' && updatedFormElement !== null && updatedFormElement.hasOwnProperty('value')) {
            updatedFormElement.value = value;
            updatedFormElement.valid = this.checkValidity(updatedFormElement.value, updatedFormElement.validation);
            updatedFormElement.touched = true;
            updatedFormElement.error = false; // conditie pt a se face campurile type error


        } else {
            updatedFormElement = value;
        }

        cerereCopy[key] = updatedFormElement;

        this.verificaToataCererea(cerereCopy);

        this.setState({
            cerere: cerereCopy
        });
    }

    verificaToataCererea = (cerereCopy) => {
        let formIsValid = true;
        for (let key in cerereCopy) {
            if (typeof cerereCopy[key] === 'object' && cerereCopy[key] !== null && cerereCopy[key].hasOwnProperty('value')) {
                formIsValid = cerereCopy[key].valid && formIsValid;
            }
        }
        this.setState({
            formIsValid: formIsValid
        })
    }

    actualizeazaStateCerere = (traseuDocument) => {
        let cerereCopy = {...this.state.cerere};
        cerereCopy.catreCinePleacaIntern.value = traseuDocument;
        cerereCopy.catreCinePleacaIntern.value.length === 0 ? cerereCopy.catreCinePleacaIntern.valid = false : cerereCopy.catreCinePleacaIntern.valid = true;

        this.verificaToataCererea(cerereCopy);

        this.setState({
            cerere: cerereCopy
        })
    }

    handleActualizeazaNumeEmail = (key1, value1, key2, value2) => {

        const cerereCopy = {...this.state.cerere};
        let updatedFormElementName = {
            ...cerereCopy[key1]
        };
        updatedFormElementName.value = value1;
        updatedFormElementName.valid = true;
        cerereCopy[key1] = updatedFormElementName;


        let updatedFormElementEmail = {
            ...cerereCopy[key2]
        };
        updatedFormElementEmail.value = value2;
        updatedFormElementEmail.valid = true;

        cerereCopy[key2] = updatedFormElementEmail;

        this.verificaToataCererea(cerereCopy);

        this.setState({
            cerere: cerereCopy
        });

    }
    /**
     *
     * @param nrDeOrdine
     *
     */
    cautaCerereServerSide = (nrDeOrdine) => {
        cautaCerereServerSide(nrDeOrdine, this.state.aniPtMetodaGetDocByNrDeOrdine)
            .then(listaCereri => {
                this.setState({
                    listaCereriServerSide: listaCereri
                })
            })
    }


    handleRezultatAlesDinDropdownRaspunsRevenire = (result, key, tipSolicitare) => {
        getDocumentByID_Document(result)
            .then(raspuns => {

                if (key === "esteRaspuns") {
                    if (tipSolicitare === 5) {
                        this.handleActualizeazaForm("continutCerere", raspuns.ContinutulActului);
                        this.handleActualizeazaForm("catreCinePleaca", raspuns.CineAEmis_Autoritate);
                        this.handleActualizeazaForm("esteRaspunsLaSolicitarea", result);

                        this.setState({
                            continutValue: raspuns.ContinutulActului,
                            catreCinePleacaValue: raspuns.CineAEmis_Autoritate,
                            listaCereriServerSide: [],
                            esteRevenireLaText: "",
                            esteRaspunsLaText: formatareTextEsteRevenireRaspuns(raspuns)
                        })
                    } else if (tipSolicitare === 6) {
                        this.handleActualizeazaForm("continutCerere", raspuns.ContinutulActului);
                        this.handleActualizeazaForm("esteRaspunsLaSolicitarea", result);

                        let departamentEmitentSolicitareOriginala = this.state.listaDepartamente.filter(dep => raspuns.CineAEmis_ID_Departament === dep.key);

                        if (departamentEmitentSolicitareOriginala.length !== 0) {
                            let traseuDocument = [{
                                Departament: {
                                    key: departamentEmitentSolicitareOriginala[0].key,
                                    text: departamentEmitentSolicitareOriginala[0].text,
                                    value: departamentEmitentSolicitareOriginala[0].value
                                },
                                OrdineTraseu: 1,
                                PredatOriginal: true
                            }];

                            this.actualizeazaStateCerere(traseuDocument);
                        }

                        this.setState({
                            continutValue: raspuns.ContinutulActului,
                            listaCereriServerSide: [],
                            esteRevenireLaText: "",
                            esteRaspunsLaText: formatareTextEsteRevenireRaspuns(raspuns)
                        })
                    }
                } else if (key === "esteRevenire") {

                    this.handleActualizeazaForm("continutCerere", raspuns.ContinutulActului);
                    this.handleActualizeazaForm("numarActIntrare", raspuns.NrActuluiIntrat);
                    this.handleActualizeazaForm("esteRevenireLaSolicitarea", result);
                    this.handleActualizeazaForm("numeIntreg", raspuns.NumeDeponent);
                    this.handleActualizeazaForm("emailSolicitant", raspuns.MailDeponent);

                    this.setState({
                        emailDeponentValue: raspuns.MailDeponent,
                        numeDeponentValue: raspuns.NumeDeponent,
                        autoritateValue: raspuns.CineAEmis_Autoritate,
                        continutValue: raspuns.ContinutulActului,
                        listaCereriServerSide: [],
                        esteRaspunsLaText: "",
                        esteRevenireLaText: formatareTextEsteRevenireRaspuns(raspuns)
                    })

                    if (raspuns.CineAEmis_Autoritate !== "") {
                        this.handleActualizeazaForm("tipUser", optiuniDropdownCineAEmisCererea[1].value);
                        this.handleActualizeazaForm("autoritateEmitenta", raspuns.CineAEmis_Autoritate);
                        this.handleActualizeazaForm("numeIntreg", raspuns.NumeDeponent);
                        this.handleActualizeazaForm("emailSolicitant", raspuns.MailDeponent);

                        this.setState({
                            emailDeponentValue: raspuns.MailDeponent,
                            numeDeponentValue: raspuns.NumeDeponent,
                            autoritateValue: raspuns.CineAEmis_Autoritate,
                        })

                    } else {
                        this.handleActualizeazaForm("tipUser", optiuniDropdownCineAEmisCererea[0].value);
                        this.handleActualizeazaForm("numeIntreg", raspuns.NumeDeponent);
                        this.handleActualizeazaForm("emailSolicitant", raspuns.MailDeponent);
                        this.setState({
                            emailDeponentValue: raspuns.MailDeponent,
                            numeDeponentValue: raspuns.NumeDeponent
                        })
                    }
                }
            })
    }


    /**
     * metoda realizeaza POST-ul atunci cand este apasat butonul 'Trimite' din Modalul de confirmare depunere cereri
     */
    postCereri = () => {

        let cerereCopy = {...this.state.cerere};
        let cerereDeTrimis;

        let traseuDocument = [];
        let catreCinePleacaInternCopy = cerereCopy.catreCinePleacaIntern.value;
        for (let i = 0; i < catreCinePleacaInternCopy.length; i++) {
            let element = {
                PredatLa_ID_Departament: catreCinePleacaInternCopy[i].Departament.value,
                OrdineTraseu: catreCinePleacaInternCopy[i].OrdineTraseu,
                PredatOriginal: catreCinePleacaInternCopy[i].PredatOriginal
            };
            traseuDocument.push(element);
        }

        cerereDeTrimis = {
            "MailDeponent": cerereCopy.emailSolicitant.value,
            "ContinutulActului": cerereCopy.continutCerere.value.toString(),
            "NrActuluiIntrat": cerereCopy.numarActIntrare.value,
            "ID_TipDocument": cerereCopy.tipulActului.value,
            "EsteRaspunsLa_ID_Document": cerereCopy.esteRaspunsLaSolicitarea.value,
            "ObservatiiSolicitant": cerereCopy.observatiiUtilizator,
            "EsteRevenireLa_ID_Document": cerereCopy.esteRevenireLaSolicitarea.value,
            "CineAEmis_Autoritate": cerereCopy.autoritateEmitenta.value,
            "CineAEmis_ID_Departament": cerereCopy.departamentEmitent.value,
            "TrimiteLaInstitutia": cerereCopy.catreCinePleaca.value,
            "NumeDeponent": cerereCopy.numeIntreg.value,
            "NumeAngajatDeponent": cerereCopy.numeAngajat.value,
            "MailAngajatDeponent": cerereCopy.emailAngajat.value,
            "TraseuDocument": traseuDocument,
            "ID_ModTrimitereRaspuns": cerereCopy.modalitateTrimitere.value === '' ? null : cerereCopy.modalitateTrimitere.value,
            "SolicitareFinalizata": cerereCopy.solicitareFinalizata,
            "SolicitareExpediata": cerereCopy.solicitareExpediata,
            "TermenLimita": cerereCopy.termenLimita.value
        }

        this.setState({
            btnOkDepune: true
        })

        axios
            .post('Registratura/DepuneDocument', cerereDeTrimis)
            .then(response => {
                this.setState({
                    btnOkDepune: false,
                    responsePost: response.data,
                    succesfulPost: true,
                    openModalConfirmare: false
                })

            })
            .catch(error => {
                console.log(error);
                alert("Ceva nu a funcționat, acțiunea nu a fost înregistrată! " + error.response.data);

                this.setState({
                    btnOkDepune: false,
                    succesfulPost: false,
                    openModalConfirmare: false
                })
            })
    }

    handleModalActulPleacaLaOpen = () => {
        this.setState({
            modalActulPleacaLaOpen: true
        })
    }

    handleModalActulPleacaLaClose = () => {
        this.setState({
            modalActulPleacaLaOpen: false
        })
    }

    getSugestiiContinut = ({value}) => {

        getSugestii(value, "ContinutulActului", this.state.detaliiUser.email)
            .then(sugestiiContinut => {
                this.setState({
                    optiuniContinut: sugestiiContinut
                })
            })
    }

    clearSugestiiContinut = () => {
        this.setState({
            optiuniContinut: []
        })
    }

    //pentru Autosuggest nume angajat
    getSugestiiNume = ({value}) => {
        getSugestiiNumeEmailAutosuggest(value, 'nume')
            .then(sugestiiNume => {
                this.setState({
                    optiuniNumeAngajat: sugestiiNume
                })
            })
    }

    clearSugestiiNume = () => {
        this.setState({
            optiuniNumeAngajat: []
        })
    }


    onNameAngajatSuggestionSelected = (event, {suggestion}) => {
        this.handleActualizeazaNumeEmail("numeAngajat", suggestion.nume, "emailAngajat", suggestion.email);
        this.setState({
            emailAngajatValue: suggestion.email
        });
    };

    getSugestiiEmail = ({value}) => {
        getSugestiiNumeEmailAutosuggest(value, 'username')
            .then(sugestiiEmail => {
                this.setState({
                    optiuniEmailAngajat: sugestiiEmail
                })
            })
    }

    clearSugestiiEmail = () => {
        this.setState({
            optiuniEmailAngajat: []
        })
    }


    onEmailAngajatSuggestionSelected = (event, {suggestion}) => {
        this.handleActualizeazaNumeEmail("numeAngajat", suggestion.nume, "emailAngajat", suggestion.email);
        this.setState({
            numeAngajatValue: suggestion.nume
        });
    };

    getSugestiiAutoritateEmitenta = ({value}) => {
        getSugestii(value, "CineAEmis_Autoritate", this.state.detaliiUser.email)
            .then(optiuniAutoritateResponse => {
                this.setState({
                    optiuniAutoritate: optiuniAutoritateResponse
                })
            })
    }

    clearSugestiiAutoritateEmitenta = () => {
        this.setState({
            optiuniAutoritate: []
        })
    }

    getSugestiiNumeDeponent = ({value}) => {
        getSugestii(value, "NumeDeponent", this.state.detaliiUser.email)
            .then(optiuniNumeDeponentResponse => {
                this.setState({
                    optiuniNumeDeponent: optiuniNumeDeponentResponse
                })
            })
    }

    clearSugestiiNumeDeponent = () => {
        this.setState({
            optiuniNumeDeponent: []
        })
    }

    getSugestiiEmailDeponent = ({value}) => {
        getSugestii(value, "MailDeponent", this.state.detaliiUser.email)
            .then(optiuniEmailDeponentResponse => {
                this.setState({
                    optiuniEmailDeponent: optiuniEmailDeponentResponse
                })
            })
    }

    clearSugestiiEmailDeponent = () => {
        this.setState({
            optiuniEmailDeponent: []
        })
    }

    getSugestiiCatreCinePleaca = ({value}) => {
        getSugestii(value, "TrimiteLaInstitutia", this.state.detaliiUser.email)
            .then(optiuniCatreCinePleacaResponse => {
                this.setState({
                    optiuniCatreCinePleaca: optiuniCatreCinePleacaResponse
                })
            })
    }

    clearSugestiiCatreCinePleaca = () => {
        this.setState({
            optiuniCatreCinePleaca: []
        })
    }


    render() {
        const {openModalConfirmare} = this.state;
        const {cerere} = this.state;

        const {
            numeDeponentValue,
            optiuniNumeDeponent,
            emailDeponentValue,
            optiuniEmailDeponent,
            numeAngajatValue,
            optiuniNumeAngajat,
            emailAngajatValue,
            optiuniEmailAngajat,
            continutValue,
            optiuniContinut,
            autoritateValue,
            optiuniAutoritate,
            optiuniCatreCinePleaca,
            catreCinePleacaValue
        } = this.state;
        const numeDeponentInputProps = {
            placeholder: "Nume solicitant",
            value: numeDeponentValue,
            onChange: (event, {newValue}) => {
                this.handleActualizeazaForm("numeIntreg", newValue)
                this.setState({
                    numeDeponentValue: newValue
                })
            }
        };
        const emailDeponentInputProps = {
            placeholder: "Email solicitant",
            value: emailDeponentValue,
            onChange: (event, {newValue}) => {
                this.handleActualizeazaForm("emailSolicitant", newValue)
                this.setState({
                    emailDeponentValue: newValue
                })
            }
        };
        const numeAngajatInputProps = {
            placeholder: "Nume angajat deponent",
            value: numeAngajatValue,
            onChange: (event, {newValue}) => {
                this.handleActualizeazaForm("numeAngajat", newValue)
                this.setState({
                    numeAngajatValue: newValue
                })
            }
        };
        const emailAngajatInputProps = {
            placeholder: "Email angajat deponent",
            value: emailAngajatValue,
            onChange: (event, {newValue}) => {
                this.handleActualizeazaForm("emailAngajat", newValue)
                this.setState({
                    emailAngajatValue: newValue
                })
            }
        };
        const continutInputProps = {
            placeholder: "Conținutul actului",
            value: continutValue,
            onChange: (event, {newValue}) => {
                this.handleActualizeazaForm("continutCerere", newValue)
                this.setState({
                    continutValue: newValue
                })
            }
        };
        const autoritateInputProps = {
            placeholder: "Autoritate emitentă",
            value: autoritateValue,
            onChange: (event, {newValue}) => {
                this.handleActualizeazaForm("autoritateEmitenta", newValue)
                this.setState({
                    autoritateValue: newValue
                })
            }
        };
        const catreCinePleacaInputProps = {
            placeholder: "Către cine pleacă",
            value: catreCinePleacaValue,
            onChange: (event, {newValue}) => {
                this.handleActualizeazaForm("catreCinePleaca", newValue)
                this.setState({
                    catreCinePleacaValue: newValue
                })
            }
        };

        return (
            <Grid style={{
                paddingBottom: "3%"
            }}>
                <MainPage
                    match={this.props.match}
                    titluPagina={"Pagina Depuneri Solicitări"}
                    afiseazaDropdownDepartamente={false}/>

                <div>
                    {!this.state.succesfulPost &&
                    <Grid.Row style={{
                        textAlign: "center",
                        paddingBottom: "0%"
                    }}>
                        {this.state.esteInTimpulProgramului ?
                            <Grid.Column style={{
                                paddingLeft: "25%",
                                paddingRight: "25%",
                                paddingTop: "0%",
                                paddingBottom: "0%",
                                textAlign: "center"
                            }}>
                                {pasiDeUrmatUtilizator("tipul solicitării")}
                            </Grid.Column> :

                            <Grid.Column style={{
                                paddingLeft: "34%",
                                paddingTop: "0.2%",
                                paddingBottom: "0%",
                                textAlign: "center"
                            }}>
                                <MessageComponent iconName={"warning"}
                                                  message1={"Sunteți în afara programului!"}
                                                  message2={"Puteți înregistra doar solicitări de ieșire sau interne ca răspuns, restul solicitărilor vor primi număr de ordine în următoarea zi lucrătoare!"}
                                                  size={"small"}
                                                  style={{
                                                      width: "48%"
                                                  }}
                                                  negative={true}
                                />
                            </Grid.Column>
                        }
                    </Grid.Row>}

                    {!this.state.succesfulPost &&
                    <div>
                        <Grid.Row centered>
                            <Grid.Column style={{
                                paddingLeft: "10%",
                            }}>
                                <Form size="large">
                                    <p><strong>Data depunerii: </strong>{cerere.dataDepunerii}</p>

                                    <RequiredStar labelTitle={"Tipul solicitării"} required={true}
                                                  continutPopup={"Intrare - solicitare care intra în Universitate; Ieșire - solicitare care iese din Universitate; Intern - solicitare trimisă în cadrul Universității"}/>
                                    <Form.Dropdown selection placeholder="Alege tipul solicitării"
                                                   options={this.state.tipuriCereri} width={3}
                                                   error={cerere.tipulActului.error}
                                                   onChange={((e, data) => this.handleActualizeazaForm('tipulActului', data.value))}
                                    />

                                    <Form.Group>
                                        {cerere.tipulActului.value === 4 &&
                                        <div>
                                            <RequiredStar labelTitle={"Solicitarea este revenire"} required={true}
                                                          esteFormGroup={true}
                                                          continutPopup={"Dacă ați mai depus o solicitare asemănătoare, alegeți 'Da'; în caz contrar, alegeți 'Nu'"}/>
                                            <Form.Dropdown selection
                                                           width={3} error={cerere.esteRevenire.error}
                                                           options={optiuniDropdownEsteRaspunsLa}
                                                           onChange={((e, data) => this.handleActualizeazaForm('esteRevenire', data.value))}/>
                                        </div>
                                        }
                                        {cerere.esteRevenire.value === 'Da' &&
                                        <div style={{
                                            width: "30%",
                                            paddingLeft: "2%"
                                        }}>
                                            <RequiredStar labelTitle={"Este revenire la"} required={true}
                                                          continutPopup={"Este necesară specificarea numărului de ordine al solicitării la care se face referire"}/>
                                            <Form.Dropdown search deburr selection
                                                           error={cerere.esteRevenireLaSolicitarea.error}
                                                           options={this.state.listaCereriServerSide}
                                                           placeholder="Introduceți numărul de ordine"
                                                           onChange={(e, data) => this.handleRezultatAlesDinDropdownRaspunsRevenire(data.value, "esteRevenire", cerere.tipulActului.value)}
                                                           onSearchChange={(e) => this.cautaCerereServerSide(e.target.value)}/>
                                        </div>}

                                        {cerere.esteRevenire.value === 'Da' && cerere.esteRevenireLaSolicitarea.value !== undefined &&
                                        <div style={{
                                            paddingLeft: "2%",
                                            width: "50%"
                                        }}>
                                            <p><strong>Solicitarea este revenire la: </strong></p>
                                            <p>{this.state.esteRevenireLaText}</p>
                                        </div>
                                        }
                                    </Form.Group>

                                    <Form.Group>
                                        {(cerere.tipulActului.value === 5 || cerere.tipulActului.value === 6) &&
                                        <div>
                                            <RequiredStar labelTitle={"Solicitarea este răspuns"} required={true}
                                                          esteFormGroup={true}
                                                          continutPopup={"Dacă depuneți un răspuns la o solicitare de intrare, alegeți 'Da'; în caz contrar, alegeți 'Nu'"}/>
                                            <Form.Dropdown selection width={3} error={cerere.esteRaspuns.error}
                                                           options={optiuniDropdownEsteRaspunsLa}
                                                           onChange={((e, data) => this.handleActualizeazaForm('esteRaspuns', data.value))}/>
                                        </div>}
                                        {cerere.esteRaspuns.value === 'Da' &&
                                        <div style={{
                                            width: "30%",
                                            paddingLeft: "2%"
                                        }}>
                                            <RequiredStar labelTitle={"Este răspuns la"} required={true}
                                                          continutPopup={"Este necesară specificarea numărului de ordine al solicitării la care se dă răspuns"}/>
                                            <Form.Dropdown search deburr selection
                                                           error={cerere.esteRaspunsLaSolicitarea.error}
                                                           placeholder="Introduceți numărul de ordine"
                                                           options={this.state.listaCereriServerSide}
                                                           onChange={(e, data) => this.handleRezultatAlesDinDropdownRaspunsRevenire(data.value, "esteRaspuns", cerere.tipulActului.value)}
                                                           onSearchChange={(e) => this.cautaCerereServerSide(e.target.value)}/>
                                        </div>}

                                        {cerere.esteRaspuns.value === 'Da' && cerere.esteRaspunsLaSolicitarea.value !== undefined &&
                                        <div style={{
                                            paddingLeft: "2%",
                                            width: "50%"
                                        }}>
                                            <p><strong>Solicitarea este răspuns la: </strong></p>
                                            <p>{this.state.esteRaspunsLaText}</p>
                                        </div>}
                                    </Form.Group>


                                    {cerere.tipulActului.value === 4 &&
                                    <Form.Input style={{
                                        width: "20%"
                                    }}
                                                label={
                                                    <RequiredStar labelTitle={"Numărul actului intrat"} required={false}
                                                                  continutPopup={"Poate fi specificat numărul de ordine al documentului intrat, dacă se cunoaște"}/>
                                                }
                                                onChange={(event) => this.handleActualizeazaForm('numarActIntrare', event.target.value)}/>}

                                    {(cerere.esteRaspuns.value !== undefined || cerere.esteRevenire.value !== undefined) &&
                                    <div>
                                        <RequiredStar required={true} labelTitle={
                                            cerere.tipulActului.value === 4 ? "Departament/facultate/structură din Universitate care a primit"
                                                : ((cerere.tipulActului.value === 5 || cerere.tipulActului.value === 6) && cerere.esteRaspuns.value === 'Da')
                                                ? 'Departamentul care face răspunsul' : (cerere.tipulActului.value === 5 && cerere.esteRaspuns.value === 'Nu') ? "Departamentul care face înregistrarea de ieșire"
                                                    : "Departamentul care face înregistrarea internă"}/>

                                        {(this.state.ePersonalRegistratura || this.state.departamenteUser.length > 1) ?
                                            <Form.Dropdown selection
                                                           width={7}
                                                           options={this.state.ePersonalRegistratura ? this.state.listaDepartamente : this.state.departamenteUser}
                                                           search error={cerere.departamentEmitent.error}
                                                           value={cerere.departamentEmitent.value}
                                                           onChange={((e, data) => this.handleActualizeazaForm('departamentEmitent', data.value))}/>
                                            : <p>{this.state.departamenteUser[0].text}</p>}
                                    </div>}

                                    {((cerere.esteRaspuns.value !== undefined || cerere.esteRevenire.value !== undefined) && cerere.tipulActului.value === 4) &&
                                    <Form.Group style={{
                                        paddingTop: "2%"
                                    }}>
                                        <div>
                                            <RequiredStar required={true} esteFormGroup={true}
                                                          labelTitle={"Cine face solicitarea de număr"}
                                                          continutPopup={"Se va alege statutul celui care face solicitarea: persoană fizică dacă faceți solicitarea pentru dvs sau pentru o persoană fizică, autoritate emitentă în restul cazurilor"}
                                            />
                                            <Form.Dropdown selection
                                                           width={3} error={cerere.tipUser.error}
                                                           options={optiuniDropdownCineAEmisCererea}
                                                           value={cerere.tipUser.value}
                                                           required
                                                           disabled={cerere.tipulActului.value === ""}
                                                           onChange={((e, data) => this.handleActualizeazaForm('tipUser', data.value))}/>
                                        </div>

                                        {(cerere.tipUser.value === 'Autoritate emitenta' && cerere.tipulActului.value === 4) &&
                                        <div style={{
                                            paddingRight: "0.4%",
                                            paddingLeft: "0.5%"
                                        }}>
                                            <RequiredStar labelTitle={"Autoritatea emitentă"} required={true}
                                                          continutPopup={"Specificați autoritatea/instituția care a făcut solicitarea"}/>
                                            <Autosuggest
                                                suggestions={optiuniAutoritate}
                                                onSuggestionsFetchRequested={this.getSugestiiAutoritateEmitenta}
                                                onSuggestionsClearRequested={this.clearSugestiiAutoritateEmitenta}
                                                inputProps={autoritateInputProps}
                                                getSuggestionValue={getSuggestionValue}
                                                renderSuggestion={renderSuggestion}
                                            />
                                        </div>
                                        }

                                        {(cerere.tipulActului.value === 4 && (cerere.tipUser.value === 'Autoritate emitenta' || cerere.tipUser.value === "Persoana fizica"))
                                        &&
                                        <div style={{
                                            paddingRight: "1%",
                                            paddingLeft: "0.5%"
                                        }}>
                                            {cerere.tipUser.value === "Autoritate emitenta" &&
                                            <RequiredStar required={false}
                                                          labelTitle={"Persoana din instituția solicitantă"}
                                                          continutPopup={"Specificați numele solicitantului"}/>}
                                            {cerere.tipUser.value === "Persoana fizica" &&
                                            <RequiredStar required={true}
                                                          labelTitle={"Nume solicitant"}
                                                          continutPopup={"Specificați numele solicitantului"}/>}

                                            <Autosuggest
                                                suggestions={optiuniNumeDeponent}
                                                onSuggestionsFetchRequested={this.getSugestiiNumeDeponent}
                                                onSuggestionsClearRequested={this.clearSugestiiNumeDeponent}
                                                inputProps={numeDeponentInputProps}
                                                getSuggestionValue={getSuggestionValue}
                                                renderSuggestion={renderSuggestion}
                                            />
                                        </div>}


                                        {(cerere.tipulActului.value === 4 && (cerere.tipUser.value === 'Autoritate emitenta' || cerere.tipUser.value === "Persoana fizica"))
                                        && <div>
                                            <RequiredStar required={false}
                                                          labelTitle={cerere.tipUser.value === 'Autoritate emitenta' ? "E-mail persoană din instituția solicitantă" : "E-mail solicitant"}
                                                          continutPopup={"Specificați e-mailul solicitantului"}/>
                                            <Autosuggest
                                                suggestions={optiuniEmailDeponent}
                                                onSuggestionsFetchRequested={this.getSugestiiEmailDeponent}
                                                onSuggestionsClearRequested={this.clearSugestiiEmailDeponent}
                                                inputProps={emailDeponentInputProps}
                                                getSuggestionValue={getSuggestionValue}
                                                renderSuggestion={renderSuggestion}
                                            />
                                        </div>}
                                    </Form.Group>}


                                    <Form.Group>
                                        {cerere.tipulActului.value !== '' ?
                                            this.state.ePersonalRegistratura ?
                                                <div style={{
                                                    paddingRight: "1%",
                                                    paddingLeft: "0.5%",
                                                    width: "20%",
                                                    paddingTop: "0.6%"
                                                }}>
                                                    <RequiredStar labelTitle={"Nume angajat care înregistrează"}
                                                                  required={true}/>
                                                    <Autosuggest
                                                        suggestions={optiuniNumeAngajat}
                                                        onSuggestionsFetchRequested={this.getSugestiiNume}
                                                        onSuggestionsClearRequested={this.clearSugestiiNume}
                                                        inputProps={numeAngajatInputProps}
                                                        getSuggestionValue={getSuggestionValueName}
                                                        renderSuggestion={renderSuggestionValueName}
                                                        onSuggestionSelected={this.onNameAngajatSuggestionSelected}
                                                    />
                                                </div>
                                                : <div style={{
                                                    paddingRight: "1%",
                                                    paddingLeft: "0.9%",
                                                    width: "20%",
                                                    paddingTop: "1%"
                                                }}>
                                                    <RequiredStar required={false}
                                                                  labelTitle={"Nume angajat care înregistrează"}/>
                                                    <p>{cerere.numeAngajat.value}</p>
                                                </div>
                                            : null}

                                        {cerere.tipulActului.value !== '' ?
                                            this.state.ePersonalRegistratura ?
                                                <div style={{
                                                    width: "20%",
                                                    paddingTop: "0.6%"
                                                }}>
                                                    <RequiredStar labelTitle={"E-mail angajat care înregistrează"}
                                                                  required={true}/>
                                                    <Autosuggest
                                                        suggestions={optiuniEmailAngajat}
                                                        onSuggestionsFetchRequested={this.getSugestiiEmail}
                                                        onSuggestionsClearRequested={this.clearSugestiiEmail}
                                                        inputProps={emailAngajatInputProps}
                                                        getSuggestionValue={getSuggestionValueEmail}
                                                        renderSuggestion={renderSuggestionValueEmail}
                                                        onSuggestionSelected={this.onEmailAngajatSuggestionSelected}
                                                    />

                                                </div>
                                                : <div style={{
                                                    width: "20%",
                                                    paddingTop: "1%"
                                                }}>
                                                    <RequiredStar required={false}
                                                                  labelTitle={"E-mail angajat care înregistrează"}/>
                                                    <p>{cerere.emailAngajat.value}</p>
                                                </div>
                                            : null}

                                    </Form.Group>

                                    <Form.Group>
                                        {cerere.tipulActului.value === 5 ?
                                            <div style={{
                                                paddingRight: "0.8%",
                                                paddingBottom: "1%",
                                                paddingLeft: "0.5%",
                                                width: "20%"
                                            }}>
                                                <RequiredStar required={true}
                                                              labelTitle={"Către cine pleacă în exterior"}
                                                              continutPopup={"Se precizează către cine se trimite solicitarea de ieșire"}/>
                                                <Autosuggest
                                                    suggestions={optiuniCatreCinePleaca}
                                                    onSuggestionsFetchRequested={this.getSugestiiCatreCinePleaca}
                                                    onSuggestionsClearRequested={this.clearSugestiiCatreCinePleaca}
                                                    inputProps={catreCinePleacaInputProps}
                                                    getSuggestionValue={getSuggestionValue}
                                                    renderSuggestion={renderSuggestion}
                                                />
                                            </div> : cerere.tipulActului.value === 6 ?
                                                <div style={{
                                                    paddingLeft: "0.6%",
                                                }}>
                                                    <RequiredStar required={true}
                                                                  labelTitle={"Către cine pleacă intern"}
                                                                  continutPopup={"Se precizează la ce departamente este necesar să ajungă documentul curent"}/>
                                                    <ModalSeteazaTraseu
                                                        color={"grey"}
                                                        handleModalClose={this.handleModalActulPleacaLaClose}
                                                        handleModalOpen={this.handleModalActulPleacaLaOpen}
                                                        modalOpen={this.state.modalActulPleacaLaOpen}
                                                        modalTitle={"Setează Traseu Document"}
                                                        tabelComponent={
                                                            <CatreCinePleacaInternComponent
                                                                listaDepartamente={this.state.listaDepartamente}
                                                                emailUser={this.state.detaliiUser.email}
                                                                actualizeazaStateCerere={this.actualizeazaStateCerere.bind(this)}
                                                                traseuDocument={cerere.catreCinePleacaIntern.value}
                                                            />}
                                                        iconName={"pin"}
                                                        textBtnTriggerModal={"Setează traseu"}/>
                                                </div>
                                                : null}

                                        <div style={{
                                            paddingTop: "2%",
                                            paddingRight: "1%",
                                            paddingLeft: "1%"
                                        }}>
                                            {(cerere.tipulActului.value === 5 || cerere.tipulActului.value === 6)
                                            && cerere.esteRaspuns.value !== undefined && this.state.esteInTimpulProgramului &&
                                            <Form.Checkbox
                                                label={<RequiredStar
                                                    labelTitle={cerere.esteRaspuns.value === 'Da' ?
                                                        "Doriți ca statusul solicitării la care se face răspuns să fie 'Finalizat'?" :
                                                        "Doriți ca statusul solicitării curente să fie 'Finalizat'?"}
                                                    required={false}
                                                    continutPopup={cerere.esteRaspuns.value === 'Da' ? "Bifarea acestei opțiuni nu va seta și statusul solicitării curente ca fiind 'Finalizat'" : undefined}/>
                                                }
                                                onChange={() => {
                                                    this.handleActualizeazaForm("solicitareFinalizata", !cerere.solicitareFinalizata);
                                                }}
                                                checked={cerere.solicitareFinalizata}
                                            />}
                                            {(cerere.tipulActului.value === 5 || cerere.tipulActului.value === 6)
                                            && cerere.solicitareFinalizata && cerere.esteRaspuns.value !== undefined &&
                                            <Form.Checkbox
                                                onChange={() => {
                                                    this.handleActualizeazaForm("solicitareExpediata", !cerere.solicitareExpediata);
                                                }}
                                                label={
                                                    <RequiredStar
                                                        labelTitle={cerere.esteRaspuns.value === 'Da' ?
                                                            "Doriți ca statusul solicitării la care se face răspuns să fie și 'Expediat'?" :
                                                            "Doriți ca statusul solicitării curente să fie și 'Expediat'?"}
                                                        required={false}
                                                        continutPopup={cerere.esteRaspuns.value === 'Da' ? "Bifarea acestei opțiuni va seta și statusul solicitării curente ca fiind 'Finalizat' și 'Expediat'" : undefined}
                                                    />
                                                }
                                                checked={cerere.solicitareExpediata}
                                            />}
                                        </div>

                                        {(cerere.tipulActului.value === 5 || cerere.tipulActului.value === 6) && cerere.solicitareExpediata && this.state.esteInTimpulProgramului &&
                                        <div>
                                            <RequiredStar labelTitle={"Modalitate de trimitere"} required={true}
                                                          esteFormGroup={true}
                                                          continutPopup={cerere.esteRaspuns.value === 'Da' ?
                                                              "Specificați modalitatea prin care este expediat răspunsul" :
                                                              cerere.esteRaspuns.value === 'Nu' ? "Specificați modalitatea prin care este expediată solicitarea de ieșire" : ''}/>
                                            <Form.Dropdown
                                                selection error={cerere.modalitateTrimitere.error}
                                                width={3}
                                                options={this.state.modalitatiTrimitere}
                                                search
                                                onChange={((e, data) => this.handleActualizeazaForm('modalitateTrimitere', data.value))}
                                            />
                                        </div>}

                                    </Form.Group>


                                    {cerere.tipulActului.value !== "" &&
                                    <Form.Group style={{
                                        paddingLeft: "0.6%"
                                    }}>
                                        <div style={{
                                            width: "25%"
                                        }}>
                                            <RequiredStar required={true}
                                                          labelTitle={"Conținutul/rezumatul solicitării"}
                                                          continutPopup={"Prezentare pe scurt al conținutului documentului înregistrat"}/>
                                            <Autosuggest
                                                suggestions={optiuniContinut}
                                                onSuggestionsFetchRequested={this.getSugestiiContinut}
                                                onSuggestionsClearRequested={this.clearSugestiiContinut}
                                                inputProps={continutInputProps}
                                                getSuggestionValue={getSuggestionValue}
                                                renderSuggestion={renderSuggestion}
                                            />
                                        </div>
                                        {this.state.afiseazaCampTermenLimita && cerere.tipulActului.value === 4 &&
                                        <Form.Input type="date" min={moment().format("YYYY-MM-DD")}
                                                    label={<RequiredStar required={false} labelTitle={"Termen limită"}/>}
                                               //defaultValue={moment().add(30, 'days').format("YYYY-MM-DD")}
                                               onChange={(event) => this.handleActualizeazaForm("termenLimita", event.target.value)}
                                        />}
                                        <Form.Input width={6} label={<RequiredStar required={false}
                                                                                   labelTitle={"Observații solicitant"}
                                                                                   continutPopup={"Pot fi specificate observații pe scurt cu privire la solicitarea curentă"}/>}
                                                    disabled={cerere.tipulActului.value === ""}
                                                    onChange={(event) => this.handleActualizeazaForm('observatiiUtilizator', event.target.value)}/>
                                    </Form.Group>}
                                </Form>
                            </Grid.Column>
                        </Grid.Row>
                    </div>
                    }


                    {this.state.succesfulPost &&
                    <div>
                        <Grid.Row className="btnDepuneDinNou">
                            <Grid.Column>
                                <MessageComponent iconName={"check"} message1={"Cererea a fost salvată cu succes!"}
                                                  message2={!this.state.esteInTimpulProgramului ? "Veți primi număr de înregistrare în următoarea zi lucrătoare!" : ""}
                                                  positive={true} size={"large"}/>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column>
                                <TabelSimplu listaDocumente={[this.state.responsePost]}/>
                            </Grid.Column>
                        </Grid.Row>

                        <Grid.Row className="btnDepuneDinNou">
                            <Grid.Column>
                                <Button size="large" color="green" type="button"
                                        onClick={() => window.location.reload()}>
                                    <Icon name="file alternate outline"/>
                                    Depune o nouă solicitare
                                </Button>
                            </Grid.Column>
                        </Grid.Row>
                    </div>
                    }

                    {!this.state.succesfulPost &&
                    <ModalEstiSigurCa formIsValid={this.state.formIsValid}
                                      handleOpenModal={this.handleOpenModalConfirmare}
                                      handleCloseModal={this.handleCloseModalConfirmare}
                                      openModal={openModalConfirmare}
                                      actiuneApasareBtnOk={this.postCereri}
                                      stateBtnOk={this.state.btnOkDepune}
                                      modalTitle={"Depune solicitare"}
                                      modalContent={"Sunteți sigur/ă că doriți să depuneți această solicitare?"}
                                      textBtnTriggerModal={"Înregistrează cerere"}
                    />}
                </div>
            </Grid>
        )
    }
}


export default PaginaDepuneriCerere;
