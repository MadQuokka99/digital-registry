import React, {Component} from 'react'
import {Grid, Form, Loader} from 'semantic-ui-react'

import {
    getCereriPanaLaTermenLimita,
    estePersonalRegistratura,
    getCountCereriTermenLimita,
    getDepartamente, getStatusuriCerere, formatareDepPtDropdown, formatarePtDropdown
} from "../Utils/ApiCalls";
import {getUsername} from "../Utils/GetUsername";

import MainPage from "./MainPage";
import MessageComponent from "../Components/MessageComponent";
import TabelViewDocumente from "../Components/TabelViewDocumente";


const optiuniDropdownNrZileTermenLimita = [
    {
        key: 1,
        text: "30",
        value: 30
    },
    {
        key: 2,
        text: "14",
        value: 14
    },
    {
        key: 3,
        text: "7",
        value: 7
    },
    {
        key: 4,
        text: "5",
        value: 5
    },
]

class PaginaNotificariTermenLimita extends Component {

    state = {
        detaliiUser: {
            email: null,
            ID_Departament: null
        },
        listaCereriTermenLimita: [],
        nrZileTermenLimitaAlesDinDropdown: undefined,
        estePersonalRegistratura: false,

        listaDepartamente: [],
        statusuriDocument: [],
        nrCereriTermenLimita: null,
        noItemsFoundFunctieAfisareTermeneLimita: false
    }

    async componentDidMount() {

        let username = getUsername();
        const rol = 'PersonalRegistratura';

        this.setState({});

        await Promise.all([
            getDepartamente(), getStatusuriCerere(), estePersonalRegistratura(rol, username)
        ]).then(([departamente, statusuri, ePersonalRegistratura]) => {
            this.setState({
                listaDepartamente: formatareDepPtDropdown(false, departamente), //este false, deoarece la true ID_Departament se pune -1 pt PersonalRegistratura
                stadiiCereri: formatarePtDropdown(statusuri, "ID_Status", "DenumireStatus"),
                estePersonalRegistratura: ePersonalRegistratura
            })
        })
            .catch(error => {
                console.log(error);
            })
    }

    handleNrZileAlesDinDropdown = (ID_Departament, nrZile) => {
        this.setState({
            nrZileTermenLimitaAlesDinDropdown: nrZile
        })

        getCountCereriTermenLimita(ID_Departament, nrZile)
            .then(response => {
                this.setState({
                    nrCereriTermenLimita: response
                })

            })

        getCereriPanaLaTermenLimita(ID_Departament, nrZile)
            .then(response => {
                this.setState({
                    listaCereriTermenLimita: response,
                    noItemsFoundFunctieAfisareTermeneLimita: response.length === 0
                })
            });
    }

    seteazaDepartamentUser = (username, departament) => {
        this.setState({
            detaliiUser: {
                ID_Departament: departament.value,
                email: username
            },
        })
        this.handleNrZileAlesDinDropdown(departament.value, optiuniDropdownNrZileTermenLimita[2].value);
    }

    actualizeazaInTermeneLimita = (termeneLimita) => {
        this.setState({
            listaCereriTermenLimita: termeneLimita
        })
    }

    render() {
        const detaliiUserCopy = this.state.detaliiUser;
        return (
            <Grid>
                <MainPage explicatiePagina={"În cadrul acestei pagini sunt afișate solicitările care sunt în departamentul dvs și sunt nefinalizate"}
                    match={this.props.match}
                    titluPagina="Pagina Termene Limită"
                    seteazaDepartamentUser={this.seteazaDepartamentUser.bind(this)}
                    afiseazaDropdownDepartamente={true}
                    nrCereriTermenLimita={this.state.nrCereriTermenLimita}
                />

                <Grid.Row centered>
                    <Form>
                        <Form.Dropdown
                            label="Număr zile până la termen limită"
                            options={optiuniDropdownNrZileTermenLimita}
                            selection
                            value={this.state.nrZileTermenLimitaAlesDinDropdown}
                            placeholder="Selectați nr de zile dorite"
                            onChange={((e, data) => {
                                this.handleNrZileAlesDinDropdown(detaliiUserCopy.ID_Departament, data.value)
                            })}
                        />
                    </Form>
                </Grid.Row>

                {this.state.noItemsFoundFunctieAfisareTermeneLimita ?
                    <Grid.Row className="messageInfoStyle">
                        <Grid.Column verticalAlign="middle">
                            <MessageComponent
                                iconName={"time"}
                                message1={"Nu există solicitări care să expire în "}
                                size={"large"}
                                nrZileTermenLimita={this.state.nrZileTermenLimitaAlesDinDropdown}
                                message2={" zile!"}
                                compact={true}
                                info={true}
                            />
                        </Grid.Column>
                    </Grid.Row>
                    : (this.state.listaCereriTermenLimita.length === 0)
                        ? <Loader active inline="centered"/>
                        : <Grid.Row>
                            <TabelViewDocumente
                                afiseazaBtnTrimiteReminder={this.state.estePersonalRegistratura}
                                istoricCereri={this.state.listaCereriTermenLimita}
                                ePersonalRegistratura={false} // conditia asta permite afisarea de editare doar pt traseu si status
                                stadiiCereri={this.state.stadiiCereri}
                                listaDepartamente={this.state.listaDepartamente}
                                actualizeazaInIstoricCerere={this.actualizeazaInTermeneLimita.bind(this)}
                                detaliiUser={this.state.detaliiUser}/>
                        </Grid.Row>}
            </Grid>

        )
    }
}

export default PaginaNotificariTermenLimita;