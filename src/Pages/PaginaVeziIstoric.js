import {
    Grid,
    Loader,
    Form,
} from "semantic-ui-react";
import React, {Component} from "react";
import moment from "moment";
import axios from "../Utils/axios-AGSIS-API";
import MainPage from "./MainPage";

import {
    estePersonalRegistratura,
    getDepartamente,
    getStatusuriCerere,
    getModuriTrimitere,
    formatarePtDropdown,
    aniPtDropdown, formatareDepPtDropdown, getCereriIstoricByDepartament, getCountCereriIstoric, cautareDocument,
} from "../Utils/ApiCalls";
import {getUsername} from "../Utils/GetUsername";
import TabelViewDocumente from "../Components/TabelViewDocumente";
import BannerCuFiltre from "../Components/BannerCuFiltre";
import {verificareObjectIsEmpty} from "../Utils/FunctiiIstoricCereriIntrate";
import MessageComponent from "../Components/MessageComponent";
import PaginationForTable from "../Components/PaginationForTable";
import "../Design/PaginiIstoricCereriIntrate.css";

const filtrariCautareCopy = {
    NrActuluiIntrat: "", //bif
    MailDeponent: "", //bif
    NumeDeponent: "",
    ContinutulActului: "", //bif
    DataInregistrare_Inceput: "",
    DataInregistrare_Sfarsit: "",
    CineAEmis_Autoritate: "", //bif
    CineAEmis_ID_Departament: "", //bif
    NrDeOrdine: null, //bif,
    An: null
}

/**
 * optiunile pentru dropdown-ul de alegere a tipului de solicitant
 */
const optiuniDropdownCineAEmisCererea = [{
    key: 1,
    text: "Persoana fizica",
    value: "Persoana fizica",
    disabled: false
},
    {
        key: 2,
        text: "Departament",
        value: "Departament",
        disabled: false
    }]

class PaginaVeziIstoric extends Component {

    state = {
        istoricCereri: [], //cererile depuse de utilizatorul logat
        filtrariCautare: {...filtrariCautareCopy}, //obiect pentru search-ul din istoric dupa campurile specificate

        cautareDupaParametri: false, //in cazul in care este folosita optiunea de filtrare dupa nr de ordine a cererilor din istoric
        noItemsFoundFunctieFiltrare: false, //in cazul in care functia de filtrare dupa nr de ordine nu returneaza niciun rezultat
        numarCereriIstoric: null, //numarul solicitarilor depuse de utilizator, pentru istoric
        cereriParcurse: null, //in functie de inaintarea pe pagini in istoric, se aduna la acest numar numarul de cereri afisare
        noItemsFoundFunctieAfisareIstoric: false, //in cazul in care functia de afisare istoric returneaza un string gol, se afiseaza un mesaj specific

        macarUnFiltruActiv: false,
        loaderDocumentCautare: false,

        anAlesDinDropdownPentruIstoric: moment().year(),  //anul ales de utilizator pentru afisarea istoricului
        aniIstoric: [], //anii din 2020 pana in prezent pentru afisarea istoricului

        idCerereInEditare: null, //state pentru a permite editarea campurilor unei linii (cereri) din istoric, daca e gol se poate alege o cerere, daca nu, nu
        ePersonalRegistratura: false, //state boolean pentru a verifica daca utilizatorul este personal din Registratura

        listaDepartamente: [],

        detaliiUser: {
            email: null,
            ID_Departament: null
        },

        filtruTipUserAfisareIstoric: '',

        stadiiCereri: [], //array cu stadiile posibile ale unei solicitari
        modalitatiTrimitere: [], //array cu modalitatile de trimitere

        afiseazaDropdownDepartamente: true
    }

    async componentDidMount() {

        let listaAni = aniPtDropdown();

        let filtreCopy = {...this.state.filtrariCautare}
        filtreCopy.An = listaAni[0].value;

        this.setState({
            aniIstoric: listaAni,
            filtrariCautare: filtreCopy
        })


        let username = getUsername();
        const rol = 'PersonalRegistratura';

        await Promise.all([
            estePersonalRegistratura(rol, username), getDepartamente(), getStatusuriCerere(), getModuriTrimitere(),
        ]).then(([estePersonalRegistratura, departamente, statusuri, moduriTrimitere]) => {

            if (estePersonalRegistratura) {
                username = -1;
            }

            this.setState({
                ePersonalRegistratura: estePersonalRegistratura,
                listaDepartamente: formatareDepPtDropdown(false, departamente), //este false, deoarece la true ID_Departament se pune -1 pt PersonalRegistratura
                stadiiCereri: formatarePtDropdown(statusuri, "ID_Status", "DenumireStatus"),
                modalitatiTrimitere: formatarePtDropdown(moduriTrimitere, "ID_ModTrimitereRaspuns", "DenumireModTrimitereRaspuns"),
                detaliiUser: {
                    email: username
                },
                anAlesDinDropdownPentruIstoric: listaAni[0].value
            })
        })
            .catch(error => {
                console.log(error);
            })

    }


    actualizeazaStateAfisareIstoric = (raspuns) => {

        if (raspuns.length === 0) {
            this.setState({
                noItemsFoundFunctieAfisareIstoric: true
            })
        } else {
            this.setState({
                noItemsFoundFunctieAfisareIstoric: false
            })
        }

        let filtreCopy = {...filtrariCautareCopy};
        filtreCopy.An = this.state.anAlesDinDropdownPentruIstoric;

        this.setState({
            istoricCereri: raspuns,
            filtrariCautare: filtreCopy,
            cautareDupaParametri: false,
            noItemsFoundFunctieFiltrare: false,
            cereriParcurse: this.state.cereriParcurse + raspuns.length
        })
    }


    /**
     *
     * @param nrPagina
     * @param an
     *
     * functie care:
     * - returneaza nr de solicitari depuse de utilizatorul logat
     * - returneaza un array de obiecte de tip Cerere, reprezentand istoricul de cereri depuse de utilizator, retinut in state-ul istoricCereri
     */
    getIstoricCereriByUsername = (nrPagina, an) => {

        axios
            .get(`Registratura/DocumentCountByUsernameAn?Username=${this.state.detaliiUser.email}&An=${an}`)
            .then(numarCereri => {
                this.setState({
                    numarCereriIstoric: numarCereri.data,
                })
            })
            .catch(error => {
                console.log(error);
            })

        axios
            .get(`Registratura/DocumentListPagedByUsernameAn?Username=${this.state.detaliiUser.email}&An=${an}&NrPePagina=${8}&NrPagina=${nrPagina}`)
            .then(raspuns => {
                this.actualizeazaStateAfisareIstoric(raspuns.data);
            })
            .catch(error => {
                console.log(error);
            })
    }

    getIstoricCereriByDepartament = (ID_Departament, an, nrPagina) => {

        getCountCereriIstoric(ID_Departament, an)
            .then(raspuns => {
                this.setState({
                    numarCereriIstoric: raspuns,
                })
            })

        getCereriIstoricByDepartament(ID_Departament, an, 8, nrPagina)
            .then(raspuns => {
                this.actualizeazaStateAfisareIstoric(raspuns);
            })
            .catch(error => {
                console.log(error);
            })
    }


    /**
     *
     * functia care returneaza un array de obiecte de tip Cerere in functie de numarul de ordine specificat de utilizator
     */
    filtrareIstoricDupaParametri = (obiect) => {
        this.setState({
            loaderDocumentCautare: true
        })

        cautareDocument(obiect)
            .then(raspuns => {
                this.setState({
                    loaderDocumentCautare: false
                })

                if (raspuns.length === 0) {
                    this.setState({
                        noItemsFoundFunctieFiltrare: true,
                    })
                } else {
                    this.setState({
                        noItemsFoundFunctieFiltrare: false
                    })
                }

                this.setState({
                    loaderDocumentCautare: false,
                    istoricCereri: raspuns,
                    cautareDupaParametri: true,
                    numarCereriIstoric: raspuns.length
                })
            })
    }


    handleAnAlesDinDropdown = (anAles) => {

        let filtreCopy = {...this.state.filtrariCautare}
        filtreCopy.An = anAles;

        this.setState({
            anAlesDinDropdownPentruIstoric: anAles,
            filtrariCautare: filtreCopy
        })

        if (verificareObjectIsEmpty(filtreCopy)) {
            if (this.state.filtruTipUserAfisareIstoric === 'Persoana fizica') {
                this.getIstoricCereriByUsername(1, anAles);
            } else if (this.state.filtruTipUserAfisareIstoric === 'Departament')
                this.getIstoricCereriByDepartament(this.state.detaliiUser.ID_Departament, anAles, 1);
        } else
            this.filtrareIstoricDupaParametri(filtreCopy);


    }

    actualizeazaInIstoricCerere = (istoricCereri) => {
        this.setState({
            istoricCereri: istoricCereri
        })
    }

    seteazaDepartamentUser = (username, departament) => {
        this.setState({
            detaliiUser: {
                ID_Departament: departament.value,
                email: username
            },
            filtruTipUserAfisareIstoric: 'Departament'
        })

        this.getIstoricCereriByDepartament(departament.value, this.state.anAlesDinDropdownPentruIstoric, 1);
    }


    handleInputIntrodusDeUser = (key, input) => {

        let filtrariCautareCopy = {...this.state.filtrariCautare};

        if (key === "cineAEmis") {

            let listaDepartamenteCopy = this.state.listaDepartamente.filter(dep => input === dep.key);

            filtrariCautareCopy["CineAEmis_ID_Departament"] = listaDepartamenteCopy[0].value;

        } else if (key === 'NrDeOrdine' && input !== null && input !== '') {
            filtrariCautareCopy[key] = parseInt(input);
        } else
            filtrariCautareCopy[key] = input;

        if (input === "") {
            if (verificareObjectIsEmpty(filtrariCautareCopy)) {
                this.setState({
                    macarUnFiltruActiv: false
                })
                if (this.state.filtruTipUserAfisareIstoric === 'Persoana fizica') {
                    this.getIstoricCereriByUsername(1, this.state.anAlesDinDropdownPentruIstoric);
                } else if (this.state.filtruTipUserAfisareIstoric === 'Departament')
                    this.getIstoricCereriByDepartament(this.state.detaliiUser.ID_Departament, this.state.anAlesDinDropdownPentruIstoric, 1);
            } else {
                this.setState({
                    macarUnFiltruActiv: true
                })
            }
        } else this.setState({
            macarUnFiltruActiv: true
        })
        this.setState({
            filtrariCautare: filtrariCautareCopy,
        })
    }


    resetareFiltre = () => {
        let filtreCopy = {...filtrariCautareCopy};
        filtreCopy.An = this.state.anAlesDinDropdownPentruIstoric;

        this.setState({
            filtrariCautare: filtreCopy,
            macarUnFiltruActiv: false
        })

        if (this.state.filtruTipUserAfisareIstoric === 'Persoana fizica') {
            this.getIstoricCereriByUsername(1, this.state.anAlesDinDropdownPentruIstoric);
        } else if (this.state.filtruTipUserAfisareIstoric === 'Departament')
            this.getIstoricCereriByDepartament(this.state.detaliiUser.ID_Departament, this.state.anAlesDinDropdownPentruIstoric, 1);
    }


    handleTipUserAlesDinDropdown = (tipUser) => {
        this.setState({
            filtruTipUserAfisareIstoric: tipUser
        })

        if (tipUser === 'Persoana fizica') {
            this.getIstoricCereriByUsername(1, this.state.anAlesDinDropdownPentruIstoric);
            this.setState({
                afiseazaDropdownDepartamente: false
            })

        } else if (tipUser === 'Departament') {
            this.getIstoricCereriByDepartament(this.state.detaliiUser.ID_Departament, this.state.anAlesDinDropdownPentruIstoric, 1);
            this.setState({
                afiseazaDropdownDepartamente: true
            })

        }
    }

    render() {
        return (
            <Grid>
                <MainPage explicatiePagina={"În cadrul acestei pagini aveți istoricul solicitărilor depuse de către departamentul dvs"}
                    match={this.props.match}
                    titluPagina="Pagina Istoric Solicitări"
                    seteazaDepartamentUser={this.seteazaDepartamentUser.bind(this)}
                    afiseazaDropdownDepartamente={this.state.afiseazaDropdownDepartamente}/>

                <Grid.Row centered style={{
                    paddingBottom: "0%",
                    paddingTop: "0%"
                }}>
                        <Form>
                            <Form.Dropdown
                                selection fluid
                                label="Filtrare după"
                                options={optiuniDropdownCineAEmisCererea}
                                defaultValue={optiuniDropdownCineAEmisCererea[1].value}
                                onChange={((e, data) => this.handleTipUserAlesDinDropdown(data.value))}
                            />
                        </Form>
                </Grid.Row>
                <Grid.Row centered>
                    <BannerCuFiltre
                        handleInputIntrodusDeUser={this.handleInputIntrodusDeUser.bind(this)}
                        filtrareIstoricDupaParametri={this.filtrareIstoricDupaParametri.bind(this)}
                        resetareFiltre={this.resetareFiltre.bind(this)}
                        handleAnAlesDinDropdown={this.handleAnAlesDinDropdown.bind(this)}
                        aniIstoric={this.state.aniIstoric}
                        anAlesDinDropdownPentruIstoric={this.state.anAlesDinDropdownPentruIstoric}
                        macarUnFiltruActiv={this.state.macarUnFiltruActiv}
                        filtrariCautare={this.state.filtrariCautare}
                        listaDepartamente={this.state.listaDepartamente}
                    />
                </Grid.Row>

                {this.state.noItemsFoundFunctieAfisareIstoric ?
                    (<Grid.Row className="messageInfoStyle">
                        <Grid.Column verticalAlign="middle">
                            <MessageComponent iconName={"search"} message1={"Nu aveți nicio solicitare depusă!"}
                                              info={true} compact={true} size={"large"}
                                              messageContent={" Mergeți pe pagina 'Depune Cerere' pentru a depune o solicitare"}/>
                        </Grid.Column>
                    </Grid.Row>)

                    : this.state.loaderDocumentCautare ?
                        <Loader active inline="centered"/>
                        : this.state.noItemsFoundFunctieFiltrare ?
                            (<Grid.Row className="messageInfoStyle">
                                    <Grid.Column verticalAlign="middle">
                                        <MessageComponent iconName={"search"}
                                                          message1={"Nu s-a găsit nicio solicitare care să conțină aceste informații!"}
                                                          messageContent={"Vă rugăm să verificați corectitudinea datelor introduse"}
                                                          info={true}
                                                          compact={true}
                                                          size={"large"}/>
                                    </Grid.Column>

                                </Grid.Row>
                            ) : (this.state.istoricCereri.length === 0
                                ? <Loader active inline="centered"/>
                                : <Grid.Row>
                                    <Grid.Column>
                                        <TabelViewDocumente
                                            istoricCereri={this.state.istoricCereri}
                                            ePersonalRegistratura={this.state.ePersonalRegistratura}
                                            stadiiCereri={this.state.stadiiCereri}
                                            modalitatiTrimitere={this.state.modalitatiTrimitere}
                                            listaDepartamente={this.state.listaDepartamente}
                                            actualizeazaInIstoricCerere={this.actualizeazaInIstoricCerere.bind(this)}
                                            detaliiUser={this.state.detaliiUser}
                                        />
                                    </Grid.Column>
                                </Grid.Row>)}

                {!this.state.cautareDupaParametri && !this.state.noItemsFoundFunctieFiltrare && !this.state.noItemsFoundFunctieAfisareIstoric &&
                <PaginationForTable
                    labelMessage={"Total cereri depuse: "}
                    nrCereri={this.state.numarCereriIstoric}
                    detaliiUser={this.state.detaliiUser}
                    anAlesDinDropdown={this.state.anAlesDinDropdownPentruIstoric}
                    getCereri={this.getIstoricCereriByDepartament}
                    getIstoricCereriByUsername={this.getIstoricCereriByUsername}
                    filtruTipUser={this.state.filtruTipUserAfisareIstoric}
                />}
            </Grid>
        )
    }
}

export default PaginaVeziIstoric;
