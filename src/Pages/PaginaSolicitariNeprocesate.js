import React, {Component} from "react";
import MainPage from "./MainPage";
import {Grid, Loader} from "semantic-ui-react";
import TabelViewDocumente from "../Components/TabelViewDocumente";
import {getCereriNeprocesate} from "../Utils/ApiCalls";
import MessageComponent from "../Components/MessageComponent";
import "../Design/PaginiIstoricCereriIntrate.css";

class PaginaSolicitariNeprocesate extends Component {

    state = {

        noItemsFoundFunctieAfisareSolicitariNeprocesate: false,
        solicitariNeprocesate: [],

    }

    componentDidMount() {

       this.getInfoSolicitariNeprocesate();

    }

    getInfoSolicitariNeprocesate = () => {
        getCereriNeprocesate()
            .then(response => {
                if (response.length === 0) {
                    this.setState({
                        noItemsFoundFunctieAfisareSolicitariNeprocesate: true
                    })
                } else
                    this.setState({
                        noItemsFoundFunctieAfisareSolicitariNeprocesate: false
                    })
                this.setState({
                    solicitariNeprocesate: response
                })
            })
    }

    render() {
        return (
            <Grid>
                <MainPage
                    titluPagina="Pagină Solicitări Neprocesate"
                    match={this.props.match}
                    afiseazaDropdownDepartamente={false}
                    nrCereriNeprocesate={this.state.nrCereriNeprocesate}
                />

                {this.state.noItemsFoundFunctieAfisareSolicitariNeprocesate ?
                    <Grid.Row className="messageInfoStyle">
                        <Grid.Column verticalAlign="middle">
                            <MessageComponent iconName={"tasks"} message1={"Nu aveți nicio solicitare neprocesată!"}
                                              size={"large"}
                                              compact={true}
                                              info={true}
                                              />
                        </Grid.Column>
                    </Grid.Row> :
                    (this.state.solicitariNeprocesate.length === 0)
                        ? <Loader active inline="centered"/>
                        : <TabelViewDocumente istoricCereri={this.state.solicitariNeprocesate}
                                              afiseazaBtnInregistreaza={true}
                                              getInfoSolicitariNeprocesate={this.getInfoSolicitariNeprocesate.bind(this)}
                        />

                }
            </Grid>)
    }
}


export default PaginaSolicitariNeprocesate;