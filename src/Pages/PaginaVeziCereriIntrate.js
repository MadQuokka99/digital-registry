import React, {Component} from "react";
import TabelViewDocumente from "../Components/TabelViewDocumente";

import {
    aniPtDropdown, cautareDocument, formatareDepPtDropdown, formatarePtDropdown,
    getCereriIntrateByDepartament, getCountCereriIntrate, getDepartamente, getStatusuriCerere
} from "../Utils/ApiCalls";
import moment from "moment";
import {
    Grid,
    Loader,
} from "semantic-ui-react";
import MainPage from "./MainPage";
import BannerCuFiltre from "../Components/BannerCuFiltre";
import {verificareObjectIsEmpty} from "../Utils/FunctiiIstoricCereriIntrate";
import MessageComponent from "../Components/MessageComponent";
import PaginationForTable from "../Components/PaginationForTable";
import "../Design/PaginiIstoricCereriIntrate.css";

const filtrariCautareCopy = {
    NrActuluiIntrat: "", //bif
    MailDeponent: "", //bif
    NumeDeponent: "",
    ContinutulActului: "", //bif
    DataInregistrare_Inceput: "",
    DataInregistrare_Sfarsit: "",
    CineAEmis_Autoritate: "", //bif
    CineAEmis_ID_Departament: "", //bif
    NrDeOrdine: null, //bif,
    An: null
}


class PaginaVeziCereriIntrate extends Component {

    state = {

        cereriIntrate: [],
        filtrariCautare: {...filtrariCautareCopy}, //obiect pentru search-ul din istoric dupa campurile specificate

        detaliiUser: {
            email: null,
            ID_Departament: null,
        },

        cautareDupaParametri: false, //in cazul in care este folosita optiunea de filtrare dupa nr de ordine a cererilor din istoric
        noItemsFoundFunctieFiltrare: false, //in cazul in care functia de filtrare dupa nr de ordine nu returneaza niciun rezultat
        numarCereriIntrate: null, //numarul solicitarilor depuse de utilizator, pentru istoric
        cereriParcurse: null, //in functie de inaintarea pe pagini in istoric, se aduna la acest numar numarul de cereri afisare
        noItemsFoundFunctieAfisareCereriIntrate: false, //in cazul in care functia de afisare istoric returneaza un string gol, se afiseaza un mesaj specific

        macarUnFiltruActiv: false,
        loaderDocumentCautare: false,

        anAlesDinDropdownPentruCereriIntrate: moment().year(),  //anul ales de utilizator pentru afisarea istoricului
        aniIstoric: [], //anii din 2020 pana in prezent pentru afisarea istoricului

        listaDepartamente: []

    }

    async componentDidMount() {

        let listaAni = aniPtDropdown();

        let filtreCopy = {...this.state.filtrariCautare}
        filtreCopy.An = listaAni[0].value;

        await Promise.all([
            getDepartamente(), getStatusuriCerere()
        ]).then(([departamente, statusuri]) => {

            this.setState({
                listaDepartamente: formatareDepPtDropdown(false, departamente), //este false, deoarece la true ID_Departament se pune -1 pt PersonalRegistratura
                stadiiCereri: formatarePtDropdown(statusuri, "ID_Status", "DenumireStatus"),
                aniIstoric: listaAni,
                anAlesDinDropdownPentruCereriIntrate: listaAni[0].value,
                filtrariCautare: filtreCopy
            })
        })
            .catch(error => {
                console.log(error);
            })

    }

    seteazaDepartamentUser = (username, departament) => {
        this.setState({
            detaliiUser: {
                ID_Departament: departament.value,
                email: username
            },
        })
        this.getCereriIntrate(departament.value, this.state.anAlesDinDropdownPentruCereriIntrate, 1);
    }

    /**
     *
     * @param obiect
     *
     * functia care returneaza un array de obiecte de tip Cerere in functie de numarul de ordine specificat de utilizator
     */
    filtrareCereriIntrateDupaParametri = (obiect) => {

        this.setState({
            loaderDocumentCautare: true
        })
        cautareDocument(obiect)
            .then(raspuns => {
                this.setState({
                    loaderDocumentCautare: false
                })

                if (raspuns.length === 0) {
                    this.setState({
                        noItemsFoundFunctieFiltrare: true,
                    })
                } else {
                    this.setState({
                        noItemsFoundFunctieFiltrare: false
                    })
                }
                this.setState({
                    loaderDocumentCautare: false,
                    cereriIntrate: raspuns,
                    cautareDupaParametri: true,
                    numarCereriIntrate: raspuns.length
                })
            })
    }


    getCereriIntrate = (ID_Departament, an, nrPagina) => {

        getCountCereriIntrate(ID_Departament, an)
            .then(raspuns => {
                this.setState({
                    numarCereriIntrate: raspuns,
                })
            })


        getCereriIntrateByDepartament(ID_Departament, an, 8, nrPagina)
            .then(response => {

                if (response.length === 0) {
                    this.setState({
                        noItemsFoundFunctieAfisareCereriIntrate: true
                    })
                } else {
                    this.setState({
                        noItemsFoundFunctieAfisareCereriIntrate: false
                    })
                }
                let filtreCopy = {...filtrariCautareCopy};
                filtreCopy.An = this.state.anAlesDinDropdownPentruCereriIntrate;

                this.setState({
                    cereriIntrate: response,
                    filtrariCautare: filtreCopy,
                    cautareDupaParametri: false,
                    noItemsFoundFunctieFiltrare: false,
                })
            })
    }

    handleAnAlesDinDropdown = (anAles) => {

        let filtreCopy = {...this.state.filtrariCautare}
        filtreCopy.An = anAles;

        this.setState({
            anAlesDinDropdownPentruCereriIntrate: anAles,
            filtrariCautare: filtreCopy
        })

        this.getCereriIntrate(this.state.detaliiUser.ID_Departament, anAles, 1);
    }

    handleInputIntrodusDeUser = (key, input) => {
        let filtrariCautareCopy = {...this.state.filtrariCautare};

        if (key === "cineAEmis") {

            let listaDepartamenteCopy = this.state.listaDepartamente.filter(dep => input === dep.key);

            filtrariCautareCopy["CineAEmis_ID_Departament"] = listaDepartamenteCopy[0].value;

        } else if (key === 'NrDeOrdine' && input !== '' && input !== null) {
            filtrariCautareCopy[key] = parseInt(input)
        } else filtrariCautareCopy[key] = input;

        if (input === "") {
            if (verificareObjectIsEmpty(filtrariCautareCopy)) {
                this.setState({
                    macarUnFiltruActiv: false
                })
                this.getCereriIntrate(this.state.detaliiUser.ID_Departament, this.state.anAlesDinDropdownPentruCereriIntrate, 1);
            } else {
                this.setState({
                    macarUnFiltruActiv: true
                })
            }
        } else this.setState({
            macarUnFiltruActiv: true
        })
        this.setState({
            filtrariCautare: filtrariCautareCopy,
        })
    }

    resetareFiltre = () => {

        let filtreCopy = {...filtrariCautareCopy};
        filtreCopy.An = this.state.anAlesDinDropdownPentruCereriIntrate;

        this.setState({
            filtrariCautare: filtreCopy,
            macarUnFiltruActiv: false
        })
        this.getCereriIntrate(this.state.detaliiUser.ID_Departament, this.state.anAlesDinDropdownPentruCereriIntrate, 1);
    }

    actualizeazaInIstoricCerere = (istoricCereri) => {
        this.setState({
            cereriIntrate: istoricCereri
        })
    }

    render() {
        return (
            <Grid>
                <MainPage explicatiePagina={"În cadrul acestei pagini aveți istoricul solicitărilor care sunt sau au fost la un moment dat în departamentul dvs"}
                    match={this.props.match}
                    titluPagina="Pagina Solicitări Intrate"
                    seteazaDepartamentUser={this.seteazaDepartamentUser.bind(this)}
                    afiseazaDropdownDepartamente={true}/>

                <Grid.Row centered>
                    <BannerCuFiltre
                        handleInputIntrodusDeUser={this.handleInputIntrodusDeUser.bind(this)}
                        filtrareIstoricDupaParametri={this.filtrareCereriIntrateDupaParametri.bind(this)}
                        resetareFiltre={this.resetareFiltre.bind(this)}
                        handleAnAlesDinDropdown={this.handleAnAlesDinDropdown.bind(this)}
                        aniIstoric={this.state.aniIstoric}
                        anAlesDinDropdownPentruIstoric={this.state.anAlesDinDropdownPentruCereriIntrate}
                        macarUnFiltruActiv={this.state.macarUnFiltruActiv}
                        filtrariCautare={this.state.filtrariCautare}
                        listaDepartamente={this.state.listaDepartamente}
                    />
                </Grid.Row>

                {this.state.noItemsFoundFunctieAfisareCereriIntrate ?
                    (<Grid.Row className="messageInfoStyle">
                        <Grid.Column verticalAlign="middle">
                            <MessageComponent iconName={"search"} message1={"Nu aveți nicio solicitare intrată!"}
                                              info={true} compact={true} size={"large"}/>
                        </Grid.Column>
                    </Grid.Row>)
                    : this.state.loaderDocumentCautare ?
                        <Loader active inline="centered"/>
                        : this.state.noItemsFoundFunctieFiltrare ?
                            (<Grid.Row className="messageInfoStyle">
                                    <Grid.Column verticalAlign="middle">
                                        <MessageComponent iconName={"search"}
                                                          message1={" Nu s-a găsit nicio solicitare care să conțină aceste informații!"}
                                                          messageContent={"Vă rugăm să verificați corectitudinea datelor introduse"}
                                                          info={true}
                                                          compact={true}
                                                          size={"large"}/>
                                    </Grid.Column>

                                </Grid.Row>
                            ) : (this.state.cereriIntrate.length === 0
                                ? <Loader active inline="centered"/>
                                : <Grid.Row>
                                    <Grid.Column>
                                        <TabelViewDocumente
                                            istoricCereri={this.state.cereriIntrate}
                                            ePersonalRegistratura={false}
                                            stadiiCereri={this.state.stadiiCereri}
                                            listaDepartamente={this.state.listaDepartamente}
                                            actualizeazaInIstoricCerere={this.actualizeazaInIstoricCerere.bind(this)}
                                            detaliiUser={this.state.detaliiUser}
                                        />
                                    </Grid.Column>
                                </Grid.Row>)}

                {!this.state.cautareDupaParametri && !this.state.noItemsFoundFunctieFiltrare && !this.state.noItemsFoundFunctieAfisareCereriIntrate &&
                <PaginationForTable
                    labelMessage={"Total cereri intrate:  "}
                    nrCereri={this.state.numarCereriIntrate}
                    detaliiUser={this.state.detaliiUser}
                    anAlesDinDropdown={this.state.anAlesDinDropdownPentruCereriIntrate}
                    getCereri={this.getCereriIntrate}
                />}

            </Grid>
        )
    }
}

export default PaginaVeziCereriIntrate;