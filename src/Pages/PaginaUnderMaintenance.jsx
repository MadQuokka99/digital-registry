import {Container, Header, Icon, Button} from "semantic-ui-react";
import React, {Component} from "react";
import {setariRegistratura, updateRegistraturaSetari} from "../Utils/ApiCalls";


class PaginaUnderMaintenance extends Component {

    scoatePaginaDinMentenanta() {
        setariRegistratura()
            .then(response => {
                let obiectDeTrimis = response;
                obiectDeTrimis.UnderMaintenance.isUnderMaintenance = !obiectDeTrimis.UnderMaintenance.isUnderMaintenance;

                updateRegistraturaSetari(obiectDeTrimis)
                    .then(raspuns => {
                        this.props.actualizeazaMantenanceStatus(raspuns.UnderMaintenance.isUnderMaintenance);
                    })
            });
    }

    render() {
        return (
            <Container text textAlign={"center"} style={{marginTop: "15%", marginBottom: "5%"}}>
                <Icon name={"cogs"} size={"huge"}/>
                <Header
                    as='h1'
                    content='Pagina se află în mentenanță'
                    inverted
                />
                <Header
                    as='h2'
                    content={this.props.underMaintenanceStatus.message}
                />
                <br/>
                {this.props.ePersonalRegistratura &&
                <Button icon color="green" size="big"
                        onClick={() => this.scoatePaginaDinMentenanta()}
                >
                    <Icon name="lock open"/> Scoate din mentenanță
                </Button>
                }
            </Container>
        )
    }
}

export default PaginaUnderMaintenance;