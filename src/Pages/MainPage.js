import React, {Component} from 'react';
import 'react-router-dom';

import {Form, Grid, Icon, Menu} from 'semantic-ui-react';
import {Link} from "react-router-dom";

import {
    estePersonalRegistratura,
    formatareDepPtDropdown,
    getCountCereriNeprocesate,
    getCountCereriTermenLimita, getDepartamente,
    getDepartamenteDinCareFaceParte,
    getProgramRegistratura
} from "../Utils/ApiCalls";
import pathDocumente from '../Utils/pathDocumente.json'
import {SemanticToastContainer} from "react-semantic-toasts";
import 'react-semantic-toasts/styles/react-semantic-alert.css';
import {getUsername} from "../Utils/GetUsername";
import PropTypes from "prop-types";
import MenuItem from "../Components/MenuItem";
import PopupExplicatiiCampuriDepunere from "../Components/PopupExplicatiiCampuriDepunere";

class MainPage extends Component {

    state = {
        ePersonalRegistratura: false,
        programRegistratura: {
            oraInceput: "",
            oraSfarsit: ""
        }, //obiect pentru retinerea programului de la Registratura

        departamenteUser: [],

        nrCereriTermenLimita: null,
        nrCereriNeprocesate: null,

        departamentAles: ''

    }

    async componentDidMount() {

        let username = getUsername();
        const rol = 'PersonalRegistratura';

        await Promise.all([
            getProgramRegistratura(), estePersonalRegistratura(rol, username)]
        ).then(([programRegistratura, estePersonalRegistratura]) => {
            this.setState({
                programRegistratura: {
                    oraInceput: programRegistratura.OraInceput,
                    oraSfarsit: programRegistratura.OraSfarsit
                },
                ePersonalRegistratura: estePersonalRegistratura,
            })

            if (estePersonalRegistratura) {
                username = -1;
            }


            getDepartamenteDinCareFaceParte(username)
                .then(departamenteUser => {
                    getDepartamente()
                        .then(toateDepartamentele => {
                            let departamente;

                            if (estePersonalRegistratura) {
                                departamente = formatareDepPtDropdown(true, toateDepartamentele)
                            } else
                                departamente = formatareDepPtDropdown(false, departamenteUser)

                            if (this.props.afiseazaDropdownDepartamente) {
                                this.props.seteazaDepartamentUser(username, departamente[0]);
                                this.setState({
                                    departamentAles: departamente[0]
                                })
                            }

                            getCountCereriTermenLimita(departamente[0].value, 7)
                                .then(response => {
                                    this.setState({
                                        nrCereriTermenLimita: response,
                                        departamenteUser: departamente
                                    })
                                })

                            getCountCereriNeprocesate()
                                .then(response => {
                                    this.setState({
                                        nrCereriNeprocesate: response
                                    })
                                })
                        })
                })
        })
            .catch(error => {
                console.log(error);
            })
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.nrCereriTermenLimita !== prevProps.nrCereriTermenLimita) {
            this.setState({
                nrCereriTermenLimita: this.props.nrCereriTermenLimita
            });
        }
    }

    depunereTicket = () => {
        const url = pathDocumente.depunereTicket;
        window.open(url, "_black")
    }

    handleDepartamentAles = (departament) => {

        let departamentUser = this.state.departamenteUser.filter(dep => departament === dep.key);
        let username = getUsername();

        this.props.seteazaDepartamentUser(username, departamentUser[0]);
        this.setState({
            departamentAles: departamentUser[0]
        })
    }


    render() {
        const nrCereriNeprocesate = this.state.nrCereriNeprocesate;
        const nrCereriTermenLimita = this.state.nrCereriTermenLimita;

        return (
            <div>
                <SemanticToastContainer position="top-right"/>
                <Grid>

                    <Grid.Row columns="equal" style={{
                        paddingTop: "3%",
                        paddingBottom: "1%",
                    }}>

                        <Grid.Column>
                            <Menu size="huge">
                                <Menu.Item position="left">
                                    <Icon name="time"/>
                                    <strong>Program Registratură
                                        L-V: {this.state.programRegistratura.oraInceput} - {this.state.programRegistratura.oraSfarsit}</strong>
                                </Menu.Item>


                                <Link to="/">
                                    <MenuItem titlu={'Depune Cerere'}
                                              iconName={"file alternate outline"}
                                              match={this.props.match}
                                              path={"/"}
                                    />
                                </Link>


                                {this.state.ePersonalRegistratura &&
                                <Link to="/solicitarineprocesate">
                                    <MenuItem
                                        titlu={"Solicitări Neprocesate"}
                                        iconName={"folder open outline"}
                                        match={this.props.match}
                                        path={"/solicitarineprocesate"}
                                        nrCereri={nrCereriNeprocesate !== 0 ? nrCereriNeprocesate : undefined}
                                    />
                                </Link>}

                                <Link to="/istoric">
                                    <MenuItem titlu={"Vezi Istoric Solicitări"}
                                              iconName={"history"}
                                              match={this.props.match}
                                              path={"/istoric"}
                                    />
                                </Link>

                                {!this.state.ePersonalRegistratura &&
                                <Link to="/cereriintrate">
                                    <MenuItem titlu={"Vezi Cereri Intrate"}
                                              iconName={"envelope open outline"}
                                              match={this.props.match}
                                              path={"/cereriintrate"}
                                    />
                                </Link>}

                                <Link to={"/termenelimita"}>
                                    <MenuItem
                                        titlu={"Termene limită"}
                                        iconName={"time"}
                                        match={this.props.match}
                                        path={"/termenelimita"}
                                        nrCereri={nrCereriTermenLimita !== 0 ? nrCereriTermenLimita : undefined}
                                    />
                                </Link>

                                <MenuItem
                                    titlu={"Vezi Tutorial"}
                                    iconName={"video play"}
                                    match={this.props.match}
                                    path={""}

                                />

                                <MenuItem
                                    titlu={"Sesizează Neregulă"}
                                    iconName={"question"}
                                    depunereTicket={this.depunereTicket}
                                    match={this.props.match}
                                    path={""}
                                />

                                {this.state.ePersonalRegistratura &&
                                <Link to="/setari">
                                    <MenuItem
                                        titlu={"Setări Registratură"}
                                        iconName={"settings"}
                                        match={this.props.match}
                                        path={"/setari"}
                                    />
                                </Link>}
                            </Menu>
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row columns="equal">
                        <Grid.Column>
                            <h1 style={{
                                paddingTop: "0%",
                                paddingBottom: "0%"
                            }}>
                                {this.props.titluPagina} <sup><PopupExplicatiiCampuriDepunere continutPopup={this.props.explicatiePagina}/></sup>
                            </h1>
                        </Grid.Column>
                    </Grid.Row>

                    {(this.props.afiseazaDropdownDepartamente && this.state.departamenteUser.length > 1 && !this.state.ePersonalRegistratura) &&
                    <Grid.Row centered style={{
                        paddingBottom: "1.3%",
                        paddingTop: "0%"
                    }}>
                        <Form>
                            <Form.Dropdown options={this.state.departamenteUser}
                                           placeholder="Alege departament"
                                           selection
                                           label={"Afișare pentru departamentul"}
                                           defaultValue={this.state.departamentAles.value}
                                           onChange={((e, data) => this.handleDepartamentAles(data.value))}/>
                        </Form>
                    </Grid.Row>}

                </Grid>
            </div>
        )
    }
}

MainPage.propTypes = {
    titluPagina: PropTypes.string.isRequired,
    seteazaDepartamentUser: PropTypes.func,
    afiseazaDropdownDepartamente: PropTypes.bool.isRequired,
    explicatiePagina: PropTypes.string
}

export default MainPage;
