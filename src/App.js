import './App.css';
import React, {Component} from "react";
import {Switch, Route, HashRouter} from "react-router-dom";

import PaginaDepuneriCerere from "./Pages/PaginaDepuneriCerere";
import PaginaVeziIstoric from "./Pages/PaginaVeziIstoric";
import PaginaUnderMaintenance from "./Pages/PaginaUnderMaintenance";
import axios from './Utils/axios-AGSIS-API'
import {Loader} from "semantic-ui-react";
import PaginaNotificariTermenLimita from "./Pages/PaginaNotificariTermenLimita";
import PaginaVeziCereriIntrate from "./Pages/PaginaVeziCereriIntrate";
import PaginaSetariRegistratura from "./Pages/PaginaSetariRegistratura";
import PaginaSolicitariNeprocesate from "./Pages/PaginaSolicitariNeprocesate";
import {getUsername} from "./Utils/GetUsername";
import {estePersonalRegistratura} from "./Utils/ApiCalls";

class App extends Component {
    state = {
        underMaintenance: {
            isUnderMaintenance: null,
            message: null
        },
        ePersonalRegistratura: false
    }

    componentDidMount() {
        axios
            .get(`Registratura/MaintenanceStatus`)
            .then(resp => {
                this.setState({
                    underMaintenance: resp.data //Ne pare rau pentru disconfortul creat! sau Revenim în scurt timp!
                })
            })

        let username = getUsername();
        const rol = 'PersonalRegistratura';

        estePersonalRegistratura(rol, username)
            .then(response => {
                this.setState({
                    ePersonalRegistratura: response
                })
            })
    }

    actualizeazaMantenanceStatus = (isUnderMaintenance) => {

        let underMaintenanceStatusCopy = {...this.state.underMaintenance};
        underMaintenanceStatusCopy.isUnderMaintenance = isUnderMaintenance;

        this.setState({
            underMaintenance: underMaintenanceStatusCopy
        })
    }

    render() {
        return (
            <>
                {
                    this.state.underMaintenance.isUnderMaintenance === null
                        ? <Loader active inline="centered"/>
                        : this.state.underMaintenance.isUnderMaintenance === true
                        ? <PaginaUnderMaintenance
                            ePersonalRegistratura={this.state.ePersonalRegistratura}
                            underMaintenanceStatus={this.state.underMaintenance}
                            actualizeazaMantenanceStatus={this.actualizeazaMantenanceStatus.bind(this)}
                        />
                        :
                        <HashRouter>
                            <Switch>
                                <Route path="/" exact component={PaginaDepuneriCerere}/>
                                <Route path="/istoric" component={PaginaVeziIstoric}/>
                                <Route path="/termenelimita" component={PaginaNotificariTermenLimita}/>
                                <Route path="/cereriintrate" component={PaginaVeziCereriIntrate}/>
                                <Route path="/setari" component={PaginaSetariRegistratura}/>
                                <Route path="/solicitarineprocesate" component={PaginaSolicitariNeprocesate}/>
                            </Switch>
                        </HashRouter>

                }
            </>
        )
    }
}

export default App;
